using Microsoft.VisualStudio.TestTools.UnitTesting;
using Syroot.Worms.Worms2;

namespace Syroot.Worms.Test.Worms2
{
    /// <summary>
    /// Represents a collection of tests for the <see cref="SchemeWeapons"/> class.
    /// </summary>
    [TestClass]
    public class SchemeWeaponsTests
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Tests all files found in the test directory.
        /// </summary>
        [TestMethod]
        public void TestSchemeWeapons() => Tools.TestFiles<SchemeWeapons>("*.wep");
    }
}
