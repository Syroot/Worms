using Microsoft.VisualStudio.TestTools.UnitTesting;
using Syroot.Worms.Worms2;

namespace Syroot.Worms.Test.Worms2
{
    /// <summary>
    /// Represents a collection of tests for the <see cref="SchemeOptions"/> class.
    /// </summary>
    [TestClass]
    public class SchemeOptionsTests
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Tests all files found in the test directory.
        /// </summary>
        [TestMethod]
        public void TestSchemeOptions() => Tools.TestFiles<SchemeOptions>("*.opt");
    }
}
