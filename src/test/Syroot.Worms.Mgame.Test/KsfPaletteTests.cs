using Microsoft.VisualStudio.TestTools.UnitTesting;
using Syroot.Worms.Mgame;

namespace Syroot.Worms.Test
{
    /// <summary>
    /// Represents a collection of tests for the <see cref="RiffPalette"/> class.
    /// </summary>
    [TestClass]
    public class KsfPaletteTests
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Tests all files found in the test directory.
        /// </summary>
        [TestMethod]
        public void TestKsfPalettes() => Tools.TestFiles<KsfPalette>("*.pal");
    }
}
