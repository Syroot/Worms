using System;
using System.Buffers.Binary;
using System.Globalization;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Syroot.Worms.Armageddon;

namespace Syroot.Worms.Test.Armageddon
{
    /// <summary>
    /// Represents a collection of tests for the <see cref="Scheme"/> class.
    /// </summary>
    [TestClass]
    public class SchemeTests
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Tests all files found in the test directory.
        /// </summary>
        [TestMethod]
        public void TestSchemes() => Tools.TestFiles<Scheme>("*.wsc");

        /// <summary>
        /// Tests all schemes from a Worms Scheme Database CSV dump (ask owner Byte for updated dumps).
        /// </summary>
        [TestMethod]
        public void TestSchemesWsdb()
        {
            // Reuse buffer for all schemes, including the SCHM header stripped in the dump.
            byte[] buffer = new byte[407];
            BinaryPrimitives.WriteInt32LittleEndian(buffer, 0x4D484353);

            using StreamReader csv = new StreamReader(@"Files\Schemes\WA\WSDB\dump.csv");
            while (!csv.EndOfStream)
            {
                string[] cells = csv.ReadLine()!.Split(',');
                int id = Int32.Parse(cells[0]);
                DateTime uploadTime = DateTime.Parse(cells[1], CultureInfo.InvariantCulture);

                string data = cells[2];
                int length = (data.Length - @"\x".Length) / 2;
                for (int i = 0; i < length; i++)
                    buffer[i + 4] = Convert.ToByte(data.Substring(i * 2 + 2, 2), 16);

                using MemoryStream stream = new MemoryStream(buffer, 0, length, false);
                Tools.TestStream<Scheme>($"WSDB {id} / {uploadTime:yyyy-MM-dd HH:mm}", stream);
            }
        }

        /// <summary>
        /// Tests rounding down to the nearest valid fall damage value.
        /// </summary>
        [TestMethod]
        public void RoundFallDamage()
        {
            Scheme scheme = new Scheme();
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => scheme.FallDamage = -1);
            scheme.FallDamage = 0; Assert.AreEqual(0, scheme.FallDamage);
            scheme.FallDamage = 1; Assert.AreEqual(0, scheme.FallDamage);
            scheme.FallDamage = 3; Assert.AreEqual(0, scheme.FallDamage);
            scheme.FallDamage = 4; Assert.AreEqual(4, scheme.FallDamage);
            scheme.FallDamage = 507; Assert.AreEqual(504, scheme.FallDamage);
            scheme.FallDamage = 508; Assert.AreEqual(508, scheme.FallDamage);
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => scheme.FallDamage = 509);
        }

        /// <summary>
        /// Tests rounding down to the nearest valid water rise rate value.
        /// </summary>
        [TestMethod]
        public void RoundWaterRiseRate()
        {
            Scheme scheme = new Scheme();
            scheme.WaterRiseRate = 0; Assert.AreEqual(0, scheme.WaterRiseRate);
            scheme.WaterRiseRate = 1; Assert.AreEqual(0, scheme.WaterRiseRate);
            scheme.WaterRiseRate = 4; Assert.AreEqual(0, scheme.WaterRiseRate);
            scheme.WaterRiseRate = 5; Assert.AreEqual(5, scheme.WaterRiseRate);
            scheme.WaterRiseRate = 252; Assert.AreEqual(245, scheme.WaterRiseRate);
            scheme.WaterRiseRate = 253; Assert.AreEqual(253, scheme.WaterRiseRate);
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => scheme.WaterRiseRate = 254);
        }

        /// <summary>
        /// Tests rounding down to the nearest valid fall damage value.
        /// </summary>
        [TestMethod]
        public void RoundObjectCount()
        {
            Scheme scheme = new Scheme();
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => scheme.ObjectCount = 0);
            scheme.ObjectCount = 1; Assert.AreEqual(1, scheme.ObjectCount);
            scheme.ObjectCount = 2; Assert.AreEqual(2, scheme.ObjectCount);
            scheme.ObjectCount = 30; Assert.AreEqual(30, scheme.ObjectCount);
            scheme.ObjectCount = 31; Assert.AreEqual(30, scheme.ObjectCount);
            scheme.ObjectCount = 34; Assert.AreEqual(30, scheme.ObjectCount);
            scheme.ObjectCount = 35; Assert.AreEqual(35, scheme.ObjectCount);
            scheme.ObjectCount = 99; Assert.AreEqual(95, scheme.ObjectCount);
            scheme.ObjectCount = 100; Assert.AreEqual(100, scheme.ObjectCount);
            scheme.ObjectCount = 101; Assert.AreEqual(100, scheme.ObjectCount);
            scheme.ObjectCount = 109; Assert.AreEqual(100, scheme.ObjectCount);
            scheme.ObjectCount = 110; Assert.AreEqual(110, scheme.ObjectCount);
            scheme.ObjectCount = 249; Assert.AreEqual(240, scheme.ObjectCount);
            scheme.ObjectCount = 250; Assert.AreEqual(250, scheme.ObjectCount);
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => scheme.ObjectCount = 251);
        }
    }
}
