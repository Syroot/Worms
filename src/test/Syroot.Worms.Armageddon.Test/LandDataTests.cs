using Microsoft.VisualStudio.TestTools.UnitTesting;
using Syroot.Worms.Armageddon;

namespace Syroot.Worms.Test.Armageddon
{
    /// <summary>
    /// Represents a collection of tests for the <see cref="LandData"/> class.
    /// </summary>
    [TestClass]
    public class LandDataTests
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Tests all files found in the test directory.
        /// </summary>
        [TestMethod]
        public void TestLandData() => Tools.TestFiles<LandData>("*.dat", new[]
        {
            // Decompressing embedded IMG files fails with extreme IndexOutOfRangeExceptions.
            "WWPA"
        });
    }
}
