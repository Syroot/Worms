using Microsoft.VisualStudio.TestTools.UnitTesting;
using Syroot.Worms.WorldParty;

namespace Syroot.Worms.Test.WorldParty
{
    /// <summary>
    /// Represents a collection of tests for the <see cref="TeamContainer"/> class.
    /// </summary>
    [TestClass]
    public class TeamContainerTests
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Tests all files found in the test directory.
        /// </summary>
        [TestMethod]
        public void TestTeamContainers() => Tools.TestFiles<TeamContainer>("*.wwp");
    }
}
