using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Syroot.Worms.Test
{
    /// <summary>
    /// Represents a collection of tests for the <see cref="Archive"/> class.
    /// </summary>
    [TestClass]
    public class ArchiveTests
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Tests all files found in the test directory.
        /// </summary>
        [TestMethod]
        public void TestArchives() => Tools.TestFiles<Archive>("*.dir");
    }
}
