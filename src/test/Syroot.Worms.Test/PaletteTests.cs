using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Syroot.Worms.Test
{
    /// <summary>
    /// Represents a collection of tests for the <see cref="RiffPalette"/> class.
    /// </summary>
    [TestClass]
    public class PaletteTests
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Tests all files found in the test directory.
        /// </summary>
        [TestMethod]
        public void TestPalettes()
        {
            Tools.TestFiles<RiffPalette>("*.pal", new string[]
            {
                // 4 bytes of trash after data chunk
                "wwp.pal",
                "wwpmaped.pal"
            });
        }
    }
}
