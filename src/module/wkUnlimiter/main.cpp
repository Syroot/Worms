#define WIN32_LEAN_AND_MEAN
#include <stdint.h>
#include <Windows.h>
#include "wkConfig.h"
#include "wkExe.h"
#include "wkUtils.h"

// ---- Initialization ----

BOOL iniAllowMultiInstance;
BOOL iniEnableAllControls;
BOOL iniShowAllControls;
BOOL iniUnlockCamera;
BOOL iniUnlockCursor;

void init(int gameID)
{
	wk::Ini ini("wkUnlimiter.ini");

	// Load INI settings.
	ini.get("Common", "AllowMultiInstance", iniAllowMultiInstance, TRUE);
	ini.get("Frontend", "EnableAllControls", iniEnableAllControls, TRUE);
	ini.get("Frontend", "ShowAllControls", iniShowAllControls, FALSE);
	if (gameID == wk::GAMEID_WA_3_8_CD)
	{
		ini.get("Game", "UnlockCamera", iniUnlockCamera, TRUE);
		ini.get("Game", "UnlockCursor", iniUnlockCursor, TRUE);
	}

	// Ensure INI file has been created with default setting.
	ini.set("Common", "AllowMultiInstance", iniAllowMultiInstance);
	ini.set("Frontend", "EnableAllControls", iniEnableAllControls);
	ini.set("Frontend", "ShowAllControls", iniShowAllControls);
	if (gameID == wk::GAMEID_WA_3_8_CD)
	{
		ini.set("Game", "UnlockCamera", iniUnlockCamera);
		ini.set("Game", "UnlockCursor", iniUnlockCursor);
	}
}

// ---- Patch ----

HANDLE WINAPI Kernel32_CreateSemaphoreA(LPSECURITY_ATTRIBUTES lpSemaphoreAttributes, LONG lInitialCount,
	LONG lMaximumCount, LPCSTR lpName)
{
	HANDLE handle = CreateSemaphoreA(lpSemaphoreAttributes, lInitialCount, lMaximumCount, lpName);
	SetLastError(ERROR_SUCCESS);
	return handle;
}

void patch(wk::Exe& exe, int gameID)
{
	if (iniAllowMultiInstance)
	{
		// Requires WormKitDS as game loads modules after this
		exe.setImp(CreateSemaphoreA, Kernel32_CreateSemaphoreA);
	}
	if (iniEnableAllControls)
	{
		// CWnd::EnableWindow(bEnable) -> CWnd::EnableWindow(TRUE)
		exe.set(exe.find("C2 04 00 8B   49 50 8B 01 FF A0 A8") - 13, 0x9090016A);
	}
	if (iniShowAllControls)
	{
		// CWnd::ShowWindow(bShow) -> CWnd::ShowWindow(TRUE)
		exe.set(exe.find("C2 04 00 8B   49 50 8B 01 FF A0 A0") - 13, 0x9090016A);
	}
	if (iniUnlockCamera)
	{
		exe.setNop(exe.find("89 13  EB 06 3B C7 7E 02"), 2); // X- axis
		exe.setNop(exe.find("89 3B  8B 43 04 3B C1 7D 09 5F 5E"), 2); // X+ axis
		exe.setNop(exe.find("89 4B 04  5B C2 14 00 3B C6 7E 03"), 3); // Y- axis
		exe.setNop(exe.find("89 73 04  5F 5E 5B C2 14 00"), 3); // Y+ axis
	}
	if (iniUnlockCursor)
	{
		exe.setNop(0x00159A72, 6);  // X- axis 1
		exe.setNop(0x00159A8F, 6);  // X+ axis 1
		exe.setNop(0x00159AE5, 6);  // Y+ axis 1
		exe.setNop(0x00159B00, 10); // Y- axis 1 cave
		exe.setNop(0x00159B19, 6);  // Y- axis 1 island
		exe.setNop(0x00159EAD, 6);  // X- axis 2
		exe.setNop(0x00159ECA, 6);  // X+ axis 2
		exe.setNop(0x00159EE7, 6);  // Y+ axis 2
		exe.setNop(0x00159F02, 10); // Y- axis 2 cave
		exe.setNop(0x00159F1B, 6);  // Y- axis 2 island
	}
}

// ---- Main ----

int getVersion(DWORD timeDateStamp)
{
	int id = wk::getGameID(timeDateStamp);
	switch (id)
	{
		case wk::GAMEID_WA_3_6_31:
		case wk::GAMEID_WA_3_7_2_1:
		case wk::GAMEID_WA_3_8_CD:
			return id;
		default:
			return 0;
	}
}

BOOL WINAPI DllMain(HMODULE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
		{
			wk::Exe exe;
			int version = getVersion(exe.FileHeader->TimeDateStamp);
			if (version)
			{
				init(version);
				patch(exe, version);
			}
			else
			{
				MessageBox(NULL, "wkUnlimiter is incompatible with your game version. Please run the 3.6.31.0, "
					"3.7.2.1, or 3.8 CD release of Worms Armageddon. Otherwise, you can delete the module to remove "
					"this warning.", "wkUnlimiter", MB_ICONWARNING);
			}
		}
		break;

		case DLL_PROCESS_DETACH:
			break;
	}
	return TRUE;
}
