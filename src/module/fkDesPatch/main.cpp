#define WIN32_LEAN_AND_MEAN
#include <stdint.h>
#include <Windows.h>
#include "wkExe.h"
#include "wkIni.h"
#include "wkUtils.h"

// ---- Initialization ----

BOOL iniAutoOssett;

void init()
{
	wk::Ini ini("fkDesPatch.ini");

	// Load INI settings.
	ini.get("Frontend", "AutoOssett", iniAutoOssett, FALSE);

	// Ensure INI file has been created with default setting.
	ini.set("Frontend", "AutoOssett", iniAutoOssett);
}

// ---- Patch ----

void patch(wk::Exe& exe, int gameVersion)
{
	if (gameVersion == wk::GAMEID_W2_1_07_TRY)
	{
		
	}
	else
	{
		// getAddress file to code 0x400C02
		if (iniAutoOssett)
			exe.set(0x000446A2, (uint8_t)0xEB);
	}
}

// ---- Main ----

int getVersion(DWORD timeDateStamp)
{
	int id = wk::getGameID(timeDateStamp);
	switch (id)
	{
		case wk::GAMEID_W2_1_05_BR:
		case wk::GAMEID_W2_1_05_EN:
		case wk::GAMEID_W2_1_05_GE:
		case wk::GAMEID_W2_1_05_NA:
		case wk::GAMEID_W2_1_05_SA:
		case wk::GAMEID_W2_1_07_TRY:
			return id;
		default:
			return 0;
	}
}

BOOL WINAPI DllMain(HMODULE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
		{
			wk::Exe exe;
			int version = getVersion(exe.FileHeader->TimeDateStamp);
			if (version)
			{
				init();
				patch(exe, version);
			}
			else
			{
				MessageBox(NULL, "fkDesPatch is incompatible with your game version. Please run the 1.05 patch or 1.07 "
					"release of Worms 2. Otherwise, you can delete the module to remove this warning.", "fkDesPatch",
					MB_ICONWARNING);
			}
		}
		break;

		case DLL_PROCESS_DETACH:
			break;
	}
	return TRUE;
}
