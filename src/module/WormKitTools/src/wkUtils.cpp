#include "wkUtils.h"
#include <string>
#include "wkPatch.h"

namespace wk
{
	int getGameID(DWORD timeDateStamp)
	{
		switch (timeDateStamp)
		{
			case 0x3528DAFA: return GAMEID_W2_1_05_BR;
			case 0x3528DCB1: return GAMEID_W2_1_05_EN;
			case 0x3528DB52: return GAMEID_W2_1_05_GE;
			case 0x3528DA98: return GAMEID_W2_1_05_NA;
			case 0x3528DBDA: return GAMEID_W2_1_05_SA;
			case 0x3587BE19: return GAMEID_W2_1_07_TRY;
			case 0x4CE25091: return GAMEID_WA_3_6_31;
			case 0x513D83BC: return GAMEID_WA_3_7_2_1;
			case 0x5EF04515: return GAMEID_WA_3_8_CD;
			default: return GAMEID_NONE;
		}
	}

	std::string getErrorMessage(int error)
	{
		if (error == ERROR_SUCCESS)
			return std::string();

		LPTSTR buffer = NULL;
		const DWORD cchMsg = FormatMessageA(
			FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_ALLOCATE_BUFFER, NULL,
			error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), reinterpret_cast<LPTSTR>(&buffer), 0, NULL);
		if (cchMsg > 0)
		{
			std::string message(buffer);
			LocalFree(buffer);
			return message;
		}
		else
		{
			CHAR buffer[32];
			sprintf_s(buffer, "Error code 0x%08X.", error);
			return buffer;
		}
	}
}