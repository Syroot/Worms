﻿using System;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.Worms.Mgame
{
    /// <summary>
    /// Represents the two-way encryption and decryption used to obfuscate passwords passed to the client.
    /// </summary>
    public static class PasswordCrypto
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _bufferSize = 64 * sizeof(uint);
        private const string _encryptCharacters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly byte[] _decryptShifts = new byte[]
        {
            0x12, 0x1D, 0x07, 0x19, 0x0F, 0x1F, 0x16, 0x1B, 0x09, 0x1A, 0x03, 0x0D, 0x13, 0x0E, 0x14, 0x0B,
            0x05, 0x02, 0x17, 0x10, 0x0A, 0x18, 0x1C, 0x11, 0x06, 0x1E, 0x00, 0x15, 0x0C, 0x08, 0x04, 0x01
        };
        private static readonly byte[] _encryptShifts = new byte[]
        {
            0x1A, 0x1F, 0x11, 0x0A, 0x1E, 0x10, 0x18, 0x02, 0x1D, 0x08, 0x14, 0x0F, 0x1C, 0x0B, 0x0D, 0x04,
            0x13, 0x17, 0x00, 0x0C, 0x0E, 0x1B, 0x06, 0x12, 0x15, 0x03, 0x09, 0x07, 0x16, 0x01, 0x19, 0x05
        };

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Encrypts the given <paramref name="data"/> with the specified <paramref name="key"/>.
        /// </summary>
        /// <param name="data">The text (typically the password) to encrypt.</param>
        /// <param name="key">The value to apply to the transformation for modified outcome.</param>
        /// <returns>The encrypted text.</returns>
        public static string Encrypt(string data, uint key)
        {
            using MemoryStream inStream = new MemoryStream(new byte[_bufferSize]);

            // Write input into a buffer. Required to loop over the input password end.
            inStream.WriteString(data, StringCoding.ZeroTerminated, Encodings.Korean);
            inStream.Position = 0;
            using MemoryStream outStream = new MemoryStream(new byte[_bufferSize]);

            // Encrypt the contents character by character.
            while (inStream.Position < data.Length)
            {
                // Begin a new dword value at every 7th index.
                uint dword = TransformDword(inStream.ReadUInt32() + key, true);
                for (int j = 0; j < 7; j++)
                {
                    outStream.WriteByte((byte)_encryptCharacters[(int)(dword % _encryptCharacters.Length)]);
                    dword /= (uint)_encryptCharacters.Length;
                }
            }
            // Return the encrypted password as a zero-terminated ASCII string.
            outStream.Position = 0;
            return outStream.ReadString(StringCoding.ZeroTerminated, Encodings.Korean);
        }

        /// <summary>
        /// Decrypts the given <paramref name="data"/> with the specified <paramref name="key"/>.
        /// </summary>
        /// <param name="data">The text (typically the password) to decrypt.</param>
        /// <param name="key">The value to apply to the transformation for modified outcome.</param>
        /// <returns>The decrypted text.</returns>
        public static string Decrypt(string data, uint key)
        {
            using MemoryStream inStream = new MemoryStream(new byte[_bufferSize]);

            // Write input into a buffer. Required to loop over the input password end.
            inStream.WriteString(data, StringCoding.Raw, Encodings.Korean);
            inStream.Position = 0;
            using MemoryStream outStream = new MemoryStream(new byte[_bufferSize]);

            // Decrypt the contents character by character.
            for (int i = 0; i < data.Length; i += 7)
            {
                uint dword = 0;
                for (int j = 0; j < 7; j++)
                {
                    byte b = inStream.Read1Byte();
                    uint op1 = (uint)Math.Pow(_encryptCharacters.Length, j);
                    uint op2 = (uint)(b < '0' || b > '9' ? b - '7' : b - '0');
                    dword += op1 * op2;
                }
                // Finalize a new dword value at every 7th index.
                outStream.WriteUInt32(TransformDword(dword, false) - key);
            }
            // Return the decrypted password as a zero-terminated ASCII string.
            outStream.Position = 0;
            return outStream.ReadString(StringCoding.ZeroTerminated, Encodings.Korean);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static uint TransformDword(uint dword, bool encrypt)
        {
            // Shift least to most significant bits into the resulting value by an amount stored in a one-way array.
            byte[] shifts = encrypt ? _encryptShifts : _decryptShifts;

            uint result = 0;
            int shiftIndex = 0;
            while (dword > 0)
            {
                result += (dword & 1) << shifts[shiftIndex];
                shiftIndex++;
                dword >>= 1;
            }
            return result;
        }
    }
}
