﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Syroot.BinaryData;
using Syroot.Worms.IO;

namespace Syroot.Worms.Mgame
{
    /// <summary>
    /// Represents an LPD layout description file used in Worms World Party Aqua.
    /// </summary>
    public class Lpd : ILoadableFile
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Lpd"/> class, loading data from the file with the given
        /// <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the data from.</param>
        public Lpd(string fileName) => Load(fileName);

        /// <summary>
        /// Initializes a new instance of the <see cref="Lpd"/> class, loading data from the given
        /// <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the data from.</param>
        public Lpd(Stream stream) => Load(stream);

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the list of <see cref="LpdElement"/> instances stored by this class.
        /// </summary>
        public IList<LpdElement> Elements { get; set; } = new List<LpdElement>();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public void Load(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            Load(stream);
        }

        /// <inheritdoc/>
        public void Load(Stream stream)
        {
            int elementCount = stream.ReadInt32();
            Elements = new List<LpdElement>(elementCount);
            while (elementCount-- > 0)
            {
                Elements.Add(new LpdElement
                {
                    FileName = stream.ReadString(StringCoding.Int32CharCount, Encodings.Korean),
                    Rectangle = new Rectangle(stream.ReadInt32(), stream.ReadInt32(), stream.ReadInt32(), stream.ReadInt32()),
                    Properties = stream.ReadInt32(),
                    Version = stream.ReadInt32()
                });
            }
        }
    }
}
