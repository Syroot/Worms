﻿using System;

namespace Syroot.Worms.Mgame
{
    /// <summary>
    /// Represents packet data encryption as used in WWPA. Note that most code is a direct translation from the original
    /// game assembly.
    /// </summary>
    /// <remarks>
    /// The original implementation has the following bugs caused by specific, but rare patterns (in 1000 random blobs
    /// of sizes between 1 and 1000 bytes, ~0.5% were decompressed badly):
    /// - Bad calculation of decompressed bytes. The same wrong bytes are calculated by this implementation.
    /// - Bad negative source buffer indices cause out-of-bounds memory to be written. Combatted in this implementation
    ///   by ignoring such writes and keeping 0 bytes instead.
    /// The game discards decompression results of a length longer than specified in the packet header, but does not
    /// check the data itself if the length is still fine.
    /// </remarks>
    public static class PacketCompression
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static int _field_8 = 0;
        private static int _field_10 = 0;

        private static readonly int[] _bufferDwords = new int[512];

        private static readonly byte[] _buffer = new byte[256];
        private static int _bufferCursor = 0;

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Compresses the given <paramref name="decompressed"/> data.
        /// </summary>
        /// <param name="decompressed">The data to compress.</param>
        /// <returns>The compressed data.</returns>
        public static Span<byte> Compress(ReadOnlySpan<byte> decompressed)
        {
            Compressor compressor = new Compressor();
            int idx;
            int bytesToRepeat;
            int idxDword;
            int i;
            int offset;
            int v11;
            int v12;
            int field_8;
            int field_10;
            byte c;
            int shiftValue1;

            idx = 0;
            while (idx < decompressed.Length)
            {
                bytesToRepeat = idx - 0xFF;
                if (idx - 0xFF < 0)
                    bytesToRepeat = 0;
                idxDword = 0;
                shiftValue1 = -1;
                if (bytesToRepeat >= idx)
                {
                    c = decompressed[idx++];
                    Write(compressor, c);
                }
                else
                {
                    do
                    {
                        for (i = idx; i < decompressed.Length; ++i)
                        {
                            if (i - idx >= 0x111)
                                break;
                            if (decompressed[bytesToRepeat] != decompressed[i])
                                break;
                            ++bytesToRepeat;
                        }
                        offset = idx - i;
                        v11 = i - idx;
                        v12 = offset + bytesToRepeat;
                        if (v11 >= 3 && v11 > idxDword)
                        {
                            idxDword = v11;
                            shiftValue1 = idx - 12;
                            if (v11 == 0x111)
                                break;
                        }
                        bytesToRepeat = v12 + 1;
                    }
                    while (bytesToRepeat < idx);
                    if (idxDword != 0)
                    {
                        TransferBuffer(compressor);
                        compressor.Compress(true);
                        if (idxDword >= 18)
                        {
                            compressor.Shift(0, 4);
                            compressor.Shift(shiftValue1, 8);
                            compressor.Shift(idxDword - 18, 8);
                            field_10 = _field_10;
                            _field_10 = idxDword - 3 + field_10;
                        }
                        else
                        {
                            compressor.Shift(idxDword - 2, 4);
                            compressor.Shift(shiftValue1 - 1, 8);
                            field_8 = _field_8;
                            _field_8 = idxDword - 2 + field_8;
                        }
                        idx += idxDword;
                        ++_bufferDwords[idxDword];
                    }
                    else
                    {
                        c = decompressed[idx++];
                        Write(compressor, c);
                    }
                }
            }
            TransferBuffer(compressor);
            compressor.Compress(true);
            compressor.Shift(0, 4);
            compressor.Shift(0, 8);

            return compressor.Compressed.AsSpan(0, compressor.Cursor);
        }

        public static Span<byte> Decompress(ReadOnlySpan<byte> compressed)
        {
            Decompressor decompressor = new Decompressor(compressed);
            byte[] decompressed = new byte[2048];
            int idx;
            int v5;
            int v6;
            int idxCopySrc;
            int v11;
            int v12;
            int v13;
            int bytesToCopy;

            idx = 0;
            while (true)
            {
                while (true)
                {
                    while (decompressor.Sub_4C0BA0() == 0)
                    {
                        bytesToCopy = decompressor.GetBytesToCopy(8);
                        if (bytesToCopy != -1)
                        {
                            ++bytesToCopy;
                            do
                            {
                                decompressed[idx++] = decompressor.Read();
                                --bytesToCopy;
                            } while (bytesToCopy != 0);
                        }
                    }
                    v5 = decompressor.GetBytesToCopy(4) + 2;
                    if (v5 <= 2)
                        break;
                    v6 = decompressor.GetBytesToCopy(8) + 1;
                    bytesToCopy = v5 - 1;
                    if (v5 != 0)
                    {
                        ++bytesToCopy;
                        do
                        {
                            idxCopySrc = idx++ - v6;
                            --bytesToCopy;
                            if (idxCopySrc >= 0) // Bugfix: Original implementation may calculate negative indices.
                                decompressed[idx - 1] = decompressed[idxCopySrc];
                        } while (bytesToCopy != 0);
                    }
                }
                v11 = decompressor.GetBytesToCopy(8);
                if (v11 == 0)
                    break;
                v12 = decompressor.GetBytesToCopy(8);
                v13 = v12 + 18;
                bytesToCopy = v12 + 17;
                if (v13 != 0)
                {
                    ++bytesToCopy;
                    do
                    {
                        idxCopySrc = idx++ - v11;
                        --bytesToCopy;
                        decompressed[idx - 1] = decompressed[idxCopySrc];
                    } while (bytesToCopy != 0);
                }
            }
            return decompressed.AsSpan(0, idx);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void TransferBuffer(Compressor compressor)
        {
            int bufferCursor;
            int i;

            bufferCursor = _bufferCursor;
            if (bufferCursor != 0)
            {
                compressor.Compress(false);
                compressor.Shift(_bufferCursor - 1, 8);
                for (i = 0; i < _bufferCursor; ++i)
                    compressor.Write(_buffer[i]);
                _bufferCursor = 0;
            }
        }

        private static void Write(Compressor compressor, byte c)
        {
            _buffer[_bufferCursor++] = c;
            if (_bufferCursor >= _buffer.Length)
                TransferBuffer(compressor);
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        private class Compressor
        {
            internal byte[] Compressed = new byte[1024];
            internal int Cursor = 1;

            private int _cursorCompress = 0;
            private int _shift = 0;

            internal void Compress(bool bShift)
            {
                int shift;
                int cursor;
                int cursorCompress;

                if (bShift)
                {
                    Compressed[_cursorCompress] |= (byte)(0x80 >> _shift);
                }
                shift = _shift + 1;
                _shift = shift;
                if (shift == 8)
                {
                    cursor = Cursor;
                    _cursorCompress = Cursor++;
                    cursorCompress = _cursorCompress;
                    Cursor = cursor + 1;
                    Compressed[cursorCompress] = 0;
                    _shift = 0;
                }
            }

            internal void Shift(int value, int shiftPlus1)
            {
                int shift;
                int prevShift;

                shift = shiftPlus1 - 1;
                if (shiftPlus1 != 0)
                {
                    do
                    {
                        Compress(((value >> shift) & 1) != 0);
                        prevShift = shift--;
                    } while (prevShift != 0);
                }
            }

            internal void Write(byte c)
            {
                Compressed[Cursor++] = c;
            }
        }

        private class Decompressor
        {
            private readonly byte[] _compressed;
            private int _compressedCursor = 0;
            private byte _field_C;
            private byte _field_D;

            internal Decompressor(ReadOnlySpan<byte> compressed)
            {
                _compressed = compressed.ToArray();
                byte c = Read();
                _field_C = (byte)(2 * c | 1);
                _field_D = (byte)((uint)c >> 7);
            }

            internal byte Read()
            {
                return _compressed[_compressedCursor++];
            }

            internal int GetBytesToCopy(int numCalls)
            {
                if (numCalls == 0)
                    return 0;
                int bytesToCopy = 0;
                do
                {
                    numCalls--;
                    bytesToCopy = Sub_4C0BA0() + 2 * bytesToCopy;
                } while (numCalls != 0);
                return bytesToCopy;
            }

            internal int Sub_4C0BA0()
            {
                byte field_D = _field_D;
                _field_D = (byte)(_field_C >> 7);
                _field_C *= 2;
                if (_field_C == 0)
                {
                    byte c = Read();
                    _field_C = (byte)(2 * c | 1);
                    _field_D = (byte)(c >> 7);
                }
                return field_D;
            }
        }
    }
}
