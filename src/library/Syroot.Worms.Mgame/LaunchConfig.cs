﻿using System;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Net;
using Syroot.BinaryData;
using Syroot.Worms.IO;

namespace Syroot.Worms.Mgame
{
    /// <summary>
    /// Represents the memory mapped file passed to the game executable, configuring the server address.
    /// </summary>
    public class LaunchConfig
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private string _passwordString = String.Empty;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the name of the publisher providing the launch configuration. Must not exceed 15 characters.
        /// </summary>
        public string Publisher { get; set; } = "MGAME";

        /// <summary>
        /// Gets or sets an unknown value. Must not exceed 19 characters.
        /// </summary>
        public string Unknown { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the address of the game server.
        /// </summary>
        public IPEndPoint ServerEndPoint { get; set; } = new IPEndPoint(IPAddress.Any, 0);

        /// <summary>
        /// Gets or sets the initially entered user name. Must not exceed 250 characters.
        /// </summary>
        public string UserName { get; set; } = String.Empty;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a <see cref="MemoryMappedFile"/> which is then read by the game client.
        /// </summary>
        /// <param name="mapName">The name under which the game client can access the mapping.</param>
        /// <returns>The <see cref="MemoryMappedFile"/>.</returns>
        public MemoryMappedFile CreateMappedFile(string mapName)
        {
            MemoryMappedFile mappedFile = MemoryMappedFile.CreateNew(mapName, 1266,
                MemoryMappedFileAccess.ReadWrite, MemoryMappedFileOptions.None, HandleInheritability.Inheritable);
            using (BinaryStream stream = new BinaryStream(mappedFile.CreateViewStream(),
                encoding: Encodings.Korean, stringCoding: StringCoding.ZeroTerminated))
            {
                stream.WriteFixedString(Publisher, 16);
                stream.WriteFixedString(Unknown, 20);
                stream.WriteFixedString(_passwordString, 30);
                stream.WriteFixedString($"UID={UserName}", 256);
                stream.WriteFixedString(ServerEndPoint.Address.ToString(), 30);
                stream.WriteFixedString(ServerEndPoint.Port.ToString(), 914);
            }
            return mappedFile;
        }

        /// <summary>
        /// Decrypts the password stored in the configuration as previously set through
        /// <see cref="SetPassword(String, Int32)"/>.
        /// </summary>
        public string GetPassword()
        {
            if (String.IsNullOrEmpty(_passwordString))
                return String.Empty;
            string[] parts = _passwordString.Split(';');
            return PasswordCrypto.Decrypt(parts[1], UInt32.Parse(parts[2]));
        }

        /// <summary>
        /// Encrypts the given <paramref name="password"/> with the provided initial encryption <paramref name="key"/>
        /// and stores the result in the launch config.
        /// </summary>
        /// <param name="password">The password to store encrypted.</param>
        /// <param name="key">The key to encrypt with.</param>
        public void SetPassword(string password, int key = 1000)
            => _passwordString = $";{PasswordCrypto.Encrypt(password, (uint)key)};{key}";
    }
}
