﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.Worms.Mgame
{
    /// <summary>
    /// Represents a KSF image container.
    /// </summary>
    public class Ksf // TODO: Implement ILoadableFile
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Ksf"/> class.
        /// </summary>
        public Ksf() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="Ksf"/> class, loading data from the file with the given
        /// <paramref name="fileName"/> and the colors in the provided <paramref name="palette"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the data from.</param>
        /// <param name="palette">The color palette which is indexed by the image data.</param>
        public Ksf(string fileName, KsfPalette palette) => Load(fileName, palette);

        /// <summary>
        /// Initializes a new instance of the <see cref="Ksf"/> class, loading data from the given
        /// <paramref name="stream"/> and the colors in the provided <paramref name="palette"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the data from.</param>
        /// <param name="palette">The color palette which is indexed by the image data.</param>
        public Ksf(Stream stream, KsfPalette palette) => Load(stream, palette);

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the list of converted <see cref="Bitmap"/> instances stored in this KSF.
        /// </summary>
        public IList<KsfImage> Images { get; set; } = new List<KsfImage>();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Loads the data from the file with the given <paramref name="fileName"/> and the colors in the provided
        /// <paramref name="palette"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the data from.</param>
        /// <param name="palette">The color palette which is indexed by the image data.</param>
        public void Load(string fileName, KsfPalette palette)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            Load(stream, palette);
        }

        /// <summary>
        /// Loads the data from the given <paramref name="stream"/> and the colors in the provided
        /// <paramref name="palette"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the data from.</param>
        /// <param name="palette">The color palette which is indexed by the image data.</param>
        public void Load(Stream stream, KsfPalette palette)
        {
            int imageCount = stream.ReadInt32(); // Includes terminator.
            _ = stream.ReadInt32(); // data size

            // Read image headers. Terminating image is of 0 size and data offset at end of data block.
            KsfImage[] images = new KsfImage[imageCount];
            int[] offsets = new int[imageCount];
            Size[] sizes = new Size[imageCount];
            for (int i = 0; i < imageCount; i++)
            {
                KsfImage image = new KsfImage();
                offsets[i] = stream.ReadInt32();
                image.Center = new Point(stream.ReadInt32(), stream.ReadInt32());
                sizes[i] = new Size(stream.ReadInt16(), stream.ReadInt16());
                images[i] = image;
            }

            // Convert images.
            Images = new List<KsfImage>(imageCount);
            long dataStart = stream.Position;
            for (int i = 0; i < imageCount - 1; i++)
            {
                int offset = offsets[i];
                stream.Position = dataStart + offset;
                int dataLength = offsets[i + 1] - offset;
                Size size = sizes[i];
                if (!size.IsEmpty)
                {
                    images[i].RawBitmap = new RawBitmap
                    {
                        BitsPerPixel = 8,
                        Size = size,
                        Palette = palette.Colors,
                        Data = stream.ReadBytes(dataLength)
                    };
                }

                Images.Add(images[i]);
            }
        }
    }
}
