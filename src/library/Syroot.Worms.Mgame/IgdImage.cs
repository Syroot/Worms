﻿using System.Drawing;

namespace Syroot.Worms.Mgame
{
    /// <summary>
    /// Represents an image and its metadata as stored in an <see cref="Igd"/> image container.
    /// </summary>
    public class IgdImage
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int UnknownA { get; set; }
        public int UnknownB { get; set; }
        public int UnknownC { get; set; }
        public Size Size { get; set; }
        public Point Center { get; set; }
        public RawBitmap RawBitmap { get; } = new RawBitmap { BitsPerPixel = 8 };
    }
}
