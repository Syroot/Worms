﻿using System.Drawing;

namespace Syroot.Worms.Mgame
{
    /// <summary>
    /// Represents an image and its metadata as stored in a <see cref="Ksf"/> image container.
    /// </summary>
    public class KsfImage
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Point Center { get; set; }
        public RawBitmap? RawBitmap { get; set; } = null;
    }
}