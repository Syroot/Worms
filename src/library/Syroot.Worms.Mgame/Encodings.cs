﻿using System.Text;

namespace Syroot.Worms.Mgame
{
    /// <summary>
    /// Represents instances of common <see cref="Encoding"/> pages.
    /// </summary>
    public static class Encodings
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>Korean KS C 5601-1987 (codepage 949) encoding.</summary>
        public static readonly Encoding Korean;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        static Encodings()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Korean = Encoding.GetEncoding("ks_c_5601-1987");
        }
    }
}
