using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Syroot.BinaryData;
using Syroot.Worms.IO;

namespace Syroot.Worms.Armageddon
{
    /// <summary>
    /// Represents <see cref="MapGenSettings"/> stored in a LEV file.
    /// Used by WA and WWP. S. https://worms2d.info/Monochrome_map_(.bit,_.lev).
    /// </summary>
    public class GeneratedMap : ILoadableFile, ISaveableFile
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="GeneratedMap"/> class.
        /// </summary>
        public GeneratedMap() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="GeneratedMap"/> class, loading the data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the data from.</param>
        public GeneratedMap(Stream stream) => Load(stream);

        /// <summary>
        /// Initializes a new instance of the <see cref="GeneratedMap"/> class, loading the data from the given file.
        /// </summary>
        /// <param name="fileName">The name of the file to load the data from.</param>
        public GeneratedMap(string fileName) => Load(fileName);

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the <see cref="MapGenSettings"/>.
        /// </summary>
        public MapGenSettings Settings { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public void Load(Stream stream)
        {
            using BinaryStream reader = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);
            Settings = reader.ReadStruct<MapGenSettings>();
        }

        /// <inheritdoc/>>
        public void Load(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            Load(stream);
        }

        /// <inheritdoc/>
        public void Save(Stream stream)
        {
            using BinaryStream writer = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);
            writer.WriteStruct(Settings);
        }

        /// <inheritdoc/>
        public void Save(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
            Save(stream);
        }
    }

    /// <summary>
    /// Represents the required configuration for the land generator to create a map. This structure appears in LEV
    /// files (being their only content), in BIT files (being the header) and in PNG maps stored by WA (being the data
    /// of the w2lv or waLV chunks).
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MapGenSettings
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>Random seed to generate the land shape (no effect in BIT files).</summary>
        public int LandSeed;
        /// <summary>Random seed to generate object placements.</summary>
        public int ObjectSeed;
        /// <summary><see langword="true"/> treats the generated map as a cavern, otherwise as an island, depending on
        /// <see cref="Style"/>.</summary>
        public bool Cavern;
        /// <summary>Style of the land generated, dependending on <see cref="Cavern"/>.</summary>
        public MapGenStyle Style;
        /// <summary><see langword="true"/> to disable an indestructible border being placed around the map.</summary>
        public bool NoIndiBorders;
        /// <summary>Probability percentage of map objects to appear on the land, from 0-100.</summary>
        public int ObjectPercentage;
        /// <summary>Probability percentage of bridges to appear on the land, from 0-100.</summary>
        public int BridgePercentage;
        /// <summary>Height of the water in percent, from 0-99.</summary>
        public int WaterLevel;
        /// <summary>Version of the structure, determining the available <see cref="SoilTextureIndex"/> values in Worms
        /// Armageddon.</summary>
        public MapGenVersion Version;
        /// <summary>Texture index determining the style of the generated map.</summary>
        public MapGenSoil SoilTextureIndex;
        /// <summary>Water color used for the map (deprecated, used only in Worms Armageddon 1.0).</summary>
        public int WaterColor;
    }

    /// <summary>
    /// Represents the possible land styles.
    /// </summary>
    public enum MapGenStyle : int
    {
        /// <summary>A single island or a cavern.</summary>
        OneIslandOrCavern,
        /// <summary>Two islands or a double-layer cavern.</summary>
        TwoIslandsOrCaverns,
        /// <summary>One flat island or a cavern open at the bottom.</summary>
        OneFlatIslandOrCavernOpenBottom,
        /// <summary>Two flat islands or a cavern open at the left or right.</summary>
        TwoFlatIslandsOrCavernOpenLeftRight
    }

    /// <summary>
    /// Represents the possible versions which can be stored in the <see cref="MapGenSettings.Version"/> field.
    /// </summary>
    public enum MapGenVersion : short
    {
        /// <summary>Game version 3.0. Last soil texture indices are 26 = Tribal, 27 = Tribal, 28 = Urban. Additionally,
        /// floor and ceiling trimming is more severe.</summary>
        Version_3_0_0_0 = -1,
        /// <summary>Game version 3.5. Last soil texture indices are 26 = Tribal, 27 = Urban, 28 = undefined.</summary>
        Version_3_5_0_0 = 0,
        /// <summary>Game version 3.6.26.4. Last soil texture indices are 26 = Tools, 27 = Tribal, Urban = 28 (same as
        /// in WWP).</summary>
        Version_3_6_26_4 = 1
    }

    /// <summary>
    /// Represents the possible soil styles. In case of Worms Armageddon, this list matches
    /// <see cref="MapGenVersion.Version_3_6_26_4"/>.
    /// </summary>
    public enum MapGenSoil : short
    {
        ClassicBeach,
        ClassicDesert,
        ClassicFarm,
        ClassicForest,
        ClassicHell,
        Art,
        Cheese,
        Construction,
        Desert,
        Dungeon,
        Easter,
        Forest,
        Fruit,
        Gulf,
        Hell,
        Hospital,
        Jungle,
        Manhattan,
        Medieval,
        Music,
        Pirate,
        Snow,
        Space,
        Sports,
        Tentacle,
        Time,
        Tools,
        Tribal,
        Urban
    }
}
