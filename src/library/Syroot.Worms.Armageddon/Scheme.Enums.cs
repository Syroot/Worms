using System;

namespace Syroot.Worms.Armageddon
{
    /// <summary>
    /// Represents which worms are cured from being poisoned if a worm collects a health crate.
    /// </summary>
    public enum HealthCure : byte
    {
        /// <summary>Cures only the worm collecting the health crate.</summary>
        Single,
        /// <summary>Cures the team of the worm collecting the health crate.</summary>
        Team,
        /// <summary>Cures all allied teams of the worm collecting the health crate.</summary>
        Allies,
        /// <summary>Cures nobody.</summary>
        Nobody = Byte.MaxValue
    }

    /// <summary>
    /// Represents the types of objects which can appear on the map.
    /// </summary>
    [Flags]
    public enum MapObjectType
    {
        /// <summary>Neither mines or oil drums are placed on the map.</summary>
        None,
        /// <summary>Only mines are placed on the map.</summary>
        Mines = 1 << 0,
        /// <summary>Only oil drums are placed on the map.</summary>
        OilDrums = 1 << 1,
        /// <summary>Both mines and oil drums are placed on the map.</summary>
        Both = Mines | OilDrums
    }

    /// <summary>
    /// Represents the possible cavern roof extension directions.
    /// </summary>
    public enum Roofing : byte
    {
        /// <summary>Does not extend the roof.</summary>
        Default,
        /// <summary>Extends the roof infinitely upwards.</summary>
        Up,
        /// <summary>Extends the roof infinitely up- and sidewards.</summary>
        UpSide
    }

    /// <summary>
    /// Gets or sets a value determining which kinds of weapons can be dropped while rolling during roping.
    /// </summary>
    public enum RopeRollDrops : byte
    {
        /// <summary>No weapons can be dropped while rolling.</summary>
        None,
        /// <summary>Weapons which are droppable from a rope can be dropped while rolling.</summary>
        Rope,
        /// <summary>Weapons which are droppable from a rope or while jumping can be dropped while rolling.</summary>
        RopeJump
    }

    /// <summary>
    /// Represents the known versions of scheme file formats.
    /// </summary>
    public enum SchemeVersion : byte
    {
        /// <summary>WA 3.0 and WWP, original format.</summary>
        Version1 = 1,
        /// <summary>WA 3.5 and newer, storing super weapons.</summary>
        Version2 = 2,
        /// <summary>WA 3.7.3 and newer, storing new options.</summary>
        Version3 = 3
    }

    /// <summary>
    /// Represents extension methods for <see cref="SchemeVersion"/> instances.
    /// </summary>
    public static class SchemeVersionExtensions
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Returns how many <see cref="SchemeWeapon"/> instances are stored in the <paramref name="version"/>.
        /// </summary>
        /// <param name="version">The <see cref="SchemeVersion"/>.</param>
        /// <returns>The number of weapons stored for this version.</returns>
        public static int GetWeaponCount(this SchemeVersion version) => version == SchemeVersion.Version1 ? 45 : 64;
    }

    /// <summary>
    /// Represents the gravity simulation used in a game (as originating from RubberWorm).
    /// </summary>
    public enum RwGravityType : byte
    {
        /// <summary>No RubberWorm gravity logic is used.</summary>
        None,
        /// <summary>Default gravity, but affecting more objects.</summary>
        Default,
        /// <summary>Gravity is calculated as a constant force black hole.</summary>
        BlackHoleConstant,
        /// <summary>Gravity is calculated as a linear force / proportional black hole.</summary>
        BlackHoleLinear
    }

    /// <summary>
    /// Represents the known identifiers of scheme editors stored in schemes modified by them.
    /// </summary>
    public enum SchemeEditor : byte
    {
        /// <summary>Default (game editor or those that do not set a recognition byte).</summary>
        None,
        /// <summary>LeTotalKiller's PHP online editor.</summary>
        LeTotalKiller = 0x5F,
        /// <summary>SchemeEddy's Win32 editor.</summary>
        SchemeEddy = 0x89
    }

    /// <summary>
    /// Represents the effects an enabled <see cref="Scheme.SheepHeaven"/> has on the game.
    /// </summary>
    [Flags]
    public enum SheepHeavenFlags : byte
    {
        /// <summary>Crates always eject a sheep when exploding.</summary>
        Explode = 1 << 0,
        /// <summary>Extended fuse timer on sheep weapons.</summary>
        Fuse = 1 << 1,
        /// <summary>Crates have higher probability to contain sheep weapons.</summary>
        Odds = 1 << 2,
        /// <summary>Enabled all of the sheep heaven effects.</summary>
        All = Explode | Fuse | Odds
    }

    /// <summary>
    /// Represents the possible loss of control when skimming on water.
    /// </summary>
    public enum SkimControlLoss : byte
    {
        /// <summary>Control is lost when skimming.</summary>
        Lost,
        /// <summary>Control is retained but the rope cannot be shot again until landing.</summary>
        Kept,
        /// <summary>Control is retained and the rope can be shot even before landing.</summary>
        KeptWithRope
    }

    /// <summary>
    /// Represents the possible methods of triggering skip walking to speed up the walking speed.
    /// </summary>
    public enum SkipWalk : byte
    {
        /// <summary>Skip walking is possible by right clicking during the turn.</summary>
        Default,
        /// <summary>Skip walking is possible by using the up and down arrow keys.</summary>
        Facilitated,
        /// <summary>Skip walking is prevented.</summary>
        Disabled = Byte.MaxValue
    }

    /// <summary>
    /// Represents the stockpiling mode of weapon armory between rounds.
    /// </summary>
    public enum Stockpiling : byte
    {
        /// <summary>Each round starts with the exact amount of weapons set in the scheme.</summary>
        Off,
        /// <summary>Weapons collected additionally to the ones set in the scheme are transfered to the next round.</summary>
        On,
        /// <summary>Weapons are only given out at start of first round, and not renewed in following rounds.</summary>
        Anti
    }

    /// <summary>
    /// Represents the method to determine the next turn's worm.
    /// </summary>
    public enum WormSelect : byte
    {
        /// <summary>Worms are selected in the order in which they appear in the team.</summary>
        Sequential,
        /// <summary>Worms are selected by the player.</summary>
        Manual,
        /// <summary>Worms are selected randomly.</summary>
        Random
    }

    /// <summary>
    /// Represents the event triggered when the round timer runs out.
    /// </summary>
    public enum SuddenDeathEvent : byte
    {
        /// <summary>The round ends and is drawn.</summary>
        RoundEnd,
        /// <summary>A nuklear test is triggered.</summary>
        NuclearStrike,
        /// <summary>The worms health is reduced to 1.</summary>
        HealthDrop,
        /// <summary>No special event is triggered and the water only rises in the rate specified in the scheme.</summary>
        WaterRise
    }

    /// <summary>
    /// Represents the objects or effects worms phase through.
    /// </summary>
    public enum WormPhasing : byte
    {
        /// <summary>Worms are affected as usual.</summary>
        None,
        /// <summary>Worms are not affected by other worms.</summary>
        Worms,
        /// <summary>Worms are not affected by other worms and weapons.</summary>
        WormsWeapons,
        /// <summary>Worms are not affected by other worms, weapons, and damage.</summary>
        WormsWeaponsDamage
    }

    /// <summary>
    /// Represents the possible loss of control on high speed horizontal collisions.
    /// </summary>
    public enum XImpactControlLoss : byte
    {
        /// <summary>Control is lost.</summary>
        Loss,
        /// <summary>Control is retained.</summary>
        Kept = Byte.MaxValue
    }
}
