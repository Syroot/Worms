using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Syroot.BinaryData;
using Syroot.Worms.IO;

namespace Syroot.Worms.Armageddon
{
    /// <summary>
    /// Represents the list of teams and unlocked game features stored in WGT files.
    /// Used by WA. S. https://worms2d.info/Team_file.
    /// </summary>
    public class TeamContainer : ILoadableFile, ISaveableFile
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const string _signature = "WGT"; // 0-terminated.

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamContainer"/> class.
        /// </summary>
        public TeamContainer() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamContainer"/> class, loading the data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the data from.</param>
        public TeamContainer(Stream stream) => Load(stream);

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamContainer"/> class, loading the data from the given file.
        /// </summary>
        /// <param name="fileName">The name of the file to load the data from.</param>
        public TeamContainer(string fileName) => Load(fileName);

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets a value possibly indicating a version of the file format.
        /// </summary>
        public byte Version { get; set; }

        /// <summary>
        /// Gets or sets the unlocked utilities, weapon upgrades, and game cheats.
        /// </summary>
        public UnlockedFeatures UnlockedFeatures { get; set; }

        /// <summary>
        /// Gets or sets an unknown value.
        /// </summary>
        public byte Unknown { get; set; }

        /// <summary>
        /// Gets or sets the list of <see cref="Team"/> instances stored.
        /// </summary>
        public IList<Team> Teams { get; set; } = new List<Team>();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public void Load(Stream stream)
        {
            using BinaryStream reader = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);

            // Read the header.
            if (reader.ReadString(StringCoding.ZeroTerminated) != _signature)
                throw new InvalidDataException("Invalid WGT file signature.");
            Version = reader.Read1Byte(); // Really version?

            // Read global settings.
            byte teamCount = reader.Read1Byte();
            UnlockedFeatures = reader.ReadEnum<UnlockedFeatures>(false);
            Unknown = reader.Read1Byte();

            // Read the teams.
            Teams = new List<Team>();
            while (teamCount-- > 0)
                Teams.Add(reader.Load<Team>());
        }

        /// <inheritdoc/>
        public void Load(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            Load(stream);
        }

        /// <inheritdoc/>
        public void Save(Stream stream)
        {
            using BinaryStream writer = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);

            // Write the header.
            writer.Write(_signature, StringCoding.ZeroTerminated);
            writer.Write(Version);

            // Write global settings.
            writer.Write((byte)Teams.Count);
            writer.WriteEnum(UnlockedFeatures, false);
            writer.Write(Unknown);

            // Write the teams.
            foreach (Team team in Teams)
                team.Save(writer.BaseStream);
        }

        /// <inheritdoc/>
        public void Save(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
            Save(stream);
        }
    }

    /// <summary>
    /// Represents unlockable features of the game.
    /// </summary>
    [Flags]
    public enum UnlockedFeatures : int
    {
        /// <summary>No features have been unlocked yet.</summary>
        None,
        /// <summary>The utility weapon Laser Sight can be configured.</summary>
        UtilityLaserSight = 1 << 0,
        /// <summary>The utility weapon Fast Walk can be configured.</summary>
        UtilityFastWalk = 1 << 1,
        /// <summary>The utility weapon Invisibility can be configured.</summary>
        UtilityInvisibility = 1 << 2,
        /// <summary>The utility weapon Low Gravity can be configured.</summary>
        UtilityLowGravity = 1 << 3,
        /// <summary>The utility weapon Jetpack can be configured.</summary>
        UtilityJetpack = 1 << 4,
        /// <summary>The Grenade upgrade can be enabled.</summary>
        UpgradedGrenade = 1 << 8,
        /// <summary>The Shotgun upgrade can be enabled.</summary>
        UpgradedShotgun = 1 << 9,
        /// <summary>The cluster upgrade can be enabled.</summary>
        UpgradedClusters = 1 << 10,
        /// <summary>The Longbow upgrade can be enabled.</summary>
        UpgradedLongbow = 1 << 11,
        /// <summary>The upgrade of Super Sheeps to become Aqua Sheeps can be enabled.</summary>
        AquaSheep = 1 << 12,
        /// <summary>Worms can have infinite health and are killable only by drowning them.</summary>
        GodWorms = 1 << 16,
        /// <summary>Blood effects when hitting worms can be enabled.</summary>
        BloodFx = 1 << 17,
        /// <summary>Every crate explodes with a sheep.</summary>
        SheepHeaven = 1 << 18,
        /// <summary>Map terrain can be indestructible and Full Wormage scheme is accessible.</summary>
        IndestructibleAndFullWormage = 1 << 24
    }
}
