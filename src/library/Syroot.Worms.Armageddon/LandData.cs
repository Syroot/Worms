using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using Syroot.BinaryData;
using Syroot.Worms.IO;

namespace Syroot.Worms.Armageddon
{
    /// <summary>
    /// Represents map configuration stored by the land generator in LAND.DAT files.
    /// Used by WA and WWP. S. https://worms2d.info/Land_Data_file.
    /// </summary>
    public class LandData : ILoadableFile, ISaveableFile
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _alignImgData;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="LandData"/> class.
        /// </summary>
        public LandData() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="LandData"/> class, loading the data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the data from.</param>
        public LandData(Stream stream) => Load(stream);

        /// <summary>
        /// Initializes a new instance of the <see cref="LandData"/> class, loading the data from the given file.
        /// </summary>
        /// <param name="fileName">The name of the file to load the data from.</param>
        public LandData(string fileName) => Load(fileName);

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the format version of the file.
        /// </summary>
        public LandDataVersion Version { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether image data is aligned by 4-bytes. This is the case only for WWP.
        /// </summary>
        public bool AlignImgData
        {
            get => _alignImgData;
            set => _alignImgData = value;
        }

        /// <summary>
        /// Gets or sets the size of the landscape in pixels.
        /// </summary>
        public Size Size { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an indestructible top border will be enabled.
        /// </summary>
        public bool TopBorder { get; set; }

        /// <summary>
        /// Gets or sets the height of the water in pixels.
        /// </summary>
        public int WaterHeight { get; set; }

        /// <summary>
        /// Gets or sets an unknown value only available in newer W:A versions (apparently since 3.6.28.0).
        /// </summary>
        public int? Unknown { get; set; }

        /// <summary>
        /// Gets or sets an array of coordinates at which objects can be placed.
        /// </summary>
        public IList<Point> ObjectLocations { get; set; } = new List<Point>();

        /// <summary>
        /// Gets or sets the visual foreground image.
        /// </summary>
        public Img Foreground { get; set; } = new Img();

        /// <summary>
        /// Gets or sets the collision mask of the landscape.
        /// </summary>
        public Img CollisionMask { get; set; } = new Img();

        /// <summary>
        /// Gets or sets the visual background image.
        /// </summary>
        public Img Background { get; set; } = new Img();

        /// <summary>
        /// Gets or sets the path to the land image file.
        /// </summary>
        public string LandTexturePath { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the path to the Water.dir file.
        /// </summary>
        public string WaterDirPath { get; set; } = String.Empty;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public void Load(Stream stream)
        {
            using BinaryStream reader = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);

            int readLocationCount()
            {
                int oldLocationCount = reader.ReadInt32();
                int oldImgStart = oldLocationCount * Unsafe.SizeOf<Point>();

                // Check whether the old IMG start would be invalid.
                bool isNew = false;
                using (reader.TemporarySeek(oldImgStart))
                    isNew = reader.EndOfStream || reader.ReadUInt32() != Img.Signature;
                if (isNew)
                {
                    Unknown = oldLocationCount;
                    return reader.ReadInt32();
                }
                else
                {
                    Unknown = null;
                    return oldLocationCount;
                }
            }

            Img readImage(ref bool aligned)
            {
                long imgStart = reader.Position;
                Img img = new Img(reader.BaseStream, aligned);

                // If reading the next image fails, the previous data was aligned and must be reread.
                if (!aligned)
                {
                    if (reader.ReadUInt32() == Img.Signature)
                    {
                        reader.Position -= sizeof(uint);
                    }
                    else
                    {
                        reader.Position = imgStart;
                        img = new Img(reader.BaseStream, true);
                        aligned = true;
                    }
                }

                return img;
            }

            // Read the header.
            Version = reader.ReadEnum<LandDataVersion>(true);
            int fileSize = reader.ReadInt32();

            // Read the data.
            Size = reader.ReadStruct<Size>();
            TopBorder = reader.ReadBoolean(BooleanCoding.Dword);
            WaterHeight = reader.ReadInt32();

            // Read the object locations.
            int locationCount = readLocationCount();
            ObjectLocations = new List<Point>(locationCount);
            for (int i = 0; i < locationCount; i++)
                ObjectLocations.Add(reader.ReadStruct<Point>());

            // Read the image data, which alignment must be detected by trial-and-error.
            _alignImgData = Version == LandDataVersion.WormsWorldPartyAqua;
            long foreImgLocation = reader.Position;
            Foreground = readImage(ref _alignImgData);
            CollisionMask = readImage(ref _alignImgData);
            Background = new Img(reader.BaseStream, _alignImgData);

            // Read the file paths.
            LandTexturePath = reader.ReadString(StringCoding.ByteCharCount);
            WaterDirPath = reader.ReadString(StringCoding.ByteCharCount);
        }

        /// <inheritdoc/>
        public void Load(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            Load(stream);
        }

        /// <inheritdoc/>
        public void Save(Stream stream)
        {
            using BinaryStream writer = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);

            // Write the header.
            writer.WriteEnum(Version, true);
            uint fileSizeOffset = writer.ReserveOffset();

            // Write the data.
            writer.WriteStruct(Size);
            writer.Write(TopBorder, BooleanCoding.Dword);
            writer.Write(WaterHeight);
            if (Unknown.HasValue)
                writer.Write(Unknown.Value);

            // Write the possible object coordinate array.
            writer.Write(ObjectLocations.Count);
            for (int i = 0; i < ObjectLocations.Count; i++)
                writer.WriteStruct(ObjectLocations[i]);

            // Write the image data.
            Foreground.Save(writer.BaseStream, _alignImgData);
            CollisionMask.Save(writer.BaseStream, _alignImgData);
            Background.Save(writer.BaseStream, _alignImgData);

            // Write the file paths.
            writer.Write(LandTexturePath, StringCoding.ByteCharCount);
            writer.Write(WaterDirPath, StringCoding.ByteCharCount);

            writer.SatisfyOffset(fileSizeOffset, (uint)writer.Position);
        }

        /// <inheritdoc/>
        public void Save(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
            Save(stream);
        }
    }

    // ---- CONSTANTS ----------------------------------------------------------------------------------------------

    /// <summary>
    /// Represents the version of <see cref="LandData"/> files, encoded as the signature.
    /// </summary>
    public enum LandDataVersion : uint
    {
        /// <summary>Found in W2, WA, WWP, and OW.</summary>
        Team17 = 0x1A444E4C, // "LND\x1A"
        /// <summary>Found only in WWPA.</summary>
        WormsWorldPartyAqua = 0x1B444E4C // "LND\x1B"
    }
}
