﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Syroot.Worms.Armageddon
{
    partial class Scheme
    {
        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        /// <summary>
        /// Represents the blob of extended scheme options attached in <see cref="SchemeVersion.Version3"/>
        /// <see cref="Scheme"/> instances.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ExtendedOptions : IEquatable<ExtendedOptions>
        {
            /// <summary>Unchanged settings as available in a new scheme.</summary>
            public static readonly ExtendedOptions Default = new ExtendedOptions
            {
                Wind = 100,
                WindBias = 15,
                Gravity = 0.24f,
                Friction = 0.96f,
                RopeKnockForce = null,
                BloodAmount = null,
                NoCrateProbability = null,
                CrateLimit = 5,
                SuddenDeathNoWormSelect = true,
                SuddenDeathTurnDamage = 5,
                ObjectPushByExplosion = null,
                UndeterminedCrates = null,
                UndeterminedMineFuse = null,
                DrillImpartsVelocity = null,
                FlameTurnDecay = 13106 / 65536f, // 0.19999
                FlameTouchDecay = 30,
                FlameLimit = 200,
                ProjectileMaxSpeed = 32,
                RopeMaxSpeed = 16,
                JetpackMaxSpeed = 5,
                GameSpeed = 1,
                IndianRopeGlitch = null,
                HerdDoublingGlitch = null,
                JetpackBungeeGlitch = true,
                AngleCheatGlitch = true,
                GlideGlitch = true,
                FloatingWeaponGlitch = true,
                RwGravity = 1,
                TerrainOverlapGlitch = null,
                HealthCure = HealthCure.Team,
                SheepHeavenFlags = SheepHeavenFlags.All,
                DoubleTimeCount = 1
            };

            // Raw structure to read and write as one blob of dynamic length.
#pragma warning disable IDE0032 // Use auto property, fields are required for struct layout.
            private uint _dataVersion;
            private bool _constantWind;
            private short _wind;
            private byte _windBias;
            private int _gravity; // fixed-point
            private int _friction; // fixed-point
            private byte _ropeKnockForce;
            private byte _bloodAmount;
            private bool _ropeUpgrade;
            private bool _groupPlaceAllies;
            private byte _noCrateProbability;
            private ushort _crateLimit;
            private bool _suddenDeathNoWormSelect;
            private byte _suddenDeathTurnDamage;
            private WormPhasing _wormPhasingAlly;
            private WormPhasing _wormPhasingEnemy;
            private bool _circularAim;
            private bool _antiLockAim;
            private bool _antiLockPower;
            private bool _wormSelectKeepHotSeat;
            private bool _wormSelectAnytime;
            private bool _battyRope;
            private RopeRollDrops _ropeRollDrops;
            private XImpactControlLoss _keepControlXImpact;
            private bool _keepControlHeadBump;
            private SkimControlLoss _keepControlSkim;
            private bool _explosionFallDamage;
            private byte _objectPushByExplosion; // tri-state
            private byte _undeterminedCrates; // tri-state
            private byte _undeterminedMineFuse; // tri-state
            private bool _firingPausesTimer;
            private bool _loseControlDoesntEndTurn;
            private bool _shotDoesntEndTurn;
            private bool _shotDoesntEndTurnAll;
            private byte _drillImpartsVelocity; // tri-state
            private bool _girderRadiusAssist;
            private ushort _flameTurnDecay; // fixed-point fraction
            private byte _flameTouchDecay;
            private ushort _flameLimit;
            private int _projectileMaxSpeed; // fixed-point
            private int _ropeMaxSpeed; // fixed-point
            private int _jetpackMaxSpeed; // fixed-point
            private int _gameSpeed; // fixed-point
            private byte _indianRopeGlitch; // tri-state
            private byte _herdDoublingGlitch; // tri-state
            private bool _jetpackBungeeGlitch;
            private bool _angleCheatGlitch;
            private bool _glideGlitch;
            private SkipWalk _skipWalk;
            private Roofing _roofing;
            private bool _floatingWeaponGlitch;
            private int _wormBounce; // fixed-point
            private int _viscosity; // fixed-point
            private bool _viscosityWorms;
            private int _rwWind; // fixed-point
            private bool _rwWindWorms;
            private RwGravityType _rwGravityType;
            private int _rwGravity; // fixed-point
            private byte _crateRate;
            private bool _crateShower;
            private bool _antiSink;
            private bool _weaponsDontChange;
            private bool _extendedFuse;
            private bool _autoReaim;
            private byte _terrainOverlapGlitch; // tri-state
            private bool _roundTimeFractional;
            private bool _autoRetreat;
            private HealthCure _healthCure;
            private byte _kaosMod;
            private SheepHeavenFlags _sheepHeavenFlags;
            private bool _conserveUtilities;
            private bool _expediteUtilities;
            private byte _doubleTimeCount;
#pragma warning restore IDE0032

            /// <summary>
            /// Gets or sets the versioning of the extended options.
            /// </summary>
            public uint DataVersion
            {
                get => _dataVersion;
                set => _dataVersion = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether <see cref="Wind"/> determines a constant wind value.
            /// </summary>
            public bool ConstantWind
            {
                get => _constantWind;
                set => _constantWind = value;
            }

            /// <summary>
            /// Gets or sets the wind strength. If <see cref="ConstantWind"/> is <see langword="true"/>, this determines the
            /// fixed direction and strength (negative values pointing to the left). If <see cref="ConstantWind"/> is
            /// <see langword="false"/>, this determines the maximum allowed random strength, where negative values invert
            /// the randomly generated strength. Default is 100.
            /// </summary>
            public short Wind
            {
                get => _wind;
                set => _wind = value;
            }

            /// <summary>
            /// Gets or sets the likelyness of worms near sides of the map getting wind pointing towards the center of the
            /// map. Default is 15.
            /// </summary>
            public byte WindBias
            {
                get => _windBias;
                set => _windBias = value;
            }

            /// <summary>
            /// Gets or sets the acceleration due to gravity in pixels per frame per frame. Valid values lie between
            /// 1 / 65535 and 200, where 0.24 is the default.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value lies outside of 1 / 65535 or 200.</exception>
            public float Gravity
            {
                get => FixedPointToSingle(_gravity);
                set
                {
                    if (value < 1 / 65535f || value > 200)
                        throw new ArgumentOutOfRangeException(nameof(value), "Gravity must lie between 1 / 65535 and 200.");
                    _gravity = SingleToFixedPoint(value);
                }
            }

            /// <summary>
            /// Gets or sets the friction deaccelerating objects touching solid ground. Valud values lie between 0 and 2.55,
            /// where 0.96 is default, 1 is no friction, and 0 an immediate stop.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value lies outside of 0 or 2.55.</exception>
            /// <remarks>Configurable with the /friction(value * 100) command in RubberWorm.</remarks>
            public float Friction
            {
                get => FixedPointToSingle(_friction);
                set
                {
                    if (value < 0 || value > 2.55f)
                        throw new ArgumentOutOfRangeException(nameof(value), "Friction must lie between 0 and 2.55.");
                    _friction = SingleToFixedPoint(value);
                }
            }

            /// <summary>
            /// Gets or sets the rope knocking power in percent, where 100 is the default power.
            /// </summary>
            /// <remarks>Configurable with the /knock command in RubberWorm.</remarks>
            public byte? RopeKnockForce
            {
                get => TriStateToNullByte(_ropeKnockForce);
                set => _ropeKnockForce = NullByteToTriState(value);
            }

            /// <summary>
            /// Gets or sets the amount of blood emitted from worms.
            /// </summary>
            public byte? BloodAmount
            {
                get => TriStateToNullByte(_bloodAmount);
                set => _bloodAmount = NullByteToTriState(value);
            }

            /// <summary>
            /// Gets or sets a value indicating whether the Ninja Rope is more powerful.
            /// </summary>
            /// <remarks>Configurable with the /ir or /rope+ commands in RubberWorm.</remarks>
            public bool RopeUpgrade
            {
                get => _ropeUpgrade;
                set => _ropeUpgrade = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether automatically placed worms are attempted to be grouped by allies.
            /// </summary>
            public bool GroupPlaceAllies
            {
                get => _groupPlaceAllies;
                set => _groupPlaceAllies = value;
            }

            /// <summary>
            /// Gets or sets the probability of no crate falling at the start of a given turn. Valid values lie between 0
            /// and 100.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value lies outside of 0 or 100.</exception>
            public byte? NoCrateProbability
            {
                get => TriStateToNullByte(_noCrateProbability);
                set
                {
                    if (value > 100)
                        throw new ArgumentOutOfRangeException(nameof(value), "No crate probability must not be bigger than 100.");
                    _noCrateProbability = NullByteToTriState(value);
                }
            }

            /// <summary>
            /// Gets or sets the maximum number of crates existing on the map at the same time. Default is 5.
            /// </summary>
            /// <remarks>Configurable with the /cratelimit(value) command in RubberWorm.</remarks>
            public ushort CrateLimit
            {
                get => _crateLimit;
                set => _crateLimit = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether worm select is disabled during Sudden Death.
            /// </summary>
            public bool SuddenDeathNoWormSelect
            {
                get => _suddenDeathNoWormSelect;
                set => _suddenDeathNoWormSelect = value;
            }

            /// <summary>
            /// Gets or sets the health reducement between turns applied to every worm during Sudden Death. The default is
            /// 5.
            /// </summary>
            public byte SuddenDeathTurnDamage
            {
                get => _suddenDeathTurnDamage;
                set => _suddenDeathTurnDamage = value;
            }

            /// <summary>
            /// Gets or sets a value determining through which objects or effects worms of allied teams can phase.
            /// </summary>
            public WormPhasing WormPhasingAlly
            {
                get => _wormPhasingAlly;
                set => _wormPhasingAlly = value;
            }

            /// <summary>
            /// Gets or sets a value determining through which objects or effects worms of enemy teams can phase.
            /// </summary>
            public WormPhasing WormPhasingEnemy
            {
                get => _wormPhasingEnemy;
                set => _wormPhasingEnemy = value;
            }

            /// <summary>
            /// Gets or sets a value whether the aim of a weapon loops in full rather than half circles.
            /// </summary>
            /// <remarks>Configurable with the /cira command in RubberWorm.</remarks>
            public bool CircularAim
            {
                get => _circularAim;
                set => _circularAim = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether a weapons aim is reset during turns to prevent easy repeated shots.
            /// </summary>
            public bool AntiLockAim
            {
                get => _antiLockAim;
                set => _antiLockAim = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether power is unlocked like in TestStuff.
            /// </summary>
            /// <remarks>Configurable with the /alp command in RubberWorm.</remarks>
            public bool AntiLockPower
            {
                get => _antiLockPower;
                set => _antiLockPower = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether worm select does not cancel the <see cref="HotSeatTime"/>.
            /// </summary>
            public bool WormSelectKeepHotSeat
            {
                get => _wormSelectKeepHotSeat;
                set => _wormSelectKeepHotSeat = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether worms can be selected at any time during the turn, as long as worm
            /// selection is activated.
            /// </summary>
            /// <remarks>Configurable with the /swat command in RubberWorm.</remarks>
            public bool WormSelectAnytime
            {
                get => _wormSelectAnytime;
                set => _wormSelectAnytime = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether Batty Rope features are enabled, allowing Jetpack, Bungee or Ninja
            /// Rope to stay equipped and attached between turns.
            /// </summary>
            public bool BattyRope
            {
                get => _battyRope;
                set => _battyRope = value;
            }

            /// <summary>
            /// Gets or sets a value determining which kinds of weapons can be dropped from a rope while rolling.
            /// </summary>
            public RopeRollDrops RopeRollDrops
            {
                get => _ropeRollDrops;
                set => _ropeRollDrops = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether control of the worm is retained after high speed X axis collisions.
            /// </summary>
            public XImpactControlLoss KeepControlXImpact
            {
                get => _keepControlXImpact;
                set => _keepControlXImpact = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether control of the worm is retained after bumping its head while roping.
            /// </summary>
            public bool KeepControlHeadBump
            {
                get => _keepControlHeadBump;
                set => _keepControlHeadBump = value;
            }

            /// <summary>
            /// Gets or sets a value determining to which extent control of the worm is lost when skimming on water.
            /// </summary>
            public SkimControlLoss KeepControlSkim
            {
                get => _keepControlSkim;
                set => _keepControlSkim = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether a worm will suffer fall damage in addition to the usual explosion
            /// damage when they are thrown by explosions.
            /// </summary>
            public bool ExplosionFallDamage
            {
                get => _explosionFallDamage;
                set => _explosionFallDamage = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether all objects are pushed by explosions near them.
            /// </summary>
            /// <remarks>Configurable with the /ope command in RubberWorm.</remarks>
            public bool? ObjectPushByExplosion
            {
                get => TriStateToNullBool(_objectPushByExplosion);
                set => _objectPushByExplosion = NullBoolToTriState(value);
            }

            /// <summary>
            /// Gets or sets a value indicating whether the weapon contained in a crate is determined only when collecting
            /// it or the Crate Spy utility to prevent reading it from memory ahead of time.
            /// </summary>
            public bool? UndeterminedCrates
            {
                get => TriStateToNullBool(_undeterminedCrates);
                set => _undeterminedCrates = NullBoolToTriState(value);
            }

            /// <summary>
            /// Gets or sets a value indicating whether the mine fuse is determined only when triggering it to prevent
            /// reading it from memory ahead of time.
            /// </summary>
            public bool? UndeterminedMineFuse
            {
                get => TriStateToNullBool(_undeterminedMineFuse);
                set => _undeterminedMineFuse = NullBoolToTriState(value);
            }

            /// <summary>
            /// Gets or sets a value indicating whether the turn timer is paused while launching a weapon.
            /// </summary>
            /// <remarks>Configurable with the /fdpt or /nopause commands in RubberWorm.</remarks>
            public bool FiringPausesTimer
            {
                get => _firingPausesTimer;
                set => _firingPausesTimer = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the loss of control while moving a worm ends does no longer end the
            /// turn and allows the player to continue moving.
            /// </summary>
            /// <remarks>Configurable with the /ldet or /stoicworm commands in RubberWorm.</remarks>
            public bool LoseControlDoesntEndTurn
            {
                get => _loseControlDoesntEndTurn;
                set => _loseControlDoesntEndTurn = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether using a weapon does no longer end the turn and allows to shoot
            /// multiple weapons in one turn.
            /// </summary>
            /// <remarks>Configurable with the /sdet or /multishot commands in RubberWorm.</remarks>
            public bool ShotDoesntEndTurn
            {
                get => _shotDoesntEndTurn;
                set => _shotDoesntEndTurn = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the weapons Armageddon, Earthquake and Indian Nuke Test are still
            /// available with <see cref="ShotDoesntEndTurn"/> being <see langword="true"/>.
            /// </summary>
            /// <remarks>Configurable with the /usw command in RubberWorm.</remarks>
            public bool ShotDoesntEndTurnAll
            {
                get => _shotDoesntEndTurnAll;
                set => _shotDoesntEndTurnAll = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether hitting a worm with a Pneumatic Drill will cause that worm to move
            /// horizontally proportionally to how fast the drilling worm is moving.
            /// </summary>
            public bool? DrillImpartsVelocity
            {
                get => TriStateToNullBool(_drillImpartsVelocity);
                set => _drillImpartsVelocity = NullBoolToTriState(value);
            }

            /// <summary>
            /// Gets or sets a value indicating whether girder placement will not let the mouse move to positions where
            /// the placement would be out of range.
            /// </summary>
            public bool GirderRadiusAssist
            {
                get => _girderRadiusAssist;
                set => _girderRadiusAssist = value;
            }

            /// <summary>
            /// Gets or sets to what extent flames from a Petrol Bomb decay per turn. A value of 1 causes the flames to be
            /// removed completely after one turn. Valid values lie between 0 and 1, where 0.2 is the default.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value is 0.</exception>
            public float FlameTurnDecay
            {
                get => FixedPointToSingle(_flameTurnDecay);
                set
                {
                    if (value < 0 || value > 1)
                        throw new ArgumentOutOfRangeException(nameof(value), "Flame turn decay must lie between 0 and 1.");
                    _flameTurnDecay = (ushort)SingleToFixedPoint(value);
                }
            }

            /// <summary>
            /// Gets or sets to what extent flames from a Petrol Bomb decay when touched by a worm. Larger values result in
            /// the flames disappearing faster and thus imparting less total damage. Valid values are bigger than 0, the
            /// default is 30.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value is 0.</exception>
            public byte FlameTouchDecay
            {
                get => _flameTouchDecay;
                set
                {
                    if (value == 0)
                        throw new ArgumentOutOfRangeException(nameof(value), "Flame touch decay must not be 0.");
                    _flameTouchDecay = value;
                }
            }

            /// <summary>
            /// Gets or sets the maximum number of flame particles active at the same time. Valid values are bigger than 0,
            /// the default is 200.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value is 0.</exception>
            /// <remarks>Configurable with the /flames(value / 100) command in RubberWorm.</remarks>
            public ushort FlameLimit
            {
                get => _flameLimit;
                set
                {
                    if (value == 0)
                        throw new ArgumentOutOfRangeException(nameof(value), "Flame limit must not be 0.");
                    _flameLimit = value;
                }
            }

            /// <summary>
            /// Gets or sets the maximum speed at which objects can move. Valid values lie between 0 and 32768, where 32 is
            /// the default, and 0 is unlimited speed.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value lies outside 0 or 32768.</exception>
            public float ProjectileMaxSpeed
            {
                get => FixedPointToSingle(_projectileMaxSpeed);
                set
                {
                    if (value < 0 || value > 32768)
                        throw new ArgumentOutOfRangeException(nameof(value), "Projectile speed must lie between 0 and 32768.");
                    _projectileMaxSpeed = SingleToFixedPoint(value);
                }
            }

            /// <summary>
            /// Gets or sets the maximum speed at which roping can occur. Valid values lie between 0 and 32768, where 16 is
            /// the default and 0 disables the limit completely.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value lies outside 0 or 32768.</exception>
            /// <remarks>Configurable with the /speed(value | 255=0) command in RubberWorm.</remarks>
            public float RopeMaxSpeed
            {
                get => FixedPointToSingle(_ropeMaxSpeed);
                set
                {
                    if (value < 0 || value > 32768)
                        throw new ArgumentOutOfRangeException(nameof(value), "Rope speed must lie between 0 and 32768.");
                    _ropeMaxSpeed = SingleToFixedPoint(value);
                }
            }

            /// <summary>
            /// Gets or sets the maximum speed at which a Jetpack can fly. Valid values lie between 0 and 32768, where 5 is
            /// the default.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value lies outside 0 or 32768.</exception>
            public float JetpackMaxSpeed
            {
                get => FixedPointToSingle(_jetpackMaxSpeed);
                set
                {
                    if (value < 0 || value > 32768)
                        throw new ArgumentOutOfRangeException(nameof(value), "Jetpack speed must lie between 0 and 32768.");
                    _jetpackMaxSpeed = SingleToFixedPoint(value);
                }
            }

            /// <summary>
            /// Gets or sets the speed at which physics are running and sound effects are pitched to. Valid values lie
            /// between 0.0625 and 128, where 1 is the default.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value lies outside 0.0625 or 128.</exception>
            public float GameSpeed
            {
                get => FixedPointToSingle(_gameSpeed);
                set
                {
                    if (value < 0.0625 || value > 128)
                        throw new ArgumentOutOfRangeException(nameof(value), "Game speed must lie between 0.0625 and 128.");
                    _gameSpeed = SingleToFixedPoint(value);
                }
            }

            /// <summary>
            /// Gets or sets a value indicating whether firing the ninja rope while moving will bypass the angle limit of it
            /// and allow it to be shot vertically downwards.
            /// </summary>
            public bool? IndianRopeGlitch
            {
                get => TriStateToNullBool(_indianRopeGlitch);
                set => _indianRopeGlitch = NullBoolToTriState(value);
            }

            /// <summary>
            /// Gets or sets a value indicating whether the number of cows in a herd can be doubled by jumping at the right
            /// moment.
            /// </summary>
            public bool? HerdDoublingGlitch
            {
                get => TriStateToNullBool(_herdDoublingGlitch);
                set => _herdDoublingGlitch = NullBoolToTriState(value);
            }

            /// <summary>
            /// Gets or sets a value indicating whether Bungee can be triggered by turning on a jetpack and immediately
            /// pressing space with it selected.
            /// </summary>
            public bool JetpackBungeeGlitch
            {
                get => _jetpackBungeeGlitch;
                set => _jetpackBungeeGlitch = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the Baseball Bat or Longbow aim restrictions can be circumvented
            /// by selecting and firing them while moving.
            /// </summary>
            public bool AngleCheatGlitch
            {
                get => _angleCheatGlitch;
                set => _angleCheatGlitch = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether a certain type of leftward collision by a jumping, post-drilling, or
            /// post-retreat worm can cause it to continue moving with reduced speed rather than landing.
            /// </summary>
            public bool GlideGlitch
            {
                get => _glideGlitch;
                set => _glideGlitch = value;
            }

            /// <summary>
            /// Gets or sets a value determining how much skip walking speeds up.
            /// </summary>
            public SkipWalk SkipWalk
            {
                get => _skipWalk;
                set => _skipWalk = value;
            }

            /// <summary>
            /// Gets or sets a value extending the roof into specific directions to prevent roofing.
            /// </summary>
            public Roofing Roofing
            {
                get => _roofing;
                set => _roofing = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether immediately exploding weapons can be placed in a way they stick
            /// to a worms head and wait for their timer to explode them instead.
            /// </summary>
            public bool FloatingWeaponGlitch
            {
                get => _floatingWeaponGlitch;
                set => _floatingWeaponGlitch = value;
            }

            /// <summary>
            /// Gets or sets the power with which worms bounce off terrain. Valid values lie between 0 and 1, where 0
            /// disables this feature and 1 fully bounces worms back without speed loss.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value lies outside of 0 or 1.</exception>
            /// <remarks>Configurable with the /rubber(value * 255) command in RubberWorm.</remarks>
            public float WormBounce
            {
                get => FixedPointToSingle(_wormBounce);
                set
                {
                    if (value < 0 || value > 1)
                        throw new ArgumentOutOfRangeException(nameof(value), "Worm bounce must lie between 0 and 1.");
                    _wormBounce = SingleToFixedPoint(value);
                }
            }

            /// <summary>
            /// Gets or sets the amount of air viscosity affecting objects. Valid values lie between 0 and 1. Values above
            /// 0.25 are only allowed when using RubberWorm.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value lies outside of 0 or 1.</exception>
            /// <remarks>Configurable with the /visc(value * 255) command in RubberWorm.</remarks>
            public float Viscosity
            {
                get => FixedPointToSingle(_viscosity);
                set
                {
                    if (value < 0 || value > 1)
                        throw new ArgumentOutOfRangeException(nameof(value), "Viscosity must lie between 0 an 1.");
                    _viscosity = SingleToFixedPoint(value);
                }
            }

            /// <summary>
            /// Gets or sets a value indicating whether worms are affected by <see cref="Viscosity"/> aswell.
            /// </summary>
            public bool ViscosityWorms
            {
                get => _viscosityWorms;
                set => _viscosityWorms = value;
            }

            /// <summary>
            /// Gets or sets the influence power of the wind affecting all weapons. Valid values lie between 0 and 1. 1 is
            /// the same influence as the Bazooka.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value lies outside of 0 or 1.</exception>
            /// <remarks>Configurable with the /wind(value * 255) command in RubberWorm.</remarks>
            public float RwWind
            {
                get => FixedPointToSingle(_rwWind);
                set
                {
                    if (value < 0 || value > 1)
                        throw new ArgumentOutOfRangeException(nameof(value), "Wind power must lie between 0 and 1.");
                    _rwWind = SingleToFixedPoint(value);
                }
            }

            /// <summary>
            /// Gets or sets a value indicating whether worms are affected by <see cref="RwWind"/> aswell.
            /// </summary>
            public bool RwWindWorms
            {
                get => _rwWindWorms;
                set => _rwWindWorms = value;
            }

            /// <summary>
            /// Gets or sets the gravity function used.
            /// </summary>
            /// <remarks>Configurable with the /gravity, /cbh, and /pbh commands in RubberWorm.</remarks>
            public RwGravityType RwGravityType
            {
                get => _rwGravityType;
                set => _rwGravityType = value;
            }

            /// <summary>
            /// Gets or sets the strength of gravity applied by the <see cref="RwGravityType"/>. Valid values lie between 0
            /// and 16384.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">The value lies outside of 0 or 16384.</exception>
            /// <remarks>Configurable with the /gravity(value / 512), /cbh(value / 512), and /pbh(value / 512) commands in
            /// RubberWorm.</remarks>
            public float RwGravity
            {
                get => FixedPointToSingle(_rwGravity);
                set
                {
                    if (value < 0 || value > 16384)
                        throw new ArgumentOutOfRangeException(nameof(value), "Gravity must lie between 0 and 16384.");
                    _rwGravity = SingleToFixedPoint(value);
                }
            }

            /// <summary>
            /// Gets or sets the maximum number of crates spawning per turn and enables the crate counter.
            /// </summary>
            /// <remarks>Configurable with the /craterate(value) command in RubberWorm.</remarks>
            public byte CrateRate
            {
                get => _crateRate;
                set => _crateRate = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether crate shower is enabled throughout a turn.
            /// </summary>
            /// <remarks>Configurable with the /crateshower command in RubberWorm.</remarks>
            public bool CrateShower
            {
                get => _crateShower;
                set => _crateShower = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether worms falling into water will be reset to the last solid location
            /// they stood on.
            /// </summary>
            /// <remarks>Configurable with the /antisink command in RubberWorm.</remarks>
            public bool AntiSink
            {
                get => _antiSink;
                set => _antiSink = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the selected weapon no longer resets to a remembered one upon
            /// shooting it.
            /// </summary>
            /// <remarks>Configurable with the /wdca command in RubberWorm.</remarks>.
            public bool WeaponsDontChange
            {
                get => _weaponsDontChange;
                set => _weaponsDontChange = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether weapon fuses can be selected between 0-9 seconds and herd counts
            /// between 1-10 animals.
            /// </summary>
            /// <remarks>Configurable with the /fuseex command in RubberWorm.</remarks>
            public bool ExtendedFuse
            {
                get => _extendedFuse;
                set => _extendedFuse = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the aim direction of a weapon will be reset at turn start.
            /// </summary>
            /// <remarks>Configurable with the /reaim command in RubberWorm.</remarks>
            public bool AutoReaim
            {
                get => _autoReaim;
                set => _autoReaim = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether objects which overlap with the map will fall until reaching free
            /// space. If disabled, they will be stuck in place instead.
            /// </summary>
            public bool? TerrainOverlapGlitch
            {
                get => TriStateToNullBool(_terrainOverlapGlitch);
                set => _terrainOverlapGlitch = NullBoolToTriState(value);
            }

            /// <summary>
            /// Gets or sets a value whether the round time will be measured in fractional seconds.
            /// </summary>
            public bool RoundTimeFractional
            {
                get => _roundTimeFractional;
                set => _roundTimeFractional = value;
            }

            /// <summary>
            /// Gets or sets a value indicating whether retreat time will automatically be started if the turn timer ran
            /// out.
            /// </summary>
            public bool AutoRetreat
            {
                get => _autoRetreat;
                set => _autoRetreat = value;
            }

            /// <summary>
            /// Gets or sets which worms are cured when one collects a health crate.
            /// </summary>
            public HealthCure HealthCure
            {
                get => _healthCure;
                set => _healthCure = value;
            }

            /// <summary>
            /// Gets or sets the Kaos game scheme mod. 0 for none, 1-5 for the corresponding mod.
            /// </summary>
            /// <exception cref="ArgumentOutOfRangeException">Value is bigger than 5.</exception>
            /// <remarks>Configurable with the /kaosmod(value) command in RubberWorm.</remarks>
            public byte KaosMod
            {
                get => _kaosMod;
                set
                {
                    if (value > 5)
                        throw new ArgumentOutOfRangeException(nameof(value), "Kaos mod must not be greater than 5.");
                    _kaosMod = value;
                }
            }

            /// <summary>
            /// Gets or sets which effects an enabled <see cref="SheepHeaven"/> has on the game.
            /// </summary>
            public SheepHeavenFlags SheepHeavenFlags
            {
                get => _sheepHeavenFlags;
                set => _sheepHeavenFlags = value;
            }

            /// <summary>
            /// Gets or sets whether utilities can stack per turn, allowing instant effects to be used automatically in any
            /// following turn.
            /// </summary>
            public bool ConserveUtilities
            {
                get => _conserveUtilities;
                set => _conserveUtilities = value;
            }

            /// <summary>
            /// Gets or sets whether utilities are consumed immediately on collecting them, even after the turn ended.
            /// </summary>
            public bool ExpediteUtilities
            {
                get => _expediteUtilities;
                set => _expediteUtilities = value;
            }

            /// <summary>
            /// Gets or sets whether the Double Turn Time utility can stack.
            /// </summary>
            public byte DoubleTimeCount
            {
                get => _doubleTimeCount;
                set => _doubleTimeCount = value;
            }

            /// <inheritdoc/>
            public override bool Equals(object obj) => obj is ExtendedOptions options && Equals(options);

            /// <inheritdoc/>
            public bool Equals(ExtendedOptions other) => AsSpan().SequenceEqual(other.AsSpan());

            /// <inheritdoc/>
            public override int GetHashCode()
            {
                HashCode hash = new HashCode();
                hash.Add(_dataVersion);
                hash.Add(_constantWind);
                hash.Add(_wind);
                hash.Add(_windBias);
                hash.Add(_gravity);
                hash.Add(_friction);
                hash.Add(_ropeKnockForce);
                hash.Add(_bloodAmount);
                hash.Add(_ropeUpgrade);
                hash.Add(_groupPlaceAllies);
                hash.Add(_noCrateProbability);
                hash.Add(_crateLimit);
                hash.Add(_suddenDeathNoWormSelect);
                hash.Add(_suddenDeathTurnDamage);
                hash.Add(_wormPhasingAlly);
                hash.Add(_wormPhasingEnemy);
                hash.Add(_circularAim);
                hash.Add(_antiLockAim);
                hash.Add(_antiLockPower);
                hash.Add(_wormSelectKeepHotSeat);
                hash.Add(_wormSelectAnytime);
                hash.Add(_battyRope);
                hash.Add(_ropeRollDrops);
                hash.Add(_keepControlXImpact);
                hash.Add(_keepControlHeadBump);
                hash.Add(_keepControlSkim);
                hash.Add(_explosionFallDamage);
                hash.Add(_objectPushByExplosion);
                hash.Add(_undeterminedCrates);
                hash.Add(_undeterminedMineFuse);
                hash.Add(_firingPausesTimer);
                hash.Add(_loseControlDoesntEndTurn);
                hash.Add(_shotDoesntEndTurn);
                hash.Add(_shotDoesntEndTurnAll);
                hash.Add(_drillImpartsVelocity);
                hash.Add(_girderRadiusAssist);
                hash.Add(_flameTurnDecay);
                hash.Add(_flameTouchDecay);
                hash.Add(_flameLimit);
                hash.Add(_projectileMaxSpeed);
                hash.Add(_ropeMaxSpeed);
                hash.Add(_jetpackMaxSpeed);
                hash.Add(_gameSpeed);
                hash.Add(_indianRopeGlitch);
                hash.Add(_herdDoublingGlitch);
                hash.Add(_jetpackBungeeGlitch);
                hash.Add(_angleCheatGlitch);
                hash.Add(_glideGlitch);
                hash.Add(_skipWalk);
                hash.Add(_roofing);
                hash.Add(_floatingWeaponGlitch);
                hash.Add(_wormBounce);
                hash.Add(_viscosity);
                hash.Add(_viscosityWorms);
                hash.Add(_rwWind);
                hash.Add(_rwWindWorms);
                hash.Add(_rwGravityType);
                hash.Add(_rwGravity);
                hash.Add(_crateRate);
                hash.Add(_crateShower);
                hash.Add(_antiSink);
                hash.Add(_weaponsDontChange);
                hash.Add(_extendedFuse);
                hash.Add(_autoReaim);
                hash.Add(_terrainOverlapGlitch);
                hash.Add(_roundTimeFractional);
                hash.Add(_autoRetreat);
                hash.Add(_healthCure);
                hash.Add(_kaosMod);
                hash.Add(_sheepHeavenFlags);
                hash.Add(_conserveUtilities);
                hash.Add(_expediteUtilities);
                hash.Add(_doubleTimeCount);
                return hash.ToHashCode();
            }

            /// <summary>
            /// Returns this instance as raw data.
            /// </summary>
            /// <returns>A <see cref="Span{Byte}"/> storing the raw data of the options.</returns>
            internal unsafe Span<byte> AsSpan()
                => new Span<byte>(Unsafe.AsPointer(ref this), Unsafe.SizeOf<ExtendedOptions>());

            private static float FixedPointToSingle(int value) => value / 65536f;

            private static byte NullBoolToTriState(bool? value) => value switch
            {
                null => 128,
                false => 0,
                _ => 1
            };

            private static byte NullByteToTriState(byte? value) => value switch
            {
                null => Byte.MaxValue,
                _ => value.Value
            };

            private static int SingleToFixedPoint(float value) => (int)(value * 65536);

            private static bool? TriStateToNullBool(byte value) => value switch
            {
                0 => false,
                128 => null,
                _ => true
            };

            private static byte? TriStateToNullByte(byte value) => value switch
            {
                Byte.MaxValue => null,
                _ => value
            };
        }
    }
}
