namespace Syroot.Worms.Armageddon
{
    /// <summary>
    /// Represents the weapons in the game.
    /// </summary>
    public enum Weapon
    {
        /// <summary>The Bazooka weapon.</summary>
        Bazooka,
        /// <summary>The Homing Missile weapon.</summary>
        HomingMissile,
        /// <summary>The Mortar weapon.</summary>
        Mortar,
        /// <summary>The Grenade weapon.</summary>
        Grenade,
        /// <summary>The Cluster Bomb weapon.</summary>
        ClusterBomb,
        /// <summary>The Skunk weapon.</summary>
        Skunk,
        /// <summary>The Petrol Bomb weapon.</summary>
        PetrolBomb,
        /// <summary>The Banana Bomb weapon.</summary>
        BananaBomb,
        /// <summary>The Handgun weapon.</summary>
        Handgun,
        /// <summary>The Shotgun weapon.</summary>
        Shotgun,
        /// <summary>The Uzi weapon.</summary>
        Uzi,
        /// <summary>The Minigun weapon.</summary>
        Minigun,
        /// <summary>The Longbow weapon.</summary>
        Longbow,
        /// <summary>The Airstrike weapon.</summary>
        Airstrike,
        /// <summary>The Napalm Strike weapon.</summary>
        NapalmStrike,
        /// <summary>The Mine weapon.</summary>
        Mine,
        /// <summary>The Firepunch weapon.</summary>
        Firepunch,
        /// <summary>The Dragonball weapon.</summary>
        Dragonball,
        /// <summary>The Kamikaze weapon.</summary>
        Kamikaze,
        /// <summary>The Prod weapon.</summary>
        Prod,
        /// <summary>The Battle Axe weapon.</summary>
        BattleAxe,
        /// <summary>The Blowtorch weapon.</summary>
        Blowtorch,
        /// <summary>The Pneumatic Drill weapon.</summary>
        PneumaticDrill,
        /// <summary>The Girder weapon.</summary>
        Girder,
        /// <summary>The Ninja Rope weapon.</summary>
        NinjaRope,
        /// <summary>The Parachute weapon.</summary>
        Parachute,
        /// <summary>The Bungee weapon.</summary>
        Bungee,
        /// <summary>The Teleport weapon.</summary>
        Teleport,
        /// <summary>The Dynamite weapon.</summary>
        Dynamite,
        /// <summary>The Sheep weapon.</summary>
        Sheep,
        /// <summary>The Baseball Bat weapon.</summary>
        BaseballBat,
        /// <summary>The Flame Thrower weapon.</summary>
        Flamethrower,
        /// <summary>The Homing Pigeon weapon.</summary>
        HomingPigeon,
        /// <summary>The Mad Cow weapon.</summary>
        MadCow,
        /// <summary>The Holy Hand Grenade weapon.</summary>
        HolyHandGrenade,
        /// <summary>The Old Woman weapon.</summary>
        OldWoman,
        /// <summary>The Sheep Launcher weapon.</summary>
        SheepLauncher,
        /// <summary>The Super Sheep or Aqua Sheep weapon.</summary>
        SuperSheep,
        /// <summary>The Mole Bomb weapon.</summary>
        MoleBomb,
        /// <summary>The Jetpack utility.</summary>
        Jetpack,
        /// <summary>The Low Gravity utility.</summary>
        LowGravity,
        /// <summary>The Laser Sight utility.</summary>
        LaserSight,
        /// <summary>The Fast Walk utility.</summary>
        FastWalk,
        /// <summary>The Invisibility utility.</summary>
        Invisibility,
        /// <summary>The Damage x2 utility.</summary>
        DamageX2,
        /// <summary>The Freeze super weapon.</summary>
        Freeze,
        /// <summary>The Super Banana Bomb super weapon.</summary>
        SuperBananaBomb,
        /// <summary>The Mine Strike super weapon.</summary>
        MineStrike,
        /// <summary>The Girder Starter Pack super weapon.</summary>
        GirderStarterPack,
        /// <summary>The Earthquake super weapon.</summary>
        Earthquake,
        /// <summary>The Scales Of Justice super weapon.</summary>
        ScalesOfJustice,
        /// <summary>The Ming Vase super weapon.</summary>
        MingVase,
        /// <summary>The Mike's Carpet Bomb super weapon.</summary>
        MikesCarpetBomb,
        /// <summary>The Patsy's Magic Bullet super weapon.</summary>
        MagicBullet,
        /// <summary>The Indian Nuclear Test super weapon.</summary>
        NuclearTest,
        /// <summary>The Select Worm super weapon.</summary>
        SelectWorm,
        /// <summary>The Salvation Army super weapon.</summary>
        SalvationArmy,
        /// <summary>The Mole Squadron super weapon.</summary>
        MoleSquadron,
        /// <summary>The MB Bomb super weapon.</summary>
        MBBomb,
        /// <summary>The Concrete Donkey super weapon.</summary>
        ConcreteDonkey,
        /// <summary>The Suicide Bomber super weapon.</summary>
        SuicideBomber,
        /// <summary>The Sheep Strike super weapon.</summary>
        SheepStrike,
        /// <summary>The Mail Strike super weapon.</summary>
        MailStrike,
        /// <summary>The Armageddon super weapon.</summary>
        Armageddon
    }

    /// <summary>
    /// Represents utility methods for <see cref="Weapon"/> instances.
    /// </summary>
    public static class WeaponTools
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Returns a value indicating whether the <paramref name="weapon"/> is handled as a super weapon.
        /// </summary>
        /// <param name="weapon">The <see cref="Weapon"/> to check for being a super weapon.</param>
        /// <returns>Whether the weapon is a super weapon.</returns>
        public static bool IsSuperWeapon(this Weapon weapon) => weapon >= Weapon.Freeze;
    }
}
