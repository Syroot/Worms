using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using Syroot.BinaryData;
using Syroot.Worms.IO;

namespace Syroot.Worms.Armageddon
{
    /// <summary>
    /// Represents a team stored in a <see cref="TeamContainer"/> file.
    /// </summary>
    public class Team : ILoadable, ISaveable
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _missionCount = 33;
        private const int _trainingMissionCount = 6;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the name of the team.
        /// </summary>
        public string Name { get; set; } = String.Empty;

        /// <summary>
        /// Gets the 8 worm names.
        /// </summary>
        public string[] WormNames { get; } = new string[8];

        /// <summary>
        /// Gets or sets the AI intelligence difficulty level, from 0-5, where 0 is human-controlled.
        /// </summary>
        public byte CpuLevel { get; set; }

        /// <summary>
        /// Gets or sets the name of soundbank for the voice of team worms.
        /// </summary>
        public string SoundBankName { get; set; } = String.Empty;

        public byte SoundBankLocation { get; set; }

        /// <summary>
        /// Gets or sets the name of the team fanfare.
        /// </summary>
        public string FanfareName { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets a value indicating whether the fanfare with the name stored in <see cref="FanfareName"/>
        /// (<see langword="true"/>) or the player's countries' fanfare should be played (<see langword="false"/>).
        /// </summary>
        public byte UseCustomFanfare { get; set; }

        /// <summary>
        /// Gets or sets the sprite index of the team grave, -1 being a custom bitmap grave.
        /// </summary>
        public sbyte GraveSprite { get; set; }

        /// <summary>
        /// Gets or sets the file name of the team grave bitmap if it uses a custom one.
        /// </summary>
        public string GraveFileName { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the team grave bitmap if it uses a custom one.
        /// </summary>
        public RawBitmap Grave { get; set; } = new RawBitmap();

        /// <summary>
        /// Gets or sets the team's special weapon.
        /// </summary>
        public TeamWeapon TeamWeapon { get; set; }

        /// <summary>
        /// Gets or sets the number of games lost.
        /// </summary>
        public int GamesLost { get; set; }

        /// <summary>
        /// Gets or sets the number of deathmatch games lost.
        /// </summary>
        public int DeathmatchesLost { get; set; }

        /// <summary>
        /// Gets or sets the number of games won.
        /// </summary>
        public int GamesWon { get; set; }

        /// <summary>
        /// Gets or sets the number of deathmatch games won.
        /// </summary>
        public int DeathmatchesWon { get; set; }

        /// <summary>
        /// Gets or sets the number of games drawn.
        /// </summary>
        public int GamesDrawn { get; set; }

        /// <summary>
        /// Gets or sets the number of deathmatch games drawn.
        /// </summary>
        public int DeathmatchesDrawn { get; set; }

        /// <summary>
        /// Gets or sets the number of opponent worms killed by this team.
        /// </summary>
        public int Kills { get; set; }

        /// <summary>
        /// Gets or sets the number of opponent worms killed by this team in deathmatches.
        /// </summary>
        public int DeathmatchKills { get; set; }

        /// <summary>
        /// Gets or sets the number of worms which got killed in this team.
        /// </summary>
        public int Deaths { get; set; }

        /// <summary>
        /// Gets or sets the number of worms which got killed in this team in deathmatches.
        /// </summary>
        public int DeathmatchDeaths { get; set; }

        /// <summary>
        /// Gets the array of 33 mission statuses.
        /// </summary>
        public TeamMissionStatus[] MissionStatuses { get; } = new TeamMissionStatus[_missionCount];

        /// <summary>
        /// Gets or sets the file name of the team flag.
        /// </summary>
        public string FlagFileName { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the bitmap of the team flag.
        /// </summary>
        public RawBitmap Flag { get; set; } = new RawBitmap();

        /// <summary>
        /// Gets or sets the deathmatch rank this team reached.
        /// </summary>
        public byte DeathmatchRank { get; set; }

        /// <summary>
        /// Gets or sets the seconds the team required to finish all 6 training missions. 
        /// </summary>
        public int[] TrainingMissionTimes { get; set; } = new int[_trainingMissionCount];

        public int[] Unknown1 { get; set; } = new int[10];

        /// <summary>
        /// Gets or sets the medals the team achieved in all 6 training missions.
        /// </summary>
        public byte[] TrainingMissionMedals { get; set; } = new byte[_trainingMissionCount];

        public byte[] Unknown2 { get; set; } = new byte[10];

        public int[] Unknown3 { get; set; } = new int[7];

        public byte Unknown4 { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public void Load(Stream stream)
        {
            using BinaryStream reader = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);

            Name = reader.ReadString(17);
            for (int i = 0; i < WormNames.Length; i++)
                WormNames[i] = reader.ReadString(17);
            CpuLevel = reader.Read1Byte();
            SoundBankName = reader.ReadString(0x20);
            SoundBankLocation = reader.Read1Byte();
            FanfareName = reader.ReadString(0x20);
            UseCustomFanfare = reader.Read1Byte();

            GraveSprite = reader.ReadSByte();
            if (GraveSprite < 0)
            {
                GraveFileName = reader.ReadString(0x20);
                Grave = new RawBitmap()
                {
                    BitsPerPixel = 8,
                    Size = new Size(24, 32),
                    Palette = reader.ReadColors(256),
                    Data = reader.ReadBytes(24 * 32)
                };
            }

            TeamWeapon = reader.ReadEnum<TeamWeapon>(true);
            GamesLost = reader.ReadInt32();
            DeathmatchesLost = reader.ReadInt32();
            GamesWon = reader.ReadInt32();
            DeathmatchesWon = reader.ReadInt32();
            GamesDrawn = reader.ReadInt32();
            DeathmatchesDrawn = reader.ReadInt32();
            Kills = reader.ReadInt32();
            DeathmatchKills = reader.ReadInt32();
            Deaths = reader.ReadInt32();
            DeathmatchDeaths = reader.ReadInt32();
            for (int i = 0; i < _missionCount; i++)
                MissionStatuses[i] = reader.ReadStruct<TeamMissionStatus>();

            FlagFileName = reader.ReadString(0x20);
            Flag = new RawBitmap()
            {
                BitsPerPixel = 8,
                Size = new Size(20, 17),
                Palette = reader.ReadColors(256),
                Data = reader.ReadBytes(20 * 17)
            };

            DeathmatchRank = reader.Read1Byte();
            TrainingMissionTimes = reader.ReadInt32s(_trainingMissionCount);
            Unknown1 = reader.ReadInt32s(10);
            TrainingMissionMedals = reader.ReadBytes(_trainingMissionCount);
            Unknown2 = reader.ReadBytes(10);
            Unknown3 = reader.ReadInt32s(7);
            Unknown4 = reader.Read1Byte();
        }

        /// <inheritdoc/>
        public void Save(Stream stream)
        {
            using BinaryStream writer = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);

            writer.WriteFixedString(Name, 17);
            for (int i = 0; i < WormNames.Length; i++)
                writer.WriteFixedString(WormNames[i], 17);
            writer.Write(CpuLevel);
            writer.WriteFixedString(SoundBankName, 0x20);
            writer.Write(SoundBankLocation);
            writer.WriteFixedString(FanfareName, 0x20);
            writer.Write(UseCustomFanfare);

            writer.Write(GraveSprite);
            if (GraveSprite < 0)
            {
                writer.WriteFixedString(GraveFileName, 0x20);
                writer.WriteColors(Grave.Palette!);
                writer.Write(Grave.Data);
            }

            writer.WriteEnum(TeamWeapon, true);
            writer.Write(GamesLost);
            writer.Write(DeathmatchesLost);
            writer.Write(GamesWon);
            writer.Write(DeathmatchesWon);
            writer.Write(GamesDrawn);
            writer.Write(DeathmatchesDrawn);
            writer.Write(Kills);
            writer.Write(DeathmatchKills);
            writer.Write(Deaths);
            writer.Write(DeathmatchDeaths);
            for (int i = 0; i < MissionStatuses.Length; i++)
                writer.WriteStruct(ref MissionStatuses[i]);

            writer.WriteFixedString(FlagFileName, 0x20);
            writer.WriteColors(Flag.Palette!);
            writer.Write(Flag.Data);

            writer.Write(DeathmatchRank);
            writer.Write(TrainingMissionTimes);
            writer.Write(Unknown1);
            writer.Write(TrainingMissionMedals);
            writer.Write(Unknown2);
            writer.Write(Unknown3);
            writer.Write(Unknown4);
        }
    }

    /// <summary>
    /// Represents a team's progress in a mission.
    /// </summary>
    [DebuggerDisplay("TeamMissionStatus Attemps={Attempts} Medal={Medal}")]
    public struct TeamMissionStatus
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>The number of attempts the team required to solve the mission.</summary>
        public int Attempts;
        /// <summary>The medal the team got to solve the mission.</summary>
        public int Medal;
    }

    /// <summary>
    /// Represents the special weapon of a team.
    /// </summary>
    public enum TeamWeapon : byte
    {
        /// <summary>The Flame Thrower weapon.</summary>
        Flamethrower,
        /// <summary>The Mole Bomb weapon.</summary>
        MoleBomb,
        /// <summary>The Old Woman weapon.</summary>
        OldWoman,
        /// <summary>The Homing Pigeon weapon.</summary>
        HomingPigeon,
        /// <summary>The Sheep Launcher weapon.</summary>
        SheepLauncher,
        /// <summary>The Mad Cow weapon.</summary>
        MadCow,
        /// <summary>The Holy Hand Grenade weapon.</summary>
        HolyHandGrenade,
        /// <summary>The Super Sheep or Aqua Sheep weapon.</summary>
        SuperSheep
    }
}
