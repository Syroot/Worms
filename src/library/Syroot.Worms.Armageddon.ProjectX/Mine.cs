using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public struct Mine
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int Sensitivity;
        [BinaryMember(Order = 2)] public int TimeBeforeOn;
        [BinaryMember(Order = 3)] public CollisionFlags SensitiveTo;
        [BinaryMember(Order = 4)] public int FuseTime;
        [BinaryMember(Order = 5)] public int ExplosionBias;
        [BinaryMember(Order = 6)] public int ExplosionPower;
        [BinaryMember(Order = 7)] public int MaxDamage;
    }
}
