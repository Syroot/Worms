using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public struct Sprite
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int SpriteNumber;
        [BinaryMember(Order = 2)] public SpriteAnimationType AnimationType;
        [BinaryMember(Order = 3)] public int TrailSprite;
        [BinaryMember(Order = 4)] public int TrailAmount;
        [BinaryMember(Order = 5)] public int TrailVanishSpeed;
        [BinaryMember(Order = 6)] public int Unknown;
    }

    public enum SpriteAnimationType : int
    {
        HorizontalVelocity,
        Cycle,
        TrackMovement,
        TrackSpeed,
        SlowCycle,
        FasterCycle,
        FastCycle
    }
}
