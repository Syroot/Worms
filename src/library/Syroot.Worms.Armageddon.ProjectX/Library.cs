using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Syroot.BinaryData;
using Syroot.Worms.IO;

namespace Syroot.Worms.Armageddon.ProjectX
{
    /// <summary>
    /// Represents a library stored in a PXL file which contains reusable weapons, files and scripts.
    /// Used by WA PX. S. https://worms2d.info/Project_X/Library_file.
    /// </summary>
    public class Library : IList<LibraryItem>, ILoadableFile, ISaveableFile
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _signature = 0x1BCD0102;
        private const byte _version = 0x00;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly List<LibraryItem> _list = new List<LibraryItem>();

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Scheme"/> class.
        /// </summary>
        public Library() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="Scheme"/> class, loading the data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the data from.</param>
        public Library(Stream stream) => Load(stream);

        /// <summary>
        /// Initializes a new instance of the <see cref="Scheme"/> class, loading the data from the given file.
        /// </summary>
        /// <param name="fileName">The name of the file to load the data from.</param>
        public Library(string fileName) => Load(fileName);

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public int Count => _list.Count;

        /// <summary>
        /// Gets or sets the ProjectX library version.
        /// </summary>
        public byte Version { get; set; }

        /// <inheritdoc/>
        bool ICollection<LibraryItem>.IsReadOnly => false;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the library entries matching the given key.
        /// </summary>
        /// <param name="key">The key of the entries to match.</param>
        /// <returns>All matching entries.</returns>
        public IEnumerable<LibraryItem> this[string key] => this.Where(x => x.Key == key);

        /// <inheritdoc/>
        public LibraryItem this[int index]
        {
            get => _list[index];
            set => _list[index] = value;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public void Add(LibraryItem item) => _list.Add(item);

        /// <inheritdoc/>
        public void Clear() => _list.Clear();

        /// <inheritdoc/>
        public bool Contains(LibraryItem item) => _list.Contains(item);

        /// <inheritdoc/>
        public void CopyTo(LibraryItem[] array, int arrayIndex) => _list.CopyTo(array, arrayIndex);

        /// <inheritdoc/>
        public IEnumerator<LibraryItem> GetEnumerator() => _list.GetEnumerator();

        /// <summary>
        /// Gets all attached files.
        /// </summary>
        /// <returns>The enumeration of attached files.</returns>
        public IEnumerable<byte[]> GetFiles()
            => this.Where(x => x.Type == LibraryItemType.File).Select(x => (byte[])x.Value);

        /// <summary>
        /// Gets all attached scripts.
        /// </summary>
        /// <returns>The enumeration of attached scripts.</returns>
        public IEnumerable<string> GetScripts()
            => this.Where(x => x.Type == LibraryItemType.Script).Select(x => (string)x.Value);

        /// <summary>
        /// Gets all attached weapons.
        /// </summary>
        /// <returns>The enumeration of attached weapons.</returns>
        public IEnumerable<Weapon> GetWeapons()
            => this.Where(x => x.Type == LibraryItemType.Weapon).Select(x => (Weapon)x.Value);

        /// <inheritdoc/>
        public int IndexOf(LibraryItem item) => _list.IndexOf(item);

        /// <inheritdoc/>
        public void Insert(int index, LibraryItem item) => _list.Insert(index, item);

        /// <summary>
        /// Loads the data from the given <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the data from.</param>
        public void Load(Stream stream)
        {
            using BinaryStream reader = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);

            // Read the header.
            if (reader.ReadInt32() != _signature)
                throw new InvalidDataException("Invalid PXL file signature.");
            Version = reader.Read1Byte();
            if (Version != _version)
                throw new InvalidDataException("Invalid PXL file version.");

            // Read the items.
            _list.Clear();
            int itemCount = reader.ReadInt32();
            for (int i = 0; i < itemCount; i++)
            {
                LibraryItemType type = reader.ReadEnum<LibraryItemType>(true);
                string name = reader.ReadString(StringCoding.Int32CharCount);
                switch (type)
                {
                    case LibraryItemType.File:
                        _list.Add(new LibraryItem(name, reader.ReadBytes(reader.ReadInt32())));
                        break;
                    case LibraryItemType.Script:
                        _list.Add(new LibraryItem(name, reader.ReadString(StringCoding.Int32CharCount)));
                        break;
                    case LibraryItemType.Weapon:
                        _list.Add(new LibraryItem(name, reader.Load<Weapon>()));
                        break;
                }
            }
        }

        /// <summary>
        /// Loads the data from the given file.
        /// </summary>
        /// <param name="fileName">The name of the file to load the data from.</param>
        public void Load(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            Load(stream);
        }

        /// <inheritdoc/>
        public bool Remove(LibraryItem item) => _list.Remove(item);

        /// <inheritdoc/>
        public void RemoveAt(int index) => _list.RemoveAt(index);

        /// <summary>
        /// Saves the data into the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to save the data to.</param>
        public void Save(Stream stream)
        {
            using BinaryStream writer = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);

            // Write the header.
            writer.Write(_signature);
            writer.Write(Version);

            // Write the items.
            writer.Write(Count);
            foreach (LibraryItem item in _list)
            {
                writer.WriteEnum(item.Type, true);
                writer.Write(item.Key, StringCoding.Int32CharCount);
                switch (item.Type)
                {
                    case LibraryItemType.File:
                        byte[] value = (byte[])item.Value;
                        writer.Write(value.Length);
                        writer.Write(value);
                        break;
                    case LibraryItemType.Script:
                        writer.Write((string)item.Value, StringCoding.Int32CharCount);
                        break;
                    case LibraryItemType.Weapon:
                        writer.Save((Weapon)item.Value);
                        break;
                }
            }
        }

        /// <summary>
        /// Saves the data in the given file.
        /// </summary>
        /// <param name="fileName">The name of the file to save the data in.</param>
        public void Save(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
            Save(stream);
        }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_list).GetEnumerator();
        }
    }

    /// <summary>
    /// Represents an entry in a Project X library.
    /// </summary>
    [DebuggerDisplay("LibraryItem Key={Key} Type={Type}")]
    public class LibraryItem
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private object _value = null!;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="LibraryItem"/> class with the given <paramref name="key"/> and
        /// <paramref name="value"/>.
        /// </summary>
        /// <param name="key">The key under which the item will be stored.</param>
        /// <param name="value">The value which will be stored under the key. The type is inferred from this.</param>
        public LibraryItem(string key, object value)
        {
            Key = key;
            Value = value;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the name under which this item is stored.
        /// </summary>
        public string Key { get; set; } = String.Empty;

        /// <summary>
        /// Gets the type of the item.
        /// </summary>
        public LibraryItemType Type { get; private set; }

        /// <summary>
        /// Gets or sets the data of the item.
        /// </summary>
        public object Value
        {
            get => _value;
            set
            {
                // Validate the type.
                if (value.GetType() == typeof(byte[]))
                    Type = LibraryItemType.File;
                else if (value.GetType() == typeof(string))
                    Type = LibraryItemType.Script;
                else if (value.GetType() == typeof(Weapon))
                    Type = LibraryItemType.Weapon;
                else
                    throw new ArgumentException("Invalid LibraryItemType.", nameof(value));
                _value = value;
            }
        }
    }

    /// <summary>
    /// Represents the possible type of a library entry.
    /// </summary>
    public enum LibraryItemType : byte
    {
        /// <summary>The entry is a raw file in form of a byte array.</summary>
        File = 2,
        /// <summary>The entry is a script in form of a string.</summary>
        Script = 4,
        /// <summary>The entry is a weapon in form of a <see cref="Weapon"/> instance.</summary>
        Weapon = 8
    }
}
