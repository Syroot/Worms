using System;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    internal class TargetConverter : IBinaryConverter
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public object? Read(Stream stream, object instance, BinaryMemberAttribute memberAttribute,
            ByteConverter byteConverter)
        {
            ExplosionTarget explosionTarget = instance switch
            {
                LauncherStyle launcherStyle => launcherStyle.ExplosionTarget,
                _ => throw new NotImplementedException(),
            };
            return explosionTarget switch
            {
                ExplosionTarget.Clusters => stream.ReadObject<ClusterTarget>(),
                ExplosionTarget.Fire => stream.ReadObject<FireTarget>(),
                _ => null,
            };
        }

        /// <inheritdoc/>
        public void Write(Stream stream, object instance, BinaryMemberAttribute memberAttribute, object value,
            ByteConverter byteConverter)
        {
            stream.WriteObject(value);
        }
    }
}
