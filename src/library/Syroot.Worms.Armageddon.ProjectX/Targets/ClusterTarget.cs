using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class ClusterTarget : ITarget
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int ClusterCount { get; set; }

        [BinaryMember(Order = 2)] public int DispersionPower { get; set; }

        [BinaryMember(Order = 3)] public int Speed { get; set; }

        [BinaryMember(Order = 4)] public int EjectionAngle { get; set; }

        [BinaryMember(Order = 5)] public int DispersionAngle { get; set; }

        [BinaryMember(Order = 6)] public CollisionFlags Collisions { get; set; }

        [BinaryMember(Order = 7)] public int ExplosionBias { get; set; }

        [BinaryMember(Order = 8)] public int ExplosionPushPower { get; set; }

        [BinaryMember(Order = 9)] public int ExplosionDamage { get; set; }

        [BinaryMember(Order = 10)] public int ExplosionDamageVariation { get; set; }

        [BinaryMember(Order = 11)] public int SpriteCount { get; set; }

        [BinaryMember(Order = 12)] public Sprite Sprite { get; set; }

        [BinaryMember(Order = 13)] public int Acceleration { get; set; }

        [BinaryMember(Order = 14)] public int WindResponse { get; set; }

        [BinaryMember(Order = 15)] public int MotionRandomness { get; set; }

        [BinaryMember(Order = 16)] public int GravityFactor { get; set; }

        [BinaryMember(Order = 17)] public int Unused1 { get; set; }

        [BinaryMember(Order = 18)] public int Unused2 { get; set; }

        [BinaryMember(Order = 19)] public Sound Sound { get; set; }

        [BinaryMember(Order = 20, BooleanCoding = BooleanCoding.Dword)] public bool ExplodeOnSpace { get; set; }

        [BinaryMember(Order = 21)] public ExplosionAction ExplosionAction { get; set; }

        [BinaryMember(Order = 22, Converter = typeof(ActionConverter))] public IAction? Action { get; set; }
    }
}
