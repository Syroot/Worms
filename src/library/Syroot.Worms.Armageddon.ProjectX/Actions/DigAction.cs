namespace Syroot.Worms.Armageddon.ProjectX
{
    public class DigAction : IAction
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int Unknown1 { get; set; }

        public int Unknown2 { get; set; }

        public int DiggingSound { get; set; }

        public int SpriteJumping { get; set; }

        public int Sprite1 { get; set; }

        public int Sprite2 { get; set; }

        public int Sprite3 { get; set; }
    }
}
