namespace Syroot.Worms.Armageddon.ProjectX
{
    public class BounceAction : IAction
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public CollisionFlags Collisions { get; set; }

        public int Bounciness { get; set; }

        public int Acceleration { get; set; }

        public int Sound { get; set; }

        public int Unknown1 { get; set; }

        public int Unknown2 { get; set; }

        public int ExplosionBias { get; set; }

        public int ExplosionPower { get; set; }

        public int ExplosionDamage { get; set; }

        public int DamageRandomness { get; set; }

        public int BounceCount { get; set; }
    }
}
