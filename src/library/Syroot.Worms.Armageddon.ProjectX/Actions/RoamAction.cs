using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class RoamAction : IAction
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public CollisionFlags RoamCollisions { get; set; }

        public CollisionFlags ExplosionCollisions { get; set; }

        public int WalkSpeed { get; set; }

        public int Unknown { get; set; }

        public int JumpAngleEdge { get; set; }

        public int JumpVelocityEdge { get; set; }

        public int JumpSoundEdge { get; set; }

        public int JumpAngle { get; set; }

        public int JumpVelocity { get; set; }

        public int JumpSound { get; set; }

        public int TerrainOffset { get; set; }
        
        [BinaryMember(BooleanCoding = BooleanCoding.Dword)]
        public bool Fart { get; set; }

        public int DiseasePower { get; set; }

        public int SpriteFarting { get; set; }

        public int SpriteFlying { get; set; }

        public int SpriteFlying2 { get; set; }

        public int SpriteTakeOff { get; set; }

        public int SpriteDuringFlight { get; set; }
    }
}
