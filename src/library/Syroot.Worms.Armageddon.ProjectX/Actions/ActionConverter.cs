using System;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    internal class ActionConverter : IBinaryConverter
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public object? Read(Stream stream, object instance, BinaryMemberAttribute memberAttribute,
            ByteConverter byteConverter)
        {
            ExplosionAction explosionAction = instance switch
            {
                LauncherStyle launcherStyle => launcherStyle.ExplosionAction,
                ClusterTarget clusterTarget => clusterTarget.ExplosionAction,
                _ => throw new NotImplementedException()
            };
            return explosionAction switch
            {
                ExplosionAction.Home => stream.ReadObject<HomeAction>(),
                ExplosionAction.Bounce => stream.ReadObject<BounceAction>(),
                ExplosionAction.Roam => stream.ReadObject<RoamAction>(),
                ExplosionAction.Dig => stream.ReadObject<DigAction>(),
                _ => null
            };
        }

        /// <inheritdoc/>
        public void Write(Stream stream, object instance, BinaryMemberAttribute memberAttribute, object value,
            ByteConverter byteConverter)
        {
            stream.WriteObject(value);
        }
    }
}
