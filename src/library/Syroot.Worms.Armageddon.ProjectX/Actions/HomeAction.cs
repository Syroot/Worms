using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class HomeAction : IAction
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        [BinaryMember(Offset = 4)]
        public Sprite Sprite { get; set; }

        public HomeStyle HomeStyle { get; set; }
        
        public int Delay { get; set; }

        public int Duration { get; set; }
    }

    public enum HomeStyle
    {
        None,
        Straight,
        Intelligent
    }
}
