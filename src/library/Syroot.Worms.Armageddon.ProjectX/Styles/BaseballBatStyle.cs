using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class BaseballBatStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int Damage { get; set; }

        [BinaryMember(Order = 2)] public int PushPower { get; set; }
    }
}