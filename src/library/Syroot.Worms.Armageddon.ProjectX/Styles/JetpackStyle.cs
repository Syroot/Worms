using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class JetpackStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int Fuel { get; set; }
    }
}