using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class NinjaRopeStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int ShotCount { get; set; }

        [BinaryMember(Order = 2)] public int Length { get; set; }

        [BinaryMember(Order = 3)] public int AngleRestriction { get; set; }
    }
}