using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class DragonballStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int FiringSound { get; set; }

        [BinaryMember(Order = 2)] public int ImpactSound { get; set; }

        [BinaryMember(Order = 3)] public int BallSprite { get; set; }

        [BinaryMember(Order = 4)] public int Damage { get; set; }

        [BinaryMember(Order = 5)] public int Angle { get; set; }

        [BinaryMember(Order = 6)] public int Force { get; set; }

        [BinaryMember(Order = 7)] public int BallTime { get; set; }
    }
}