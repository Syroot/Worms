using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class GunStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int BulletCount { get; set; }

        [BinaryMember(Order = 2)] public int ReloadTime { get; set; }

        [BinaryMember(Order = 3)] public int BulletSpread { get; set; }

        [BinaryMember(Order = 4)] public int Burst { get; set; }

        [BinaryMember(Order = 5)] public int BurstSpread { get; set; }

        [BinaryMember(Order = 6)] public CollisionFlags Collisions { get; set; }

        [BinaryMember(Order = 7)] public int ExplosionBias { get; set; }

        [BinaryMember(Order = 8)] public int ExplosionPower { get; set; }

        [BinaryMember(Order = 9)] public int MaxDamage { get; set; }

        [BinaryMember(Order = 10)] public int DamageSpread { get; set; }

        [BinaryMember(Order = 11)] public int ExpEffect { get; set; }

        [BinaryMember(Order = 12)] public int Range1 { get; set; }

        [BinaryMember(Order = 13)] public int Range2 { get; set; }

        [BinaryMember(Order = 14)] public int Range3 { get; set; }
    }
}