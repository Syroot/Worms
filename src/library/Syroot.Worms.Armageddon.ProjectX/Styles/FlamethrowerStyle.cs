using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class FlamethrowerStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int FuelAmount { get; set; }

        [BinaryMember(Order = 2)] public int FireIntensity { get; set; }

        [BinaryMember(Order = 3)] public int FireAmount { get; set; }

        [BinaryMember(Order = 4)] public int FireBurnTime { get; set; }

        [BinaryMember(Order = 5, BooleanCoding = BooleanCoding.Dword)] public bool RemainOnTerrain { get; set; }
    }
}