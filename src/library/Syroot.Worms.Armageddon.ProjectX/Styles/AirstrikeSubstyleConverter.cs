using System;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class AirstrikeSubstyleConverter : IBinaryConverter
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public object? Read(Stream stream, object instance, BinaryMemberAttribute memberAttribute,
            ByteConverter byteConverter)
        {
            WeaponAirstrikeSubstyle airstrikeSubstyle = instance switch
            {
                AirstrikeStyle airstrikeStyle => airstrikeStyle.AirstrikeSubstyle,
                _ => throw new NotImplementedException(),
            };
            return airstrikeSubstyle switch
            {
                WeaponAirstrikeSubstyle.Mines => stream.ReadObject<MineStyle>(),
                WeaponAirstrikeSubstyle.Launcher => stream.ReadObject<LauncherStyle>(),
                _ => null
            };
        }

        /// <inheritdoc/>
        public void Write(Stream stream, object instance, BinaryMemberAttribute memberAttribute, object value,
            ByteConverter byteConverter)
        {
            stream.WriteObject(value);
        }
    }
}