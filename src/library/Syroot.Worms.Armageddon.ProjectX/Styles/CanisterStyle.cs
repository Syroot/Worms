using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class CanisterStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int SpriteInactive { get; set; }

        [BinaryMember(Order = 2)] public int SpriteActive { get; set; }

        [BinaryMember(Order = 3)] public int DiseasePoints { get; set; }

        [BinaryMember(Order = 4)] public int MaxDamage { get; set; }
    }
}