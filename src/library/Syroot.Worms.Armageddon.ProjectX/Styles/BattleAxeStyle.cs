using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class BattleAxeStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int PercentualDamage { get; set; }
    }
}