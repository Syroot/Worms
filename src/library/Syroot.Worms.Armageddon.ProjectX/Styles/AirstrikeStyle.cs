using Syroot.BinaryData;

namespace Syroot.Worms.Armageddon.ProjectX
{
    public class AirstrikeStyle : IStyle
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public int PlaneSprite;

        [BinaryMember(Order = 2)] public int BombsCount;

        [BinaryMember(Order = 3)] public int DropDistance;

        [BinaryMember(Order = 4)] public int HorizontalSpeed;

        [BinaryMember(Order = 5)] public int Sound;

        [BinaryMember(Order = 6)] public WeaponAirstrikeSubstyle AirstrikeSubstyle;

        [BinaryMember(Order = 7, Converter = typeof(AirstrikeSubstyleConverter))] public IStyle? Style;
    }

    public enum WeaponAirstrikeSubstyle : int
    {
        Mines,
        Worms,
        Launcher
    }
}