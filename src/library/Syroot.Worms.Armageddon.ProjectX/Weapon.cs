using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Syroot.BinaryData;
using Syroot.Worms.IO;

namespace Syroot.Worms.Armageddon.ProjectX
{
    [DebuggerDisplay("Weapon Name={Name}")]
    public class Weapon : ILoadable, ISaveable
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [BinaryMember(Order = 1)] public WeaponVersion Version { get; set; }

        [BinaryMember(Order = 2)] public long Checksum { get; set; }

        [BinaryMember(Order = 3)] public int TableRow { get; set; }

        [BinaryMember(Order = 4)] public bool Remembered { get; set; }

        [BinaryMember(Order = 5)] public bool UsableInCavern { get; set; }

        [BinaryMember(Order = 6)] public int Shots { get; set; }

        [BinaryMember(Order = 7)] public bool ShotEndsTurn { get; set; }

        [BinaryMember(Order = 8)] public int RetreatTime { get; set; }

        [BinaryMember(Order = 9)] public int Unknown1 { get; set; }

        [BinaryMember(Order = 10)] public int CrateChance { get; set; }

        [BinaryMember(Order = 11)] public int CrateCount { get; set; }

        [BinaryMember(Order = 12)] public int Unknown2 { get; set; }

        [BinaryMember(Order = 13)] public WeaponActivation Activation { get; set; }

        [BinaryMember(Order = 14)] public int NotUsed { get; set; }

        [BinaryMember(Order = 15)] public int ThrowHerdCount { get; set; }

        [BinaryMember(Order = 16)] public int AirstrikeSubtype { get; set; }

        [BinaryMember(Order = 17)] public WeaponSpacebarAction SpacebarAction { get; set; }

        [BinaryMember(Order = 18)] public WeaponCrosshairAction CrosshairAction { get; set; }

        [BinaryMember(Order = 19)] public WeaponThrowAction ThrowAction { get; set; }

        [BinaryMember(Order = 20)] public IStyle? Style { get; set; }

        [BinaryMember(Order = 21)] public bool AmmunitionOverride { get; set; }

        [BinaryMember(Order = 22)] public int Ammunition { get; set; }

        [BinaryMember(Order = 23)] public int Unknown3 { get; set; }

        [BinaryMember(Order = 24)] public int WeaponSprite { get; set; }

        [BinaryMember(Order = 25)] public string NameLong { get; set; } = String.Empty;

        [BinaryMember(Order = 26)] public string Name { get; set; } = String.Empty;

        [BinaryMember(Order = 27)] public string GridImageFile { get; set; } = String.Empty;

        [BinaryMember(Order = 28)] public string GfxDirectoryFile { get; set; } = String.Empty;

        [BinaryMember(Order = 29)] public string[] SpriteNames { get; } = new string[5];

        [BinaryMember(Order = 30)] public bool DelayOverride { get; set; }

        [BinaryMember(Order = 31)] public int Delay { get; set; }

        [BinaryMember(Order = 32)] public bool UseLibrary { get; set; }

        [BinaryMember(Order = 33)] public string LibraryName { get; set; } = String.Empty;

        [BinaryMember(Order = 34)] public string LibraryWeaponName { get; set; } = String.Empty;

        [BinaryMember(Order = 35)] public string AimSpriteEven { get; set; } = String.Empty;

        [BinaryMember(Order = 36)] public string AimSpriteUphill { get; set; } = String.Empty;

        [BinaryMember(Order = 37)] public string AimSpriteDownhill { get; set; } = String.Empty;

        [BinaryMember(Order = 38)] public string PickSpriteEven { get; set; } = String.Empty;

        [BinaryMember(Order = 39)] public string PickSpriteUphill { get; set; } = String.Empty;

        [BinaryMember(Order = 40)] public string PickSpriteDownhill { get; set; } = String.Empty;

        [BinaryMember(Order = 41)] public string FireSpriteEven { get; set; } = String.Empty;

        [BinaryMember(Order = 42)] public string FireSpriteUphill { get; set; } = String.Empty;

        [BinaryMember(Order = 43)] public string FireSpriteDownhill { get; set; } = String.Empty;

        [BinaryMember(Order = 44)] public bool AimSpriteOverride { get; set; }

        [BinaryMember(Order = 45)] public bool PickSpriteOverride { get; set; }

        [BinaryMember(Order = 46)] public bool FireSpriteOverride { get; set; }

        [BinaryMember(Order = 47)] public bool Utility { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public void Load(Stream stream)
        {
            using BinaryStream reader = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);

            // Read the header.
            long offset = reader.Position;
            Version = reader.ReadEnum<WeaponVersion>(true);
            Checksum = reader.ReadInt64();

            // Read general settings.
            TableRow = reader.ReadInt32();
            Remembered = reader.ReadBoolean(BooleanCoding.Dword);
            UsableInCavern = reader.ReadBoolean(BooleanCoding.Dword);
            Shots = reader.ReadInt32();
            ShotEndsTurn = reader.ReadBoolean(BooleanCoding.Dword);
            RetreatTime = reader.ReadInt32();
            Unknown1 = reader.ReadInt32();
            CrateChance = reader.ReadInt32();
            CrateCount = reader.ReadInt32();
            Unknown2 = reader.ReadInt32();

            // Read the activation and the corresponding weapon settings.
            Activation = reader.ReadEnum<WeaponActivation>(false);
            switch (Activation)
            {
                case WeaponActivation.Airstrike:
                    AirstrikeSubtype = reader.ReadInt32();
                    Style = reader.ReadObject<AirstrikeStyle>();
                    break;
                case WeaponActivation.Crosshair:
                    NotUsed = reader.ReadInt32();
                    CrosshairAction = reader.ReadEnum<WeaponCrosshairAction>(false);
                    Style = CrosshairAction switch
                    {
                        WeaponCrosshairAction.Bow => reader.ReadObject<BowStyle>(),
                        WeaponCrosshairAction.Flamethrower => reader.ReadObject<FlamethrowerStyle>(),
                        WeaponCrosshairAction.Gun => reader.ReadObject<GunStyle>(),
                        WeaponCrosshairAction.Launcher => reader.ReadObject<LauncherStyle>(),
                        _ => null
                    };
                    break;
                case WeaponActivation.Spacebar:
                    SpacebarAction = reader.ReadEnum<WeaponSpacebarAction>(false);
                    Style = SpacebarAction switch
                    {
                        WeaponSpacebarAction.Armageddon => reader.ReadObject<LauncherStyle>(),
                        WeaponSpacebarAction.BaseballBat => reader.ReadObject<BaseballBatStyle>(),
                        WeaponSpacebarAction.BattleAxe => reader.ReadObject<BattleAxeStyle>(),
                        WeaponSpacebarAction.Blowtorch => reader.ReadObject<BlowtorchStyle>(),
                        WeaponSpacebarAction.Dragonball => reader.ReadObject<DragonballStyle>(),
                        WeaponSpacebarAction.Firepunch => reader.ReadObject<FirepunchStyle>(),
                        WeaponSpacebarAction.Jetpack => reader.ReadObject<JetpackStyle>(),
                        WeaponSpacebarAction.Kamikaze => reader.ReadObject<KamikazeStyle>(),
                        WeaponSpacebarAction.NinjaRope => reader.ReadObject<NinjaRopeStyle>(),
                        WeaponSpacebarAction.NuclearTest => reader.ReadObject<NuclearTestStyle>(),
                        WeaponSpacebarAction.Parachute => reader.ReadObject<ParachuteStyle>(),
                        WeaponSpacebarAction.PneumaticDrill => reader.ReadObject<PneumaticDrillStyle>(),
                        WeaponSpacebarAction.Prod => reader.ReadObject<ProdStyle>(),
                        WeaponSpacebarAction.SuicideBomber => reader.ReadObject<SuicideBomberStyle>(),
                        _ => null
                    };
                    break;
                case WeaponActivation.Throw:
                    ThrowHerdCount = reader.ReadInt32();
                    ThrowAction = reader.ReadEnum<WeaponThrowAction>(false);
                    Style = ThrowAction switch
                    {
                        WeaponThrowAction.Canister => reader.ReadObject<CanisterStyle>(),
                        WeaponThrowAction.Launcher => reader.ReadObject<LauncherStyle>(),
                        WeaponThrowAction.Mine => reader.ReadObject<MineStyle>(),
                        _ => null
                    };
                    break;
            }

            // Read additional settings.
            reader.Position = offset + 468;
            AmmunitionOverride = reader.ReadBoolean(BooleanCoding.Dword);
            Ammunition = reader.ReadInt32();
            Unknown3 = reader.ReadInt32();
            WeaponSprite = reader.ReadInt32();
            NameLong = reader.ReadString(StringCoding.Int32CharCount);
            Name = reader.ReadString(StringCoding.Int32CharCount);
            GridImageFile = reader.ReadString(StringCoding.Int32CharCount);
            GfxDirectoryFile = reader.ReadString(StringCoding.Int32CharCount);
            for (int i = 0; i < SpriteNames.Length; i++)
                SpriteNames[i] = reader.ReadString(StringCoding.Int32CharCount);
            DelayOverride = reader.ReadBoolean(BooleanCoding.Dword);
            Delay = reader.ReadInt32();

            UseLibrary = reader.ReadBoolean();
            if (UseLibrary)
            {
                LibraryName = reader.ReadString(StringCoding.Int32CharCount);
                LibraryWeaponName = reader.ReadString(StringCoding.Int32CharCount);
            }

            AimSpriteEven = reader.ReadString(StringCoding.Int32CharCount);
            AimSpriteUphill = reader.ReadString(StringCoding.Int32CharCount);
            AimSpriteDownhill = reader.ReadString(StringCoding.Int32CharCount);
            PickSpriteEven = reader.ReadString(StringCoding.Int32CharCount);
            PickSpriteUphill = reader.ReadString(StringCoding.Int32CharCount);
            PickSpriteDownhill = reader.ReadString(StringCoding.Int32CharCount);
            FireSpriteEven = reader.ReadString(StringCoding.Int32CharCount);
            FireSpriteUphill = reader.ReadString(StringCoding.Int32CharCount);
            FireSpriteDownhill = reader.ReadString(StringCoding.Int32CharCount);
            AimSpriteOverride = reader.ReadBoolean();
            PickSpriteOverride = reader.ReadBoolean();
            FireSpriteOverride = reader.ReadBoolean();
            if (Version == WeaponVersion.Version_0_8_0)
                Utility = reader.ReadBoolean();
        }

        /// <inheritdoc/>
        public void Save(Stream stream)
        {
            using BinaryStream writer = new BinaryStream(stream, encoding: Encoding.ASCII, leaveOpen: true);

            // Write the header.
            long offset = writer.Position;
            writer.WriteEnum(Version, true);
            writer.Write(Checksum);

            // Write the general settings.
            writer.Write(TableRow);
            writer.Write(Remembered, BooleanCoding.Dword);
            writer.Write(UsableInCavern, BooleanCoding.Dword);
            writer.Write(Shots);
            writer.Write(ShotEndsTurn, BooleanCoding.Dword);
            writer.Write(RetreatTime);
            writer.Write(Unknown1);
            writer.Write(CrateChance);
            writer.Write(CrateCount);
            writer.Write(Unknown2);

            // Write the activation and the corresponding weapon settings.
            writer.WriteEnum(Activation, true);
            switch (Activation)
            {
                case WeaponActivation.Airstrike:
                    writer.Write(AirstrikeSubtype);
                    writer.WriteObject(Style);
                    break;
                case WeaponActivation.Crosshair:
                    writer.Write(NotUsed);
                    writer.WriteEnum(CrosshairAction, true);
                    if (CrosshairAction != WeaponCrosshairAction.None)
                        writer.WriteObject(Style);
                    break;
                case WeaponActivation.Spacebar:
                    writer.WriteEnum(SpacebarAction, true);
                    switch (SpacebarAction)
                    {
                        case WeaponSpacebarAction.Armageddon:
                        case WeaponSpacebarAction.BaseballBat:
                        case WeaponSpacebarAction.BattleAxe:
                        case WeaponSpacebarAction.Blowtorch:
                        case WeaponSpacebarAction.Dragonball:
                        case WeaponSpacebarAction.Firepunch:
                        case WeaponSpacebarAction.Jetpack:
                        case WeaponSpacebarAction.Kamikaze:
                        case WeaponSpacebarAction.NinjaRope:
                        case WeaponSpacebarAction.NuclearTest:
                        case WeaponSpacebarAction.Parachute:
                        case WeaponSpacebarAction.PneumaticDrill:
                        case WeaponSpacebarAction.Prod:
                        case WeaponSpacebarAction.SuicideBomber:
                            writer.WriteObject(Style);
                            break;
                    }
                    break;
                case WeaponActivation.Throw:
                    writer.Write(ThrowHerdCount);
                    writer.WriteEnum(ThrowAction, true);
                    if (ThrowAction != WeaponThrowAction.None)
                        writer.WriteObject(Style);
                    break;
            }

            // Write additional settings.
            writer.Position = offset + 468;
            writer.Write(AmmunitionOverride, BooleanCoding.Dword);
            writer.Write(Ammunition);
            writer.Write(Unknown3);
            writer.Write(WeaponSprite);
            writer.Write(NameLong, StringCoding.Int32CharCount);
            writer.Write(Name, StringCoding.Int32CharCount);
            writer.Write(GridImageFile, StringCoding.Int32CharCount);
            writer.Write(GfxDirectoryFile, StringCoding.Int32CharCount);
            writer.Write(SpriteNames, StringCoding.Int32CharCount);
            writer.Write(DelayOverride, BooleanCoding.Dword);
            writer.Write(Delay);

            writer.Write(UseLibrary);
            if (UseLibrary)
            {
                writer.Write(LibraryName, StringCoding.Int32CharCount);
                writer.Write(LibraryWeaponName, StringCoding.Int32CharCount);
            }

            writer.Write(AimSpriteEven, StringCoding.Int32CharCount);
            writer.Write(AimSpriteUphill, StringCoding.Int32CharCount);
            writer.Write(AimSpriteDownhill, StringCoding.Int32CharCount);
            writer.Write(PickSpriteEven, StringCoding.Int32CharCount);
            writer.Write(PickSpriteUphill, StringCoding.Int32CharCount);
            writer.Write(PickSpriteDownhill, StringCoding.Int32CharCount);
            writer.Write(FireSpriteEven, StringCoding.Int32CharCount);
            writer.Write(FireSpriteUphill, StringCoding.Int32CharCount);
            writer.Write(FireSpriteDownhill, StringCoding.Int32CharCount);
            writer.Write(AimSpriteOverride);
            writer.Write(PickSpriteOverride);
            writer.Write(FireSpriteOverride);
            if (Version == WeaponVersion.Version_0_8_0)
                writer.Write(Utility);
        }
    }

    public enum WeaponVersion : int
    {
        Version_0_8_0_pre = 0x5ABBDD05,
        Version_0_8_0 = 0x5ABBDD06
    }

    public enum WeaponActivation : int
    {
        None,
        Crosshair,
        Throw,
        Airstrike,
        Spacebar
    }

    public enum WeaponSpacebarAction : int
    {
        None,
        Firepunch,
        BaseballBat,
        Dragonball,
        Kamikaze,
        SuicideBomber,
        NinjaRope,
        Bungee,
        PneumaticDrill,
        Prod,
        Teleport,
        Blowtorch,
        Parachute,
        Surrender,
        SkipGo,
        SelectWorm,
        NuclearTest,
        Girder,
        BattleAxe,
        Utility,
        Freeze,
        Earthquake,
        ScalesOfJustice,
        Jetpack,
        Armageddon
    }

    public enum WeaponCrosshairAction : int
    {
        None,
        Flamethrower,
        Gun,
        Launcher,
        Bow
    }

    public enum WeaponThrowAction : int
    {
        None,
        Mine,
        Launcher,
        Canister
    }

    public enum WeaponSpriteNameIndex
    {
        Unknown0,
        Launcher,
        Cluster,
        Home,
        Unknown4
    }
}
