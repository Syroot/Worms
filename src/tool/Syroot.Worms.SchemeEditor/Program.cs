using System;
using System.Net.Http;
using System.Threading.Tasks;
using BlazorDownloadFile;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Syroot.Worms.SchemeEditor.Services;

namespace Syroot.Worms.SchemeEditor
{
    public class Program
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public static async Task Main(string[] args)
        {
            WebAssemblyHostBuilder builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("app");
            builder.Services.AddTransient(sp => new HttpClient
            {
                BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)
            });
            builder.Services.AddBlazorDownloadFile();
            builder.Services.AddSingleton<AppData>();
            await builder.Build().RunAsync();
        }
    }
}
