﻿using System;
using Syroot.Worms.Armageddon;

namespace Syroot.Worms.SchemeEditor.Services
{
    /// <summary>
    /// Represents a service storing data which is globally available to the web app.
    /// </summary>
    public class AppData
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const string _defaultSchemeName = "Unnamed Scheme";

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private string _schemeName = _defaultSchemeName;
        private Scheme _scheme = new Scheme();

        // ---- EVENTS -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Raised when a property was changed.
        /// </summary>
        public event Action? OnChange;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the name of the current scheme. This is <see langword="null"/> if no scheme has been loaded
        /// yet.
        /// </summary>
        public string SchemeName
        {
            get => _schemeName;
            set => Set(out _schemeName, String.IsNullOrEmpty(value) ? _defaultSchemeName : value);
        }

        /// <summary>
        /// Gets or sets the currently loaded scheme.
        /// </summary>
        public Scheme Scheme
        {
            get => _scheme;
            set => Set(out _scheme, value);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void Set<T>(out T backingField, T value)
        {
            backingField = value;
            OnChange?.Invoke();
        }
    }
}
