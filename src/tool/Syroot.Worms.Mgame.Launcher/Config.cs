﻿using System.Collections.Generic;
using System.Net;

namespace Syroot.Worms.Mgame.Launcher
{
    internal class Config
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string ServerIP { get; set; }
        public ushort ServerPort { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        public int PasswordKey { get; set; }

        public string ExecutablePaths { get; set; } = "DWait.exe;Main.exe";
        public string ExecutableArgs { get; set; }
        public string MappingName { get; set; } = "KG1234567890";
        public bool StartSuspended { get; set; }

        internal IList<string> ExecutePathList => ExecutablePaths.Split(';');
        internal IPEndPoint ServerEndPoint => new IPEndPoint(IPAddress.Parse(ServerIP), ServerPort);
    }
}
