﻿using System;
using System.ComponentModel;
using static Syroot.Worms.Mgame.Launcher.WinApi.Kernel32;
using static Syroot.Worms.Mgame.Launcher.WinApi.WinBase;
using static Syroot.Worms.Mgame.Launcher.WinApi.WinBase.WaitResult;

namespace Syroot.Worms.Mgame.Launcher
{
    /// <summary>
    /// Represents a Win32 process providing specific functionality not available in the .NET <see cref="Process"/>
    /// class.
    /// </summary>
    internal class NativeProcess : IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly IntPtr _processHandle;
        private readonly IntPtr _mainThreadHandle;
        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal NativeProcess(string executablePath, string commandLine, string currentDirectory = null,
            NativeProcessFlags flags = NativeProcessFlags.None)
        {
            commandLine = String.Join(" ", executablePath, commandLine).TrimEnd();

            ProcessCreationFlags creationFlags = ProcessCreationFlags.None;
            if (flags.HasFlag(NativeProcessFlags.StartSuspended))
                creationFlags |= ProcessCreationFlags.CREATE_SUSPENDED;

            STARTUPINFO startupInfo = new STARTUPINFO();

            if (!CreateProcess(null, commandLine, IntPtr.Zero, IntPtr.Zero,
                flags.HasFlag(NativeProcessFlags.InheritHandles), creationFlags, IntPtr.Zero, currentDirectory,
                ref startupInfo, out PROCESS_INFORMATION processInfo))
                throw new InvalidOperationException("Could not create process.", new Win32Exception());

            _processHandle = processInfo.hProcess;
            _mainThreadHandle = processInfo.hThread;
        }

        ~NativeProcess()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing).
            Dispose(false);
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing).
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Resume()
        {
            if (ResumeThread(_mainThreadHandle) == UInt32.MaxValue)
                throw new InvalidOperationException("Could not resume process.", new Win32Exception());
        }

        internal void Terminate()
        {
            if (!TerminateProcess(_processHandle, 0))
                throw new InvalidOperationException("Could not terminate process.", new Win32Exception());
        }

        internal void WaitForExit()
        {
            if (WaitForSingleObject(_processHandle, INFINITE) == WAIT_FAILED)
                throw new InvalidOperationException("Could not wait for process.", new Win32Exception());
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                CloseHandle(_processHandle);
                _disposed = true;
            }
        }
    }

    [Flags]
    internal enum NativeProcessFlags
    {
        None = 0,
        InheritHandles = 1 << 0,
        StartSuspended = 1 << 1
    }
}
