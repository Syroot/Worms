﻿using System;

namespace Syroot.Worms.Mgame.Launcher.WinApi
{
    /// <summary>
    /// Represents WinAPI definitions from WinBase.h.
    /// </summary>
    internal static class WinBase
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        internal const uint INFINITE = UInt32.MaxValue;

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        internal enum WaitResult : uint
        {
            WAIT_OBJECT_0 = 0,
            WAIT_ABANDONED = 1 << 7,
            WAIT_TIMEOUT = 258,
            WAIT_FAILED = UInt32.MaxValue
        }
    }
}
