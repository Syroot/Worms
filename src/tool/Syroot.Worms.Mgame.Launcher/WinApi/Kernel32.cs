﻿using System;
using System.Runtime.InteropServices;
using static Syroot.Worms.Mgame.Launcher.WinApi.WinBase;

namespace Syroot.Worms.Mgame.Launcher.WinApi
{
    internal static class Kernel32
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        [DllImport(nameof(Kernel32), SetLastError = true)]
        internal static extern bool CloseHandle(IntPtr hObject);

        [DllImport(nameof(Kernel32), SetLastError = true)]
        internal static extern bool CreateProcess(string lpApplicationName, string lpCommandLine,
            IntPtr lpProcessAttributes, IntPtr lpThreadAttributes, bool bInheritHandles,
            ProcessCreationFlags dwCreationFlags, IntPtr lpEnvironment, string lpCurrentDirectory,
            ref STARTUPINFO lpStartupInfo, out PROCESS_INFORMATION lpProcessInformation);

        [DllImport(nameof(Kernel32), SetLastError = true)]
        internal static extern uint ResumeThread(IntPtr hThread);

        [DllImport(nameof(Kernel32), SetLastError = true)]
        internal static extern bool TerminateProcess(IntPtr hProcess, uint uExitCode);

        [DllImport(nameof(Kernel32), SetLastError = true)]
        internal static extern WaitResult WaitForSingleObject(IntPtr hHandle, uint dwMilliseconds);

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        internal struct PROCESS_INFORMATION
        {
            internal IntPtr hProcess;
            internal IntPtr hThread;
            internal uint dwProcessId;
            internal uint dwThreadId;
        };

        internal struct STARTUPINFO
        {
            internal uint cb;
            internal string lpReserved;
            internal string lpDesktop;
            internal string lpTitle;
            internal uint dwX;
            internal uint dwY;
            internal uint dwXSize;
            internal uint dwYSize;
            internal uint dwXCountChars;
            internal uint dwYCountChars;
            internal uint dwFillAttribute;
            internal uint dwFlags;
            internal ushort wShowWindow;
            internal ushort cbReserved2;
            internal IntPtr lpReserved2;
            internal IntPtr hStdInput;
            internal IntPtr hStdOutput;
            internal IntPtr hStdError;
        };

        internal enum ProcessCreationFlags : uint
        {
            None,

            DEBUG_PROCESS = 1 << 0,
            DEBUG_ONLY_THIS_PROCESS = 1 << 1,
            CREATE_SUSPENDED = 1 << 2,
            DETACHED_PROCESS = 1 << 3,

            CREATE_NEW_CONSOLE = 1 << 4,
            NORMAL_PRIORITY_CLASS = 1 << 5,
            IDLE_PRIORITY_CLASS = 1 << 6,
            HIGH_PRIORITY_CLASS = 1 << 7,

            REALTIME_PRIORITY_CLASS = 1 << 8,
            CREATE_NEW_PROCESS_GROUP = 1 << 9,
            CREATE_UNICODE_ENVIRONMENT = 1 << 10,
            CREATE_SEPARATE_WOW_VDM = 1 << 11,

            CREATE_SHARED_WOW_VDM = 1 << 12,
            CREATE_FORCEDOS = 1 << 13,
            BELOW_NORMAL_PRIORITY_CLASS = 1 << 14,
            ABOVE_NORMAL_PRIORITY_CLASS = 1 << 15,

            INHERIT_PARENT_AFFINITY = 1 << 16,
            [Obsolete] INHERIT_CALLER_PRIORITY = 1 << 17,
            CREATE_PROTECTED_PROCESS = 1 << 18,
            EXTENDED_STARTUPINFO_PRESENT = 1 << 19,

            PROCESS_MODE_BACKGROUND_BEGIN = 1 << 20,
            PROCESS_MODE_BACKGROUND_END = 1 << 21,
            CREATE_SECURE_PROCESS = 1 << 22,

            CREATE_BREAKAWAY_FROM_JOB = 1 << 24,
            CREATE_PRESERVE_CODE_AUTHZ_LEVEL = 1 << 25,
            CREATE_DEFAULT_ERROR_MODE = 1 << 26,
            CREATE_NO_WINDOW = 1 << 27,

            PROFILE_USER = 1 << 28,
            PROFILE_KERNEL = 1 << 29,
            PROFILE_SERVER = 1 << 30,
            CREATE_IGNORE_SYSTEM_DEFAULT = 1u << 31
        }
    }
}
