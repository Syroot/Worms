﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Server
{
    /// <summary>
    /// Represents the client request for an <see cref="OptionsReply"/>.
    /// </summary>
    [WwpaPacket(0x8046)]
    internal class OptionsQuery : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string UserName { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            UserName = reader.ReadString2();
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
