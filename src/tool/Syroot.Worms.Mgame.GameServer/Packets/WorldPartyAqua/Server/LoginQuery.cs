﻿using System;
using System.Collections.Generic;
using Syroot.BinaryData.Memory;
using Syroot.Worms.Mgame.GameServer.Packets.Data;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Server
{
    /// <summary>
    /// Represents the client request for a <see cref="LoginReply"/>.
    /// </summary>
    [WwpaPacket(0x8019)]
    internal class LoginQuery : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the names and passwords of the players to login.
        /// </summary>
        public IList<PlayerCredentials> Players { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            Players = new List<PlayerCredentials> { reader.ReadCredentials() };
            if (reader.ReadBoolean())
                Players.Add(reader.ReadCredentials());
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
