﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Server
{
    /// <summary>
    /// Represents the server response to an <see cref="OptionsSaveQuery"/>.
    /// </summary>
    [WwpaPacket(0x8049)]
    internal class OptionsSaveReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public bool Success { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteBoolean4(Success);
        }
    }
}
