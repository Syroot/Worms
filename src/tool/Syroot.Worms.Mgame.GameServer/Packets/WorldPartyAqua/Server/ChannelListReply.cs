﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Syroot.BinaryData.Memory;
using Syroot.Worms.Mgame.GameServer.Core.IO;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Server
{
    /// <summary>
    /// Represents the server reply to a <see cref="ChannelListQuery"/>.
    /// </summary>
    [WwpaPacket(0x801F)]
    internal class ChannelListReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public IList<ChannelInfo> Channels { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteUInt16((ushort)Channels.Count);
            writer.WriteByte((byte)Channels.Count(x => x.Type == ChannelType.Normal)); // index 0
            writer.WriteByte((byte)Channels.Count(x => x.Type == ChannelType.Roping)); // index 3
            writer.WriteByte((byte)Channels.Count(x => x.Type == ChannelType.Guild)); // index 1
            writer.WriteByte((byte)Channels.Count(x => x.Type == ChannelType.Event)); // index 2
            foreach (ChannelInfo channel in Channels)
            {
                writer.WriteUInt16(channel.UnknownA);
                writer.WriteString2(channel.Name);
                writer.WriteIPEndPoint(channel.EndPoint);
                writer.WriteEnum(channel.Type);
                writer.WriteByte(channel.UnknownD);
                writer.WriteEnum(channel.Status);
            }
        }
    }

    internal class ChannelInfo
    {
        internal ushort UnknownA { get; set; }

        /// <summary>
        /// Gets or sets the name of the channel. Should not exceed 32 characters.
        /// </summary>
        internal string Name { get; set; }

        /// <summary>
        /// Gets or sets the IP address to connect to.
        /// </summary>
        internal IPEndPoint EndPoint { get; set; }

        /// <summary>
        /// Gets or sets the category in which the channel will appear.
        /// </summary>
        internal ChannelType Type { get; set; }

        internal byte UnknownD { get; set; }

        /// <summary>
        /// Gets or sets the channel load or status.
        /// </summary>
        internal ChannelStatus Status { get; set; }
    }

    internal enum ChannelType : byte
    {
        Normal = 0,
        Roping = 1,
        Guild = 2,
        Event = 3
    }

    internal enum ChannelStatus : byte
    {
        Closed = 0,
        Load1 = 1,
        Load2 = 2,
        Load3 = 3,
        Load4 = 4,
        Load5 = 5,
        Full = 6
    }
}
