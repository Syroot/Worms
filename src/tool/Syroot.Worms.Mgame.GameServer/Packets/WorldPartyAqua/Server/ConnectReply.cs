﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Server
{
    /// <summary>
    /// Represents the server response to a <see cref="ConnectQuery"/>.
    /// </summary>
    [WwpaPacket(0x8002)]
    internal class ConnectReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets an error message displayed to prevent the connection attempt. This property must be
        /// <see langword="null"/> to allow further communication.
        /// </summary>
        internal string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the server version reported to the client, and checked by it to allow further communication.
        /// </summary>
        internal ushort ServerVersion { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            bool connectionFailed = ErrorMessage != null;
            writer.WriteBoolean4(connectionFailed);
            if (connectionFailed)
                writer.WriteString2(ErrorMessage);
            else
                writer.WriteUInt16(ServerVersion);
        }
    }
}
