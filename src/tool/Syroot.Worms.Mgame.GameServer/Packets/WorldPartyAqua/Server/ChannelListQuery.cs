﻿using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Server
{
    /// <summary>
    /// Represents the client request for a <see cref="ChannelListReply"/>.
    /// </summary>
    [WwpaPacket(0x801E)]
    internal class ChannelListQuery : IPacket
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) { }

        public void Save(ref SpanWriter writer) { }
    }
}
