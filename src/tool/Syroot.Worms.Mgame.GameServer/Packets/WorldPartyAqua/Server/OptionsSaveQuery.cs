﻿using System;
using System.Collections.Generic;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Server
{
    /// <summary>
    /// Represents the client request for an <see cref="OptionsSaveReply"/>.
    /// </summary>
    [WwpaPacket(0x8048)]
    internal class OptionsSaveQuery : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string UserName { get; set; }

        public bool Unknown { get; set; }

        public IList<string> WormNames { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            UserName = reader.ReadString2();
            Unknown = reader.ReadBoolean();
            WormNames = new List<string> { reader.ReadString2(), reader.ReadString2(), reader.ReadString2() };
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
