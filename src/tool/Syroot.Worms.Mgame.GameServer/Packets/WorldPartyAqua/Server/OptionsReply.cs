﻿using System;
using System.Collections.Generic;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Server
{
    /// <summary>
    /// Represents the server response to a <see cref="OptionsQuery"/>.
    /// </summary>
    [WwpaPacket(0x8047)]
    internal class OptionsReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public bool Success { get; set; }

        public byte Unknown { get; set; }

        public IList<string> WormNames { get; set; } // 3 elements, max. 20 chars each

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteBoolean4(Success);
            if (Success)
            {
                writer.WriteByte(Unknown);
                writer.WriteString2(WormNames[0]);
                writer.WriteString2(WormNames[1]);
                writer.WriteString2(WormNames[2]);
            }
        }
    }
}
