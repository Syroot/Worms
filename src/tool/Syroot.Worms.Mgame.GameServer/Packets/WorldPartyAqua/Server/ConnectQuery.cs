﻿using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Server
{
    /// <summary>
    /// Represents the client request for a <see cref="ConnectReply"/>.
    /// </summary>
    [WwpaPacket(0x8001)]
    internal class ConnectQuery : IPacket
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) { }

        public void Save(ref SpanWriter writer) { }
    }
}
