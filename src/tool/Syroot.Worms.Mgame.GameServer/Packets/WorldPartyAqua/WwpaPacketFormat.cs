﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Syroot.BinaryData;
using Syroot.BinaryData.Memory;
using Syroot.Worms.Mgame.GameServer.Core.Reflection;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua
{
    /// <summary>
    /// Represents the implementation of a specific packet format which can instantiate corresponding classes.
    /// </summary>
    internal class WwpaPacketFormat : IPacketFormat
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const ushort _tagStart = 0x2E9E;
        private const ushort _tagEnd = 0x7F3F;
        private const int _channelCmdQueryID = 0x10A;
        private const int _channelCmdReplyID = 0x10B;
        private const int _maxDataSize = 1024;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly IList<(Type type, WwpaPacketAttribute attrib)> _cachedClasses
             = ReflectionTools.GetAttributedClasses<WwpaPacketAttribute>();

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        private WwpaPacketFormat() { }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal static WwpaPacketFormat Instance { get; } = new WwpaPacketFormat();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public IPacket TryLoad(Stream stream)
        {
            // Read head.
            if (stream.ReadUInt16() != _tagStart
                || !stream.ReadBoolean() // bHead2, always true
                || stream.ReadBoolean() // bHead3, always false
                || !stream.ReadBoolean()) // bCompressed, always true
                return null;
            int decompressedSize = stream.ReadUInt16();
            int compressedSize = stream.ReadInt32();
            int idxPacket = stream.ReadInt32();

            // Read data.
            byte[] compressedData = stream.ReadBytes(compressedSize);

            // Read tail.
            if (stream.ReadInt32() != idxPacket || stream.ReadUInt16() != _tagEnd)
                return null;

            // Instantiate, deserialize, and return packet.
            SpanReader reader = new SpanReader(PacketCompression.Decompress(compressedData), encoding: Encodings.Korean);
            IPacket packet = GetPacket(ref reader);
            if (packet == null)
                return null;
            SpanReader dataReader = reader.Slice();
            packet.Load(ref dataReader);
            return packet;
        }

        public bool TrySave(Stream stream, IPacket packet)
        {
            WwpaPacketAttribute attrib = GetAttribute(packet);
            if (attrib == null)
                return false;

            // Retrieve (decompressed) data.
            SpanWriter writer = new SpanWriter(new byte[_maxDataSize], encoding: Encodings.Korean);
            writer.WriteInt32(attrib.ID);
            if (attrib.Command != default)
                writer.WriteInt32(attrib.Command);
            SpanWriter dataWriter = writer.Slice();
            packet.Save(ref dataWriter);
            writer.Position += dataWriter.Position;

            // Send head and data.
            stream.WriteUInt16(_tagStart);
            stream.WriteBoolean(true); // bHead2 (unknown)
            stream.WriteBoolean(false); // bHead3 (unknown)
            bool compress = false; // TODO: Does not get accepted by client if compressed.
            stream.WriteBoolean(compress);
            if (compress)
            {
                ReadOnlySpan<byte> compressedData = PacketCompression.Compress(writer.Span);
                stream.WriteUInt16((ushort)writer.Position); // decompressed size
                stream.WriteUInt32((uint)compressedData.Length); // compressed size
                stream.WriteInt32(1); // index
                stream.Write(compressedData);
            }
            else
            {
                stream.WriteUInt16(0); // unused
                stream.WriteUInt32((uint)writer.Position); // raw size
                stream.WriteInt32(1); // index
                stream.Write(writer.Span.Slice(0, writer.Position));
            }

            // Send tail.
            stream.WriteInt32(1); // index (must equal other)
            stream.WriteUInt16(_tagEnd);
            return true;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private IPacket GetPacket(ref SpanReader reader)
        {
            // Try to find a unique packet via ID only.
            int id = reader.ReadInt32();
            var packets = _cachedClasses.Where(x => x.attrib.ID == id).ToList();
            if (packets.Count == 1)
                return (IPacket)Activator.CreateInstance(packets[0].type);

            // No unique packet found, check for matching command.
            if (!reader.IsEndOfSpan)
            {
                int command = reader.ReadInt32();
                packets = packets.Where(x => x.attrib.Command == command).ToList();
                if (packets.Count == 1)
                    return (IPacket)Activator.CreateInstance(packets[0].type);

                // No unique packet found, return fallback.
                reader.Position -= sizeof(int);
            }
            return new RawPacket { ID = id };
        }

        private WwpaPacketAttribute GetAttribute(IPacket packet)
        {
            Type packetType = packet.GetType();
            return _cachedClasses.Where(x => x.type == packetType).FirstOrDefault().attrib;
        }
    }
}
