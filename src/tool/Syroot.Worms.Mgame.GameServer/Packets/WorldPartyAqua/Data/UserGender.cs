﻿namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Data
{
    internal enum UserGender : byte
    {
        Male = 1,
        Female = 2
    }
}
