﻿using Syroot.BinaryData.Memory;
using Syroot.Worms.Mgame.GameServer.Core.IO;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua
{
    /// <summary>
    /// Represents a fallback WWPA packet.
    /// </summary>
    public class RawPacket : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int ID { get; set; }

        public byte[] Data { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            Data = reader.ReadToEnd();
        }

        public void Save(ref SpanWriter writer)
        {
            writer.WriteBytes(Data);
        }
    }
}
