﻿using System;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua
{
    /// <summary>
    /// Decorates an <see cref="IPacket"/> child class with the ID of the OW server packet it represents.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class WwpaPacketAttribute : Attribute
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="WwpaPacketAttribute"/> class, decorating a class with the ID of
        /// the packet it represents.
        /// </summary>
        /// <param name="id">The ID of the packet which the decorated class represents.</param>
        public WwpaPacketAttribute(int id)
        {
            ID = id;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the ID of the packet the decorated class represents.
        /// </summary>
        public int ID { get; }

        /// <summary>
        /// Gets or sets an optional command number which the packet represents.
        /// </summary>
        public int Command { get; set; }
    }
}
