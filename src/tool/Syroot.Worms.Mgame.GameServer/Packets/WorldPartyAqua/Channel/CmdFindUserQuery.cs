﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the client request for a <see cref="CmdFindUserReply"/>.
    /// </summary>
    [WwpaPacket(0x10A, Command = 1)]
    internal class CmdFindUserQuery : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the name of the user to search.
        /// </summary>
        public string UserName { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            UserName = reader.ReadString2();
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
