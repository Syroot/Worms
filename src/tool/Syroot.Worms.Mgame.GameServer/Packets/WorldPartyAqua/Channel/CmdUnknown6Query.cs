﻿using System;
using System.Net;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the client request for a <see cref="CmdUnknown6Reply"/>.
    /// </summary>
    [WwpaPacket(0x10A, Command = 6)]
    internal class CmdUnknown6Query : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int UnknownA { get; set; } // Always 1

        public IPAddress ClientIP { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            UnknownA = reader.ReadInt32();
            ClientIP = IPAddress.Parse(reader.ReadString2());
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
