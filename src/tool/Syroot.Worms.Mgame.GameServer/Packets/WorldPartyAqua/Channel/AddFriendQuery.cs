﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the client request for an <see cref="AddFriendReply"/>.
    /// </summary>
    [WwpaPacket(0x15C)]
    internal class AddFriendQuery : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the name of the user to add as a friend.
        /// </summary>
        public string UserName { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            UserName = reader.ReadString2();
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
