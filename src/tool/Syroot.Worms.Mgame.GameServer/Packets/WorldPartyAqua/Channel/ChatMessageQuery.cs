﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the client request for a <see cref="ChatMessageReply"/>.
    /// </summary>
    [WwpaPacket(0x114)]
    internal class ChatMessageQuery : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int UnknownA { get; set; }

        public int UnknownB { get; set; } // Always 3?

        public string Message { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            UnknownA = reader.ReadInt32();
            UnknownB = reader.ReadInt32();
            Message = reader.ReadString2();
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
