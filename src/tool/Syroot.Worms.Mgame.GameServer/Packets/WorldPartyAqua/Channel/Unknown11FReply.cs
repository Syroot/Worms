﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the server response to a <see cref="Unknown11EQuery"/>.
    /// </summary>
    [WwpaPacket(0x11F)]
    internal class Unknown11FReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int UnknownA { get; set; }

        public string UnknownB { get; set; }

        public ushort UnknownC { get; set; }

        public ushort UnknownD { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteInt32(UnknownA);
            writer.WriteString2(UnknownB);
            writer.WriteUInt16(UnknownC);
            writer.WriteUInt16(UnknownD);
        }
    }
}
