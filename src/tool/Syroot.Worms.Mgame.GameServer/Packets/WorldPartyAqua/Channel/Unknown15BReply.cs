﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the server response to a <see cref="Unknown15AQuery"/>.
    /// </summary>
    [WwpaPacket(0x15B)]
    internal class Unknown15BReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public ushort UnknownA { get; set; }

        public int UnknownB { get; set; } // unused?

        public string UnknownC { get; set; }

        public ushort UnknownD { get; set; } // x - 1 / 5, unused?

        public int UnknownE { get; set; } // unused?

        public byte UnknownF { get; set; } // unused?

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteUInt16(UnknownA);
            writer.WriteInt32(UnknownB);
            writer.WriteString2(UnknownC);
            writer.WriteUInt16(UnknownD);
            writer.WriteInt32(UnknownE);
            writer.WriteByte(UnknownF);
        }
    }
}
