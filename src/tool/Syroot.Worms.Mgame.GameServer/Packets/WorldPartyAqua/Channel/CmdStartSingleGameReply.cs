﻿using System;
using System.Collections.Generic;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the <see cref="CmdQuery"/> server response to a <see cref="CmdStartSingleGameQuery"/>.
    /// </summary>
    [WwpaPacket(0x10B, Command = 5)]
    internal class CmdStartSingleGameReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public IList<StartSingleGameReplyStuff> Stuff { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteUInt16((ushort)Stuff.Count);
            foreach (StartSingleGameReplyStuff stuff in Stuff)
            {
                writer.WriteByte(stuff.UnknownA);
                writer.WriteByte(stuff.UnknownB);
                writer.WriteUInt16(stuff.UnknownC);
                writer.WriteUInt16(stuff.UnknownD);
                writer.WriteUInt16(stuff.UnknownE);
            }
        }
    }

    public class StartSingleGameReplyStuff
    {
        public byte UnknownA { get; set; }
        public byte UnknownB { get; set; }
        public ushort UnknownC { get; set; }
        public ushort UnknownD { get; set; }
        public ushort UnknownE { get; set; }
    }
}
