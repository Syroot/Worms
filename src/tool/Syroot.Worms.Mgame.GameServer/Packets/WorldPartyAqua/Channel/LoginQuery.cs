﻿using System;
using System.Collections.Generic;
using System.Net;
using Syroot.BinaryData.Memory;
using Syroot.Worms.Mgame.GameServer.Core.IO;
using Syroot.Worms.Mgame.GameServer.Packets.Data;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the client request for a <see cref="LoginReply"/>.
    /// </summary>
    [WwpaPacket(0x101)]
    internal class LoginQuery : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public IPAddress IPAddress { get; set; }
        public IList<PlayerCredentials> Players { get; set; }
        public byte UnknownA { get; set; }
        public byte UnknownB { get; set; }
        public byte[] Remain { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            IPAddress = IPAddress.Parse(reader.ReadString2());
            Players = new List<PlayerCredentials> { reader.ReadCredentials() };
            if (reader.ReadBoolean())
                Players.Add(reader.ReadCredentials());
            UnknownA = reader.ReadByte();
            UnknownB = reader.ReadByte();
            Remain = reader.ReadToEnd();
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
