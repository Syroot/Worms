﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the server response to a <see cref="LoginQuery"/>.
    /// </summary>
    [WwpaPacket(0x102)]
    internal class LoginReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public LoginResult Result { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteEnumSafe(Result);
        }
    }

    internal enum LoginResult : int
    {
        Success = 0,
        ChannelFull = 10,
        No2PSupport = 11,
        InvalidCredentials = 12,
        IDAlreadyTaken = 13,
        IDDoesNotExist = 14,
        PasswordDoesNotMatch = 15,
        Fail = 99
    }
}
