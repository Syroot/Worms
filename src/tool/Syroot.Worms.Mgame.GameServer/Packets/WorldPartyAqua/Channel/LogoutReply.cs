﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the server response to a <see cref="LogoutQuery"/>.
    /// </summary>
    [WwpaPacket(0x104)]
    internal class LogoutReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public bool Success { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteBoolean4(Success);
        }
    }
}
