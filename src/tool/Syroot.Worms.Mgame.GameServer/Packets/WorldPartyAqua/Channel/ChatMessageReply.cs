﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Represents the server response to a <see cref="ChatMessageQuery"/>.
    /// </summary>
    [WwpaPacket(0x115)]
    internal class ChatMessageReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public ChatMessageScene Scene { get; set; }

        public int UnknownA { get; set; }

        public string UnknownB { get; set; }

        public string Message { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteEnumSafe(Scene);
            writer.WriteInt32(UnknownA);
            writer.WriteString2(UnknownB);
            writer.WriteString2(Message);
        }
    }

    internal enum ChatMessageScene : int
    {
        WaitRoom = 1,
        GameRoom = 3,
        Current = 99
    }
}
