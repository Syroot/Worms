﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel
{
    /// <summary>
    /// Invalid description: "Represents the server response to a <see cref="LoginQuery"/>."
    /// </summary>
    [Obsolete("While this packet exists, it is currently unclear where it is used.")]
    [WwpaPacket(0x145)]
    internal class NotLoginReply : IPacket
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) { }

        public void Save(ref SpanWriter writer) { }
    }
}
