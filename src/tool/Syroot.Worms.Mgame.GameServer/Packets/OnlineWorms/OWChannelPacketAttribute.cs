﻿using System;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms
{
    /// <summary>
    /// Decorates an <see cref="IPacket"/> child class with the ID of the OW channel packet it represents.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class OWChannelPacketAttribute : Attribute
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="OWChannelPacketAttribute"/> class, decorating a class with the
        /// ID of the packet it represents.
        /// </summary>
        /// <param name="id">The ID of the packet which the decorated class represents.</param>
        public OWChannelPacketAttribute(byte id)
        {
            ID = id;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the ID of the packet the decorated class represents.
        /// </summary>
        public byte ID { get; }
    }
}
