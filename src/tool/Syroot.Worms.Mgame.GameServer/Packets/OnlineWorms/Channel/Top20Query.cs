﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Channel
{
    /// <summary>
    /// Represents the client request for a <see cref="LoginReply"/>.
    /// </summary>
    [OWChannelPacket(0x37)]
    internal class Top20Query : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets a value determining how many infos are requested. The client apparently always requests 20.
        /// </summary>
        public ushort Count { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            Count = reader.ReadUInt16();
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
