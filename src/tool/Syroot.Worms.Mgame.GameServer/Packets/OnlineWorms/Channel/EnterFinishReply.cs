﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Channel
{
    /// <summary>
    /// Represents an additional server response to a <see cref="Top20Query"/>, causing the client to switch
    /// to the channel screen (game lobby).
    /// </summary>
    [OWChannelPacket(0x44)]
    internal class EnterFinishReply : IPacket
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer) { }
    }
}
