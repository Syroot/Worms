﻿using System;
using System.Collections.Generic;
using System.Net;
using Syroot.BinaryData.Memory;
using Syroot.Worms.Mgame.GameServer.Packets.Data;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Channel
{
    /// <summary>
    /// Represents the client request for a <see cref="LoginReply"/>.
    /// </summary>
    [OWChannelPacket(0x10)]
    internal class LoginQuery : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public IList<PlayerCredentials> Players { get; set; }

        public IPAddress ClientIP { get; set; }

        public ushort ClientVersion { get; set; }

        public ushort UnknownA { get; set; } // Always 1?

        public byte UnknownB { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader)
        {
            Players = new List<PlayerCredentials>
            {
                new PlayerCredentials
                {
                    UserName = reader.ReadStringFix(12),
                    Password = reader.ReadStringFix(12)
                }
            };
            ClientIP = IPAddress.Parse(reader.ReadStringFix(15));
            ClientVersion = reader.ReadUInt16();
            UnknownA = reader.ReadUInt16();

            ushort additionalPlayers = reader.ReadUInt16();
            for (int i = 0; i < additionalPlayers; i++)
            {
                Players.Add(new PlayerCredentials
                {
                    UserName = reader.ReadStringFix(12),
                    Password = reader.ReadStringFix(12)
                });
            }
            UnknownB = reader.ReadByte();
        }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
