﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Channel
{
    /// <summary>
    /// Represents the server response to a <see cref="LoginQuery"/>.
    /// </summary>
    [OWChannelPacket(0x11)]
    internal class LoginReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public ChannelConnectResult Result { get; set; }

        public ChannelConnectPlayerInfo Player { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            if (Result == ChannelConnectResult.Success)
            {
                writer.WriteBoolean2(true);
                writer.WriteUInt16(0); // field_1E710
                writer.WriteUInt32(0); // field_1E714
                writer.WriteStringFix(Player.ID, 12);
                writer.WriteStringFix(Player.Name, 10);
                writer.WriteUInt16(Player.UnknownB);
                writer.WriteUInt16(Player.UnknownC);
                writer.WriteUInt16(Player.UnknownD);
                writer.WriteUInt16(Player.UnknownE);
                writer.WriteUInt16(Player.UnknownF);
                writer.WriteUInt16(Player.UnknownG);
                writer.WriteUInt16(Player.UnknownH);
                writer.WriteUInt16(Player.UnknownI);
                writer.WriteUInt16(Player.UnknownJ);
                writer.WriteUInt16(Player.UnknownK);
                writer.WriteUInt16(Player.UnknownL);
                writer.WriteUInt16(Player.UnknownM);
                writer.WriteUInt16(Player.UnknownN);
                writer.WriteUInt16(Player.UnknownO);
                writer.WriteUInt64(Player.Experience);
                writer.WriteUInt64(Player.Gold);
                writer.WriteUInt16(Player.Rank);
                writer.WriteUInt16(Player.GuildMarkIndex);
                writer.WriteStringFix(Player.GuildName, 12);
                writer.WriteUInt32(Player.UnknownS);
                writer.WriteUInt32(0); // v29
                writer.WriteUInt16(0); // v30
                writer.WriteUInt16(Player.UnknownT);
                writer.WriteUInt16(Player.UnknownU);
                writer.WriteUInt16(Player.UnknownV);
                writer.WriteUInt16(Player.UnknownW);
                writer.WriteStringFix(Player.UnknownX, 20);
                writer.WriteStringFix(Player.UnknownY, 20);
                writer.WriteStringFix(Player.UnknownZ, 20);
                writer.WriteStringFix(Player.UnknownAA, 20);
                writer.WriteUInt16(Player.UnknownAB);
                writer.WriteUInt16(Player.UnknownAC);
                // TODO: Handle UnknownAC counting something.
            }
            else
            {
                writer.WriteBoolean2(false);
                writer.WriteEnumSafe(Result);
            }
        }
    }

    internal class ChannelConnectPlayerInfo
    {
        public string ID { get; set; } // Max. 12 chars
        public string Name { get; set; } // Max. 10 chars
        public ushort UnknownB { get; set; }
        public ushort UnknownC { get; set; }
        public ushort UnknownD { get; set; }
        public ushort UnknownE { get; set; }
        public ushort UnknownF { get; set; }
        public ushort UnknownG { get; set; }
        public ushort UnknownH { get; set; }
        public ushort UnknownI { get; set; }
        public ushort UnknownJ { get; set; }
        public ushort UnknownK { get; set; }
        public ushort UnknownL { get; set; }
        public ushort UnknownM { get; set; }
        public ushort UnknownN { get; set; }
        public ushort UnknownO { get; set; }
        public ulong Experience { get; set; }
        public ulong Gold { get; set; }
        public ushort Rank { get; set; }
        public ushort GuildMarkIndex { get; set; }
        public string GuildName { get; set; } // Max. 12 chars
        public uint UnknownS { get; set; } // 4 bytes
        public ushort UnknownT { get; set; }
        public ushort UnknownU { get; set; }
        public ushort UnknownV { get; set; }
        public ushort UnknownW { get; set; }
        public string UnknownX { get; set; } // Max. 20 chars
        public string UnknownY { get; set; } // Max. 20 chars
        public string UnknownZ { get; set; } // Max. 20 chars
        public string UnknownAA { get; set; } // Max. 20 chars
        public ushort UnknownAB { get; set; }
        public ushort UnknownAC { get; set; } // counts something
    }

    internal enum ChannelConnectResult : ushort
    {
        Success,
        MissingID = 1,
        TooManyUsers = 2,
        IDInUse = 4,
        BadVersion = 5,
        Banned = 6,
        TooManyIDs = 16,
        IDNotRegisteredForCompetition = 17,
        TooManyPoints = 18,
        Unspecified = UInt16.MaxValue
    }
}
