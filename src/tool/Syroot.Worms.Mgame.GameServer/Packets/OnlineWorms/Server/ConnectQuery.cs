﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Server
{
    /// <summary>
    /// Represents the client request for a <see cref="ConnectReply"/>.
    /// </summary>
    [OWServerPacket(0x800E)]
    internal class ConnectQuery : IPacket
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) { }

        public void Save(ref SpanWriter writer) => throw new NotImplementedException();
    }
}
