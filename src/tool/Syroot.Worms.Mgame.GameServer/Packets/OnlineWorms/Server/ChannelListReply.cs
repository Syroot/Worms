﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using Syroot.BinaryData.Memory;
using Syroot.Worms.Mgame.GameServer.Core.IO;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Server
{
    /// <summary>
    /// Represents an additional server response to a <see cref="LoginQuery"/>, providing available server channels.
    /// </summary>
    [OWServerPacket(0x80C9)]
    internal class ChannelListReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public IList<ChannelInfo> Channels { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteUInt16((ushort)Channels.Count);
            foreach (ChannelInfo channel in Channels)
            {
                writer.WriteEnumSafe(channel.Type);
                writer.WriteByte(channel.Coins);
                writer.WriteColor(channel.Color);
                writer.WriteUInt16(1); // ?

                writer.WriteString2(channel.Name);
                writer.WriteByte(0); // ?

                writer.WriteBytes(channel.EndPoint.Address.GetAddressBytes());
                writer.WriteUInt16((ushort)channel.EndPoint.Port);
                writer.WriteEnumSafe(channel.Load);
            }
        }
    }

    internal class ChannelInfo
    {
        /// <summary>
        /// Gets or sets the name of the channel. Should not exceed 32 characters.
        /// </summary>
        internal string Name { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="IPEndPoint"/> under which the server hosting this channel can be reached.
        /// </summary>
        internal IPEndPoint EndPoint { get; set; }

        /// <summary>
        /// Gets or sets the type of the channel determining where it will be shown in the server screen.
        /// </summary>
        internal ChannelType Type { get; set; }

        /// <summary>
        /// Gets or sets an indicator of coins with unclear meaning. 0 is no indicator, 1 is few coins, 2 is many.
        /// </summary>
        internal byte Coins { get; set; }

        /// <summary>
        /// Gets or sets the text color of the channel.
        /// </summary>
        internal Color Color { get; set; } = Color.White;

        /// <summary>
        /// Gets or sets an indicator or state visualizing the number of players in this channel.
        /// </summary>
        internal ChannelLoad Load { get; set; } = ChannelLoad.Lowest;
    }

    internal enum ChannelType : byte
    {
        Normal = 0,
        Roping = 1,
        Special = 2
    }

    internal enum ChannelLoad : byte
    {
        Hidden,
        Lowest,
        Low,
        Medium,
        High,
        Highest,
        Full,
        Closed = 99
    }
}
