﻿using System;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Server
{
    /// <summary>
    /// Represents the server response to a <see cref="ConnectQuery"/>.
    /// </summary>
    [OWServerPacket(0x800F)]
    internal class ConnectReply : IPacket
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal string Unknown { get; set; }

        internal string Unknown2 { get; set; }

        internal ushort Version { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Load(ref SpanReader reader) => throw new NotImplementedException();

        public void Save(ref SpanWriter writer)
        {
            writer.WriteString2(Unknown);
            writer.WriteByte(0);
            writer.WriteString2(Unknown2);
            writer.WriteUInt16(Version);
        }
    }
}
