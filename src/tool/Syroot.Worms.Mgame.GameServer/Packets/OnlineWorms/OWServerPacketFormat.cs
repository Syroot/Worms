﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Syroot.BinaryData;
using Syroot.BinaryData.Memory;
using Syroot.Worms.Mgame.GameServer.Core.Reflection;

namespace Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms
{
    /// <summary>
    /// Represents the factory of packets in the OW server packet format.
    /// </summary>
    internal class OWServerPacketFormat : IPacketFormat
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _maxDataSize = 1024;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly IList<(Type type, OWServerPacketAttribute attrib)> _cachedClasses
             = ReflectionTools.GetAttributedClasses<OWServerPacketAttribute>();

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        private OWServerPacketFormat() { }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal static OWServerPacketFormat Instance { get; } = new OWServerPacketFormat();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public IPacket TryLoad(Stream stream)
        {
            // Read head.
            ushort id = stream.ReadUInt16();
            IPacket packet = GetPacket(id);
            if (packet == null)
                return null;
            int dataSize = stream.ReadUInt16();

            // Read data.
            SpanReader reader = new SpanReader(stream.ReadBytes(dataSize), encoding: Encodings.Korean);

            // Deserialize and return packet.
            packet.Load(ref reader);
            return packet;
        }

        public bool TrySave(Stream stream, IPacket packet)
        {
            ushort? id = GetID(packet);
            if (!id.HasValue)
                return false;

            // Retrieve data.
            SpanWriter writer = new SpanWriter(new byte[_maxDataSize], encoding: Encodings.Korean);
            packet.Save(ref writer);

            // Send head.
            stream.WriteUInt16(id.Value);
            stream.WriteUInt16((ushort)writer.Position);

            // Write data.
            stream.Write(writer.Span);
            return true;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private IPacket GetPacket(ushort id)
        {
            Type packetType = _cachedClasses.Where(x => x.attrib.ID == id).FirstOrDefault().type;
            return packetType == null ? null : (IPacket)Activator.CreateInstance(packetType);
        }

        private ushort? GetID(IPacket packet)
        {
            Type packetType = packet.GetType();
            return _cachedClasses.Where(x => x.type == packetType).FirstOrDefault().attrib?.ID;
        }
    }
}
