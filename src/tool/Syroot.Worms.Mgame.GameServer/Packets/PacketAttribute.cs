﻿using System;

namespace Syroot.Worms.Mgame.GameServer.Packets
{
    /// <summary>
    /// Decorates an <see cref="IPacket"/> child class with the ID of the packet it represents.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    internal abstract class PacketAttribute : Attribute
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="PacketAttribute"/> class, decorating a class with the ID of the
        /// packet it represents.
        /// </summary>
        /// <param name="id">The ID of the packet which the decorated class represents.</param>
        internal PacketAttribute(int id)
        {
            ID = id;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the ID of the packet the decorated class represents.
        /// </summary>
        internal int ID { get; }
    }
}
