﻿using System.IO;

namespace Syroot.Worms.Mgame.GameServer.Packets
{
    /// <summary>
    /// Represents the implementation of a specific packet format which can instantiate corresponding classes.
    /// </summary>
    internal interface IPacketFormat
    {
        // ---- METHODS ------------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns an <see cref="IPacket"/> instance read from the given <paramref name="stream"/> or
        /// <see langword="null"/> if the data was not compatible with this format.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read data from.</param
        /// <returns>The instantiated <see cref="IPacket"/> or <see langword="null"/>.</returns>
        IPacket TryLoad(Stream stream);

        /// <summary>
        /// Returns a value indicating whether this format could save the <paramref name="packet"/> to the given
        /// <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to write data to.</param>
        /// <param name="packet">The <see cref="IPacket"/> to serialize, if possibile.</param>
        /// <returns><see langword="true"/> if a packet was serialized; otherwise, <see langword="false"/>.</returns>
        bool TrySave(Stream stream, IPacket packet);
    }
}
