﻿using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Packets.Data
{
    /// <summary>
    /// Represents the authentication credentials for a player account sent with LoginQuery and ChanneLoginQuery
    /// packets.
    /// </summary>
    internal class PlayerCredentials
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the name of the player to login.
        /// </summary>
        internal string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password with which the player profile is authenticated.
        /// </summary>
        internal string Password { get; set; }
    }

    internal static partial class SpanReaderExtensions
    {
        internal static PlayerCredentials ReadCredentials(this ref SpanReader self) => new PlayerCredentials
        {
            UserName = self.ReadString2(),
            Password = self.ReadString2()
        };
    }
}
