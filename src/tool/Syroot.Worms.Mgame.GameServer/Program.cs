﻿using System;

namespace Syroot.Worms.Mgame.GameServer
{
    /// <summary>
    /// Represents the main class of the application containing the entry point.
    /// </summary>
    internal class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main()
        {
            try
            {
                // Start a new server.
                new Server().Listen();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unhandled exception: {ex}");
            }
        }
    }
}
