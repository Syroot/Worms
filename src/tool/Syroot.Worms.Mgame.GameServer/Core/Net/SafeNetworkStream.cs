﻿using System.Net.Sockets;
using Syroot.Worms.Mgame.GameServer.Core.IO;

namespace Syroot.Worms.Mgame.GameServer.Core.Net
{
    /// <summary>
    /// Represents a <see cref="NetworkStream"/> which ensures to read the specified number of bytes and does not return
    /// prematurely.
    /// </summary>
    /// <typeparam name="T">The type of the wrapped <see cref="Stream"/>.</typeparam>
    internal class SafeNetworkStream : SafeWrapperStream<NetworkStream>
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal SafeNetworkStream(NetworkStream baseStream) : base(baseStream) { }
    }
}
