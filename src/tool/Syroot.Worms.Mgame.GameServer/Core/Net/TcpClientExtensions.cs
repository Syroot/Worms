﻿using System.Net.Sockets;

namespace Syroot.Worms.Mgame.GameServer.Core.Net
{
    /// <summary>
    /// Represents extension methods for <see cref="TcpClient"/> instances.
    /// </summary>
    internal static class TcpClientExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the <see cref="SafeNetworkStream"/> used to send and receive data.
        /// </summary>
        /// <param name="self">The extended <see cref="TcpClient"/> instance.</param>
        /// <returns>The <see cref="SafeNetworkStream"/> instance.</returns>
        internal static SafeNetworkStream GetSafeStream(this TcpClient self)
        {
            return new SafeNetworkStream(self.GetStream());
        }
    }
}
