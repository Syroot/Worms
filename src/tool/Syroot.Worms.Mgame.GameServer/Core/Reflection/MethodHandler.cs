﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Syroot.Worms.Mgame.GameServer.Core.Reflection
{
    /// <summary>
    /// Represents an instance dispatching parameters of type <typeparamref name="T"/> to methods accepting them.
    /// </summary>
    /// <typeparam name="T">The type of the parameter to dispatch to methods.</typeparam>
    internal class MethodHandler<T>
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly Dictionary<Type, Dictionary<Type, MethodInfo>> _classCache;

        private readonly object _instance;
        private readonly Dictionary<Type, MethodInfo> _handlers;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        static MethodHandler()
        {
            _classCache = new Dictionary<Type, Dictionary<Type, MethodInfo>>();
        }

        internal MethodHandler(object instance)
        {
            _instance = instance;
            _handlers = GetHandlers(_instance);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Handle(T parameter)
        {
            Type parameterType = parameter.GetType();
            if (!_handlers.TryGetValue(parameterType, out MethodInfo method))
                throw new ArgumentException($"No method handles type {parameterType}.");
            method.Invoke(_instance, new object[] { parameter });
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private Dictionary<Type, MethodInfo> GetHandlers(object instance)
        {
            Type classType = instance.GetType();
            if (!_classCache.TryGetValue(classType, out Dictionary<Type, MethodInfo> handlerMethods))
            {
                // Find all methods which accept a specific parameter and return nothing.
                handlerMethods = new Dictionary<Type, MethodInfo>();
                foreach (MethodInfo method in classType.GetMethods(BindingFlags.Public | BindingFlags.Instance))
                {
                    Type parameterType = method.GetParameters().FirstOrDefault()?.ParameterType;
                    if (parameterType == null)
                        continue;
                    if (handlerMethods.ContainsKey(parameterType))
                        throw new InvalidOperationException($"Parameter {parameterType} handled by multiple methods.");
                    if (method.ReturnType == typeof(void) && typeof(T).IsAssignableFrom(parameterType))
                        handlerMethods.Add(parameterType, method);
                }
                _classCache.Add(classType, handlerMethods);
            }
            return handlerMethods;
        }
    }
}
