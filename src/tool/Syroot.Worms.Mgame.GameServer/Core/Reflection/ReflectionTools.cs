﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Syroot.Worms.Mgame.GameServer.Core.Reflection
{
    /// <summary>
    /// Represents utility methods helping with reflection.
    /// </summary>
    internal static class ReflectionTools
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static IList<(Type type, TAttrib attrib)> GetAttributedClasses<TAttrib>()
            where TAttrib : Attribute
        {
            List<(Type, TAttrib)> classes = new List<(Type, TAttrib)>();
            foreach (Type type in Assembly.GetExecutingAssembly().GetTypes())
                foreach (TAttrib attrib in type.GetCustomAttributes<TAttrib>())
                    classes.Add((type, attrib));
            return classes;
        }
    }
}
