﻿using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Core.IO
{
    /// <summary>
    /// Represents extension methods for <see cref="SpanReader"/> instances.
    /// </summary>
    internal static class SpanReaderExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Reads the remaining bytes that are available.
        /// </summary>
        /// <param name="self">The extended instance.</param>
        /// <returns>The remaining bytes in the reader.</returns>
        internal static byte[] ReadToEnd(this ref SpanReader self) => self.ReadBytes(self.Length - self.Position);
    }
}
