﻿using System;
using System.IO;

namespace Syroot.Worms.Mgame.GameServer.Core.IO
{
    /// <summary>
    /// Represents a stream which ensures to read the specified number of bytes and does not return prematurely.
    /// </summary>
    /// <typeparam name="T">The type of the wrapped <see cref="Stream"/>.</typeparam>
    internal class SafeWrapperStream<T> : WrapperStream<T>
        where T : Stream
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal SafeWrapperStream(T baseStream, bool leaveOpen = false) : base(baseStream, leaveOpen) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Reads <paramref name="count"/> number of bytes to the given <paramref name="buffer"/>, starting at the
        /// specified <paramref name="offset"/>, and returns the number of bytes read. Since this method ensures that
        /// the requested number of bytes is always read, it always returns <paramref name="count"/>, and otherwise
        /// throws an <see cref="IOException"/> if the required bytes could not be read.
        /// </summary>
        /// <param name="buffer">The byte array to write the read data to.</param>
        /// <param name="offset">The offset into <paramref name="buffer"/> at which to start writing data.</param>
        /// <param name="count">The number of bytes to read into <paramref name="buffer"/>.</param>
        /// <exception cref="IOException">The requested number of bytes could not be read.</exception>
        /// <returns>The number of bytes requested.</returns>
        public override int Read(byte[] buffer, int offset, int size)
        {
            int totalRead = 0;
            while (totalRead < size)
            {
                // For network streams, read returns 0 only when the underlying socket is closed, otherwise it blocks.
                int read = BaseStream.Read(buffer, offset + totalRead, size - totalRead);
                if (read == 0)
                    throw new IOException("The underlying stream closed, 0 bytes were retrieved.");
                totalRead += read;
            }
            return totalRead;
        }

        /// <summary>
        /// Reads the required number of bytes to fill the given <paramref name="buffer"/>, and returns the number of
        /// bytes read. Since this method ensures that the requested number of bytes is always read, it always returns
        /// the length of the buffer, and otherwise throws an <see cref="IOException"/> if the required bytes could not
        /// be read.
        /// </summary>
        /// <param name="buffer">The <see cref="Span{byte}"/> to write the read data to.</param>
        /// <exception cref="IOException">The requested number of bytes could not be read.</exception>
        /// <returns>The number of bytes requested.</returns>
        public override int Read(Span<byte> buffer)
        {
            int totalRead = 0;
            while (totalRead < buffer.Length)
            {
                // For network streams, read returns 0 only when the underlying socket is closed, otherwise it blocks.
                int read = BaseStream.Read(buffer.Slice(totalRead));
                if (read == 0)
                    throw new IOException("The underlying stream closed, 0 bytes were retrieved.");
                totalRead += read;
            }
            return totalRead;
        }
    }
}
