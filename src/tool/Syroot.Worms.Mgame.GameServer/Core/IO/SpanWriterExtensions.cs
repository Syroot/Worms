﻿using System.Drawing;
using System.Net;
using Syroot.BinaryData.Memory;

namespace Syroot.Worms.Mgame.GameServer.Core.IO
{
    /// <summary>
    /// Represents extension methods for <see cref="SpanWriter"/> instances.
    /// </summary>
    internal static class SpanWriterExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Writes the given <see cref="Color"/> as an RGB0 integer value.
        /// </summary>
        /// <param name="self">The extended instance.</param>
        /// <param name="value">The value to write.</param>
        internal static void WriteColor(this ref SpanWriter self, Color color)
        {
            self.WriteByte(color.R);
            self.WriteByte(color.G);
            self.WriteByte(color.B);
            self.WriteByte(0);
        }

        /// <summary>
        /// Writes the given <see cref="IPEndPoint"/> as 4 separate address bytes and a 2-byte port.
        /// </summary>
        /// <param name="self">The extended instance.</param>
        /// <param name="value">The value to write.</param>
        internal static void WriteIPEndPoint(this ref SpanWriter self, IPEndPoint value)
        {
            self.WriteBytes(value.Address.GetAddressBytes());
            self.WriteUInt16((ushort)value.Port);
        }
    }
}
