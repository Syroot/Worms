﻿using System.Drawing;
using System.Net;
using Syroot.Worms.Mgame.GameServer.Packets.Data;
using Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Server;

namespace Syroot.Worms.Mgame.GameServer
{
    internal partial class Client
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void HandleOWServerConnect(ConnectQuery packet)
        {
            SendPacket(new ConnectReply
            {
                Unknown = _server.Config.Name,
                Unknown2 = _server.Config.Region,
                Version = _server.Config.Version
            });
        }

        public void HandleOWServerLogin(LoginQuery packet)
        {
            // Send login result.
            // Create player infos from the given credentials. This would be the place to check for actual accounts.
            LoginPlayerInfo[] playerInfos = new LoginPlayerInfo[packet.Players.Length];
            for (int i = 0; i < packet.Players.Length; i++)
            {
                PlayerCredentials credentials = packet.Players[i];
                playerInfos[i] = new LoginPlayerInfo { ID = credentials.UserName, Rank = 19 };
            }
            SendPacket(new LoginReply
            {
                Result = LoginResult.Success,
                PlayerInfos = playerInfos
            });

            // Send info text.
            SendPacket(new InfoReply
            {
                Text = "Welcome to the Online Worms Server."
            });

            // Send channels. The port is abused to identify the channel later on.
            SendPacket(new ChannelListReply
            {
                Channels = new[]
                {
                    new ChannelInfo
                    {
                        Name = "Test Channel",
                        EndPoint = new IPEndPoint(_server.Config.IPAddress, 1),
                        Type = ChannelType.Normal,
                        Color = Color.LightGreen,
                        Load = ChannelLoad.Highest
                    },
                    new ChannelInfo
                    {
                        Name = "Real Channel",
                        EndPoint = new IPEndPoint(_server.Config.IPAddress, 2),
                        Type = ChannelType.Normal
                    },
                    new ChannelInfo
                    {
                        Name = "Boredom Time",
                        EndPoint = new IPEndPoint(_server.Config.IPAddress, 3),
                        Type = ChannelType.Normal,
                        Load = ChannelLoad.Medium
                    },
                    new ChannelInfo
                    {
                        Name = "Nothing Goes",
                        EndPoint = new IPEndPoint(_server.Config.IPAddress, 4),
                        Type = ChannelType.Roping,
                        Color = Color.Orange,
                        Coins = 2,
                        Load = ChannelLoad.Closed
                    },
                    new ChannelInfo
                    {
                        Name = "Doper's Heaven",
                        EndPoint = new IPEndPoint(_server.Config.IPAddress, 5),
                        Type = ChannelType.Roping,
                        Color = Color.Orange,
                        Coins = 1,
                        Load = ChannelLoad.Full
                    },
                    new ChannelInfo
                    {
                        Name = "Unhelpful Channel",
                        EndPoint = new IPEndPoint(_server.Config.IPAddress, 6),
                        Type = ChannelType.Special
                    }
                }
            });
        }

        public void HandleOWServerChannelEnter(ChannelEnterQuery packet)
        {
            // Simply allow joining a channel running on the same server port.
            SendPacket(new ChannelEnterReply
            {
                EndPoint = _server.Config.EndPoint
            });
        }
    }
}
