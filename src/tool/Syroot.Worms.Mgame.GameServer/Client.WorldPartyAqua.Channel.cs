﻿using System;
using System.Collections.Generic;
using Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Channel;
using Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Data;

namespace Syroot.Worms.Mgame.GameServer
{
    internal partial class Client
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void HandleWwpaChannelAddFriend(AddFriendQuery packet)
        {
            SendPacket(new AddFriendReply
            {
                Result = FriendRequestResult.Success,
                UserName = packet.UserName
            });
        }

        public void HandleWwpaChannelChatMessage(ChatMessageQuery packet)
        {
            SendPacket(new ChatMessageReply
            {
                UnknownA = 0,
                Scene = ChatMessageScene.Current,
                UnknownB = "Bla",
                Message = packet.Message
            });
        }

        public void HandleWwpaChannelCmdFindUser(CmdFindUserQuery packet)
        {
            SendPacket(new CmdFindUserReply
            {
                Success = true,
                NickName = packet.UserName,
                Gender = UserGender.Male,
                GamesWon = 12,
                GamesLost = 17,
                GamesDrawn = 8,
                Level = 15,
                Guild = "Shitterguild",
                Experience = 2350,
                Currency = 12000,

                Phone = "081 892 0242",
                Address = "Asshole Road 13",
                Birthday = new DateTime(1997, 11, 3),
                Introduction = "Hello I'm sooo gay",

                MaybeAvatar = new int[13]
            });
        }

        public void HandleWwpaChannelCmdStartSingleGame(CmdStartSingleGameQuery packet)
        {
            SendPacket(new CmdStartSingleGameReply
            {
                Stuff = new List<StartSingleGameReplyStuff>
                {
                    new StartSingleGameReplyStuff
                    {
                        UnknownA = 1,
                        UnknownB = 2,
                        UnknownC = 3,
                        UnknownD = 4,
                        UnknownE = 5
                    }
                }
            });
        }

        public void HandleWwpaChannelCmdUnknown6(CmdUnknown6Query packet)
        {
            SendPacket(new CmdUnknown6Reply
            {
                Result = Unknown6Status.Disconnect
            });
        }

        public void HandleWwpaChannelDisconnect(DisconnectNotice packet) { }

        public void HandleWwpaChannelLogin(LoginQuery packet)
        {
            SendPacket(new LoginReply { Result = LoginResult.Success });
            //SendPacket(new NotChannelLoginReply());
        }

        public void HandleWwpaChannelLogout(LogoutQuery packet)
        {
            SendPacket(new LogoutReply { Success = true });
        }

        public void HandleWwpaChannelUnknown11E(Unknown11EQuery packet)
        {
            SendPacket(new Unknown11FReply
            {
                UnknownA = 0x11111111,
                UnknownB = "Hello",
                UnknownC = 0x2222,
                UnknownD = 0x3333
            });
        }

        public void HandleWwpaChannelUnknown15A(Unknown15AQuery packet)
        {
            SendPacket(new Unknown15BReply
            {
                UnknownA = 0x1111,
                UnknownB = 0x22222222,
                UnknownC = "Test",
                UnknownD = 0x3333,
                UnknownE = 0x44444444,
                UnknownF = 0x55,
            });
        }

        public void HandleWwpaChannelUserList(UserListQuery packet)
        {
            List<UserListPlayer> players = new List<UserListPlayer>();
            for (int i = 0; i < 9; i++)
            {
                players.Add(new UserListPlayer
                {
                    UserName = $"Nick{i + 1}",
                    Gender = i % 2 == 0 ? UserGender.Male : UserGender.Female,
                    Guild = (byte)(i * 2),
                    Level = (ushort)((i + 1) * 5)
                });
            }

            SendPacket(new UserListReply
            {
                Command = UserListCommand.Unknown0,
                Players = players
            });
        }
    }
}
