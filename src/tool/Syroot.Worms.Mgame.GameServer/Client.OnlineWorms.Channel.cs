﻿using System.Collections.Generic;
using Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms.Channel;

namespace Syroot.Worms.Mgame.GameServer
{
    internal partial class Client
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void HandleOWChannelConnect(LoginQuery packet)
        {
            SendPacket(new LoginReply
            {
                Result = ChannelConnectResult.Success,
                Player = new ChannelConnectPlayerInfo
                {
                    ID = packet.Players[0].UserName,
                    Name = "Your Name",
                    Experience = 1337,
                    Gold = 1000000,
                    Rank = 19,
                    GuildMarkIndex = 1,
                    GuildName = "Your Guild"
                }
            });
        }

        public void HandleOWChannelTop20(Top20Query packet)
        {
            // Send some simulated player rank infos.
            Top20Reply reply = new Top20Reply
            {
                UnknownA = "Test",
                Top20 = new List<ChannelTop20Player>(20)
            };
            for (int i = 0; i < 20; i++)
            {
                reply.Top20.Add(new ChannelTop20Player
                {
                    Name = $"GoodPlayer{(char)('A' + i)}",
                    Rank = (ushort)((i + 6) / 3),
                    Experience = (ulong)(20 - i) * 957
                });
            }
            SendPacket(reply);

            // This is the last channel info packet, tell the client to go to the channel screen.
            SendPacket(new EnterFinishReply());
        }

        public void HandleOWChannelStartSingleGameQuery(StartSingleGameQuery packet)
        {
            SendPacket(new StartSingleGameReply
            {
                Success = packet.RoundType == GameStartRoundType.First
            });
        }
    }
}
