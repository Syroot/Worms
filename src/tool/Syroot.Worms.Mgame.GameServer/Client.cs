﻿using System;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using Syroot.Worms.Mgame.GameServer.Core;
using Syroot.Worms.Mgame.GameServer.Packets;
using Syroot.Worms.Mgame.GameServer.Packets.OnlineWorms;
using Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua;

namespace Syroot.Worms.Mgame.GameServer
{
    /// <summary>
    /// Represents a connection with a game client which replies to received packets appropriately.
    /// </summary>
    internal partial class Client : PacketConnection
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly Server _server;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Client"/> class accepted by the given <paramref name="server"/>
        /// through the provided <see cref="tcpClient"/>.
        /// </summary>
        /// <param name="tcpClient">The <see cref="TcpClient"/> representing the connection to the client.</param>
        /// <param name="server">The <see cref="Server"/> instance with which this client communicates.</param>
        internal Client(TcpClient tcpClient, Server server) : base(tcpClient,
            WwpaPacketFormat.Instance, OWChannelPacketFormat.Instance, OWServerPacketFormat.Instance)
        {
            _server = server;
            PrePacketHandle += OnPrePacketHandle;
            PrePacketSend += OnPrePacketSend;
            UnhandledException += OnUnhandledException;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private string FormatPacket(IPacket packet, string direction)
        {
            string packetName = String.Join(' ', packet.GetType().FullName.Split('.').TakeLast(2));
            return $"{TcpClient.Client.RemoteEndPoint} {direction} {packetName}{ObjectDumper.Dump(packet)}";
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void OnPrePacketHandle(object sender, IPacket packet)
        {
            _server.Log.Write(LogCategory.Client, FormatPacket(packet, ">>"));
        }

        private void OnPrePacketSend(object sender, IPacket packet)
        {
            _server.Log.Write(LogCategory.Server, FormatPacket(packet, "<<"));
            if (_server.Config.SendDelay > 0)
                Thread.Sleep(_server.Config.SendDelay);
        }

        private void OnUnhandledException(object sender, Exception ex)
        {
            _server.Log.Write(LogCategory.Error, $"{TcpClient.Client.RemoteEndPoint} {ex.Message}");
        }
    }
}
