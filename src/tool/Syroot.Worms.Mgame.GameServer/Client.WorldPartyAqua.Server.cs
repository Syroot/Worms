﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua;
using Syroot.Worms.Mgame.GameServer.Packets.WorldPartyAqua.Server;

namespace Syroot.Worms.Mgame.GameServer
{
    internal partial class Client
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private int _sendConnectTries;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void HandleWwpaServerChannelList(ChannelListQuery packet)
        {
            SendPacket(new ChannelListReply
            {
                Channels = new List<ChannelInfo>
                {
                    new ChannelInfo
                    {
                        UnknownA = 0x11,
                        UnknownD = 0x22,
                        Name = "Party Hard",
                        EndPoint = _server.Config.EndPoint,
                        Type = ChannelType.Normal,
                        Status = ChannelStatus.Load2
                    },
                    new ChannelInfo
                    {
                        UnknownA = 0x33,
                        UnknownD = 0x44,
                        Name = "Pay 2 Win",
                        EndPoint = _server.Config.EndPoint,
                        Type = ChannelType.Normal,
                        Status = ChannelStatus.Closed
                    },
                    new ChannelInfo
                    {
                        UnknownA = 0x55,
                        UnknownD = 0x66,
                        Name = "Free 4 None",
                        EndPoint = _server.Config.EndPoint,
                        Type = ChannelType.Normal,
                        Status = ChannelStatus.Load1
                    },
                    new ChannelInfo
                    {
                        UnknownA = 0x77,
                        UnknownD = 0x88,
                        Name = "Nothing Goes",
                        EndPoint = _server.Config.EndPoint,
                        Type = ChannelType.Roping,
                        Status = ChannelStatus.Full
                    },
                    new ChannelInfo
                    {
                        UnknownA = 0x99,
                        UnknownD = 0xAA,
                        Name = "Ropers Hell",
                        EndPoint = _server.Config.EndPoint,
                        Type = ChannelType.Roping,
                        Status = ChannelStatus.Load3
                    },
                    new ChannelInfo
                    {
                        UnknownA = 0xBB,
                        UnknownD = 0xCC,
                        Name = "Gay Guilds",
                        EndPoint = _server.Config.EndPoint,
                        Type = ChannelType.Guild,
                        Status = ChannelStatus.Load2
                    },
                    new ChannelInfo
                    {
                        UnknownA = 0xDD,
                        UnknownD = 0xEE,
                        Name = "Enormous Event",
                        EndPoint = _server.Config.EndPoint,
                        Type = ChannelType.Event,
                        Status = ChannelStatus.Closed
                    }
                }
            });
        }

        public async void HandleWwpaServerConnect(ConnectQuery packet)
        {
            // Client fails to detect reply at rare times due to unknown reasons. Resend packet some times.
            ConnectReply reply = new ConnectReply { ServerVersion = _server.Config.Version };
            _sendConnectTries = 10;
            while (_sendConnectTries-- > 0)
            {
                SendPacket(reply);
                await Task.Delay(500);
            }
        }

        public void HandleWwpaServerDisconnect(DisconnectNotice packet) { }

        public void HandleWwpaServerLogin(LoginQuery packet)
        {
            _sendConnectTries = 0;
            SendPacket(new LoginReply { Result = LoginResult.Success });
        }

        public void HandleWwpaServerOptionsQuery(OptionsQuery packet)
        {
            SendPacket(new OptionsReply
            {
                Success = true,
                Unknown = 1,
                WormNames = new List<string> { "WormName1", "WormName2", "WormName3" }
            });
        }

        public void HandleWwpaServerOptionsSaveQuery(OptionsSaveQuery packet)
        {
            SendPacket(new OptionsSaveReply { Success = true });
        }

        public void HandleWwpaRawPacket(RawPacket packet) { }
    }
}
