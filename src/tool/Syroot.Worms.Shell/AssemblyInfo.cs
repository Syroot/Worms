﻿using System.Reflection;

namespace Syroot.Worms.Shell
{
    internal static class AssemblyInfo
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        static AssemblyInfo()
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly();
            Title = entryAssembly.GetCustomAttribute<AssemblyTitleAttribute>().Title;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal static string Title { get; }
    }
}
